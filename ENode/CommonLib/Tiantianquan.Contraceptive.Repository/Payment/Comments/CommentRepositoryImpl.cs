﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Criterion;
using Tiantianquan.Infrastructure.Repositories;
using Tiantianquan.Contraceptive.Domain.Payment;

namespace Tiantianquan.Contraceptive.Repository.Payment
{
    /// <summary>
    /// 评论
    /// </summary>
   public class CommentRepositoryImpl:Repository<CommentInfo,Guid>,CommentRepository
    {
        #region 字段常量
        internal class FieldNames
        {
            /// <summary>
            /// 评论编号
            /// </summary>
            public const string CommentId = "CommentId";
            /// <summary>
            /// 订单编号
            /// </summary>
            public const string OrderId = "OrderId";
            /// <summary>
            /// 用户编号
            /// </summary>
            public const string ResidentUserId = "ResidentUserId";
            /// <summary>
            /// 头像
            /// </summary>
            public const string HeadImageUrl = "HeadImageUrl";
            /// <summary>
            /// 昵称
            /// </summary>
            public const string FullName = "FullName";
            /// <summary>
            /// 手机号
            /// </summary>
            public const string Phone = "Phone";
            /// <summary>
            /// 评论内容
            /// </summary>
            public const string CommentContent = "CommentContent";
            /// <summary>
            /// 评论图片
            /// </summary>
            public const string CommentImages = "CommentImages";
            /// <summary>
            /// 评论时间
            /// </summary>
            public const string CreateTime = "CreateTime";
            /// <summary>
            /// 来源IP
            /// </summary>
            public const string IPAddress = "IPAddress";
        }
        #endregion
        /// <summary>
        /// 获取评论分页数据
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        public List<CommentInfo> GetCommentByPage(CommentPageParam pageParam)
        {
            var criteria = Session.CreateCriteria<CommentInfo>();
            if (!string.IsNullOrWhiteSpace(pageParam.Keywords))
            {
                criteria.Add(Restrictions.Like(FieldNames.HeadImageUrl, "%" + pageParam.Keywords.Trim() + "%"));
            }

            criteria.AddOrder(NHibernate.Criterion.Order.Asc(FieldNames.CreateTime));
            if (pageParam.IsClientSort())
            {
                pageParam.CreateOrderCriteria(criteria);
            }
            criteria.SetFirstResult(pageParam.GetFirstResult());
            criteria.SetMaxResults(pageParam.GetMaxResults());
            List<CommentInfo> items = criteria.List<CommentInfo>().ToList();
            return items;
        }
        /// <summary>
        /// 获取评论总记录数
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        public int GetCommentRecordCount(CommentPageParam pageParam)
        {
            var criteria = Session.CreateCriteria<CommentInfo>();
            if (!string.IsNullOrWhiteSpace(pageParam.Keywords))
            {
                criteria.Add(Restrictions.Like(FieldNames.HeadImageUrl, "%" + pageParam.Keywords.Trim() + "%"));
            }
            criteria.SetProjection(Projections.RowCount());
            int total = (int)criteria.UniqueResult();
            return total;
        }
    }
}

