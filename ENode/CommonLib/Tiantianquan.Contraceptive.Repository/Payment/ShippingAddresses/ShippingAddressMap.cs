﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Tiantianquan.Contraceptive.Domain.Payment;

namespace Tiantianquan.Contraceptive.Repository.Payment
{

    public class ShippingAddressMap:ClassMap<ShippingAddressInfo>
    {
        public ShippingAddressMap()
        {
            this.Table("Payment_ShippingAddress");
            this.Id(x => x.ShippingAddressId).GeneratedBy.Assigned();
            this.Map(x => x.ResidentUserId).Nullable();
            this.Map(x => x.FullName).Nullable().Length(256);
            this.Map(x => x.AddressName).Nullable().Length(256);
            this.Map(x => x.AddressCode).Nullable().Length(256);
            this.Map(x => x.AddressStreet).Nullable().Length(256);
            this.Map(x => x.Phone).Nullable().Length(256);
        }
    }
}

