﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure;
using Tiantianquan.Infrastructure.AutoMapper;
using Tiantianquan.Infrastructure.Application;
using Tiantianquan.Infrastructure.Utils;
using Tiantianquan.Infrastructure.Utilities;
using Tiantianquan.Infrastructure.UI;
using Tiantianquan.Contraceptive.Domain.Payment;

namespace Tiantianquan.Contraceptive.Application.Payment
{
    /// <summary>
    /// 收货地址
    /// </summary>
   public class ShippingAddressService:ServiceBase
    {
        public ShippingAddressService(ShippingAddressRepository shippingAddressRepository)
        {
            ShippingAddressRepository = shippingAddressRepository;
        }

        protected ShippingAddressRepository ShippingAddressRepository { get; private set; }
        
        public void Insert(ShippingAddressModel model)
        {
            ShippingAddressInfo shippingAddress = new ShippingAddressInfo();
            shippingAddress.ShippingAddressId = GuidUtils.SecureSequentialGuid();       
            shippingAddress.ResidentUserId = model.ResidentUserId;
            shippingAddress.FullName = model.FullName;
            shippingAddress.AddressName = model.AddressName;
            shippingAddress.AddressCode = model.AddressCode;
            shippingAddress.AddressStreet = model.AddressStreet;
            shippingAddress.Phone = model.Phone;
            this.ShippingAddressRepository.Insert(shippingAddress);
        }
        
        public void Update(Guid shippingAddressId,ShippingAddressModel model)
        {
            ShippingAddressInfo shippingAddress = this.ShippingAddressRepository.Get(shippingAddressId);
            if (shippingAddress == null)
            {
                throw new FriendlyException("记录不存在。");
            }
            shippingAddress.ResidentUserId = model.ResidentUserId;
            shippingAddress.FullName = model.FullName;
            shippingAddress.AddressName = model.AddressName;
            shippingAddress.AddressCode = model.AddressCode;
            shippingAddress.AddressStreet = model.AddressStreet;
            shippingAddress.Phone = model.Phone;
            this.ShippingAddressRepository.Update(shippingAddress);
        }
        
        public ShippingAddressInfo GetShippingAddressById(Guid shippingAddressId)
        {
            return this.ShippingAddressRepository.Get(shippingAddressId);
        }

        public void Delete(Guid shippingAddressId)
        {
            ShippingAddressInfo shippingAddress=this.ShippingAddressRepository.Get(shippingAddressId);
            if (shippingAddress == null)
                return;
            this.ShippingAddressRepository.Delete(shippingAddress);
        }

        public PageResult<ShippingAddressInfo> GetShippingAddressByPage(ShippingAddressPageParam pageParam)
        {
            List<ShippingAddressInfo> items = this.ShippingAddressRepository.GetShippingAddressByPage(pageParam);
            int total = this.ShippingAddressRepository.GetShippingAddressRecordCount(pageParam);
            return new PageResult<ShippingAddressInfo>(total, items);
        }
    }
}

