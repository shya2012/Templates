﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure;
using Tiantianquan.Infrastructure.AutoMapper;
using Tiantianquan.Infrastructure.Application;
using Tiantianquan.Infrastructure.Utils;
using Tiantianquan.Infrastructure.Utilities;
using Tiantianquan.Infrastructure.UI;
using Tiantianquan.Contraceptive.Domain.Payment;

namespace Tiantianquan.Contraceptive.Application.Payment
{
    /// <summary>
    /// 订单明细
    /// </summary>
   public class OrderDetailService:ServiceBase
    {
        public OrderDetailService(OrderDetailRepository orderDetailRepository)
        {
            OrderDetailRepository = orderDetailRepository;
        }

        protected OrderDetailRepository OrderDetailRepository { get; private set; }
        
        public void Insert(OrderDetailModel model)
        {
            OrderDetailInfo orderDetail = new OrderDetailInfo();
            orderDetail.OrderDetailId = GuidUtils.SecureSequentialGuid();       
            orderDetail.OrderId = model.OrderId;
            orderDetail.ProductId = model.ProductId;
            orderDetail.SkuId = model.SkuId;
            orderDetail.ProductName = model.ProductName;
            orderDetail.ProductImage = model.ProductImage;
            orderDetail.SkuProperty = model.SkuProperty;
            orderDetail.SkuPropertyValue = model.SkuPropertyValue;
            orderDetail.TradPrice = model.TradPrice;
            orderDetail.TradQuantity = model.TradQuantity;
            orderDetail.TotalAmount = model.TotalAmount;
            orderDetail.CreateTime = model.CreateTime;
            this.OrderDetailRepository.Insert(orderDetail);
        }
        
        public void Update(Guid orderDetailId,OrderDetailModel model)
        {
            OrderDetailInfo orderDetail = this.OrderDetailRepository.Get(orderDetailId);
            if (orderDetail == null)
            {
                throw new FriendlyException("记录不存在。");
            }
            orderDetail.OrderId = model.OrderId;
            orderDetail.ProductId = model.ProductId;
            orderDetail.SkuId = model.SkuId;
            orderDetail.ProductName = model.ProductName;
            orderDetail.ProductImage = model.ProductImage;
            orderDetail.SkuProperty = model.SkuProperty;
            orderDetail.SkuPropertyValue = model.SkuPropertyValue;
            orderDetail.TradPrice = model.TradPrice;
            orderDetail.TradQuantity = model.TradQuantity;
            orderDetail.TotalAmount = model.TotalAmount;
            orderDetail.CreateTime = model.CreateTime;
            this.OrderDetailRepository.Update(orderDetail);
        }
        
        public OrderDetailInfo GetOrderDetailById(Guid orderDetailId)
        {
            return this.OrderDetailRepository.Get(orderDetailId);
        }

        public void Delete(Guid orderDetailId)
        {
            OrderDetailInfo orderDetail=this.OrderDetailRepository.Get(orderDetailId);
            if (orderDetail == null)
                return;
            this.OrderDetailRepository.Delete(orderDetail);
        }

        public PageResult<OrderDetailInfo> GetOrderDetailByPage(OrderDetailPageParam pageParam)
        {
            List<OrderDetailInfo> items = this.OrderDetailRepository.GetOrderDetailByPage(pageParam);
            int total = this.OrderDetailRepository.GetOrderDetailRecordCount(pageParam);
            return new PageResult<OrderDetailInfo>(total, items);
        }
    }
}

