﻿using CodeSmith.Engine;
using SchemaExplorer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class CodeHelper
{
    private TableSchema Table;

    private string Prefix;

    public CodeHelper(TableSchema table, string prefix)
    {
        Table = table;
        Prefix = prefix;
    }

    /// <summary>
    /// 判断是否是关键字
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static bool IsKeyword(String str)
    {
        List<String> list = new List<String>();
        list.Add("abstract");
        list.Add("as");
        list.Add("base");
        list.Add("bool");
        list.Add("break");
        list.Add("byte");
        list.Add("case");
        list.Add("catch");
        list.Add("char");
        list.Add("checked");
        list.Add("class");
        list.Add("const");
        list.Add("continue");
        list.Add("decimal");
        list.Add("default");
        list.Add("delegate");
        list.Add("do");
        list.Add("double");
        list.Add("else");
        list.Add("enum");
        list.Add("event");
        list.Add("explicit");
        list.Add("extern");
        list.Add("false");
        list.Add("finally");
        list.Add("fixed");
        list.Add("float");
        list.Add("for");
        list.Add("foreach");
        list.Add("goto");
        list.Add("if");
        list.Add("implicit");
        list.Add("in");
        list.Add("int");
        list.Add("interface");
        list.Add("internal");
        list.Add("is");
        list.Add("lock");
        list.Add("long");
        list.Add("namespace");
        list.Add("new");
        list.Add("null");
        list.Add("object");
        list.Add("operator");
        list.Add("out");
        list.Add("override");
        list.Add("params");
        list.Add("private");
        list.Add("protected");
        list.Add("public");
        list.Add("readonly");
        list.Add("ref");
        list.Add("return");
        list.Add("sbyte");
        list.Add("sealed");
        list.Add("short");
        list.Add("sizeof");
        list.Add("stackalloc");
        list.Add("static");
        list.Add("string");
        list.Add("struct");
        list.Add("switch");
        list.Add("this");
        list.Add("throw");
        list.Add("true");
        list.Add("try");
        list.Add("typeof");
        list.Add("uint");
        list.Add("ulong");
        list.Add("unchecked");
        list.Add("unsafe");
        list.Add("ushort");
        list.Add("using");
        list.Add("virtual");
        list.Add("void");
        list.Add("while");
        return list.Contains(str);
    }

    public static string GetPascalName(string text, string prefix = "")
    {
        if (!string.IsNullOrWhiteSpace(prefix))
        {
            if (prefix.EndsWith("_"))
                text = text.Substring(prefix.Length);
            else{
                int pos=text.IndexOf("_");
                if(pos>0){
                    text=text.Substring(pos);
                }
            }
        }

        string[] names = text.Split('_');
        string result = string.Empty;
        foreach (string item in names)
        {
            if (string.IsNullOrWhiteSpace(item))
            {
                continue;
            }
            if (names.Length > 1)
            {
                result += StringUtil.ToPascalCase(item);
            }
            else
            {
                if (char.IsUpper(item[0]))
                {
                    result = item;
                }
                else
                {
                    result = StringUtil.ToPascalCase(item);
                }
            }
        }
        return result;
    }

    public static string GetDescription(ColumnSchema column)
    {
        if (string.IsNullOrWhiteSpace(column.Description))
            return column.Name;
        return column.Description;
    }

    public static string GetPropertyName(ColumnSchema column)
    {
        return GetPascalName(column.Name);
    }

    public static string GetGetPropertyVar(ColumnSchema column)
    {
        string name = GetPascalName(column.Name);
        name = char.ToLower(name[0]) + name.Substring(1);
        if (IsKeyword(name))
        {
            return "@" + name;
        }
        return name;
    }



    public string GetTableDescription()
    {
        if (string.IsNullOrWhiteSpace(Table.Description))
            return Table.Name;
        return Table.Description;
    }

    public string GetClassName()
    {
        return GetPascalName(this.Table.Name, Prefix);
    }
    public string GetClassNameVar()
    {
        string name = GetPascalName(this.Table.Name, Prefix);
        name = char.ToLower(name[0]) + name.Substring(1);
        if (IsKeyword(name))
        {
            return "@" + name;
        }
        return name;
    }
    public string GetDbModelClassName()
    {
        return GetPascalName(this.Table.Name, Prefix)+"Info";
    }
    public string GetRepositoryInterfaceName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "Repository";
    }

    public string GetRepositoryClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "RepositoryImpl";
    }

    public string GetMapClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "Map";
    }

    public string GetModelClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "Model";
    }

    public string GetModelValidatorClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "Validator";
    }

    public string GetListClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "DTO";
    }

    public string GetServiceClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "Service";
    }

    public string GetControllerClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "Controller";
    }

    public string GetPageParamClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "PageParam";
    }
    public string GetCreateCommandClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "CreateCommand";
    }
    public string GetUpdateCommandClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "UpdateCommand";
    }

    public string GetCommandHandlerClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "CommandHandler";
    }

    public string GetDomainEventFileName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "DomainEvent";
    }
    public string GetCommandFile()
    {
        return GetPascalName(this.Table.Name, Prefix) + "Command";
    }
    public string GetCommandHandlerFile()
    {
        return GetPascalName(this.Table.Name, Prefix) + "CommandHandler";
    }
    public string GetDenormalizerClassName ()
    {
        return GetPascalName(this.Table.Name, Prefix) + "Denormalizer";
    }
    public string GetDomainEventCreatedClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "CreatedEvent";
    }
    public string GetDomainEventUpdatedClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "UpdatedEvent";
    }
    public string GetDomainEventDeletedClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "DeletedEvent";
    }
    public string GetCommandCreateClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "CreateCommand";
    }
    public string GetCommandUpdateClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "UpdateCommand";
    }
    public string GetCommandDeleteClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "DeleteCommand";
    }
    public string GetDomainEventHandlerClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "DeletedEvent";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public string GetConstorArguments()
    {
        List<string> list = new List<string>();
        foreach (var item in Table.NonPrimaryKeyColumns)
        {
            list.Add(string.Format("{0} {1}", GetTypeName(item), GetGetPropertyVar(item)));
        }

        return string.Join(",", list);
    }
    public string GetConstorArgumentsDescription(string description)
    {
        List<string> list = new List<string>();
        list.Add("/// <summary>");
        list.Add("\t\t/// "+ description);
        list.Add("\t\t/// </summary>");
        foreach (var item in Table.NonPrimaryKeyColumns)
        {
            list.Add(string.Format("\t\t/// <param name=\"{0}\">{1}</param>",  GetGetPropertyVar(item),GetDescription(item)));
        }
        list.Add("\t\t/// <returns></returns>");
        return string.Join("\n", list);
    }
    public string GetConstorCallArguments()
    {
        List<string> list = new List<string>();
        foreach (var item in Table.NonPrimaryKeyColumns)
        {
            list.Add(GetGetPropertyVar(item));
        }

        return string.Join(",", list);
    }
    public string GetCommandConstorCallArguments()
    {
        List<string> list = new List<string>();
        foreach (var item in Table.NonPrimaryKeyColumns)
        {
            list.Add("command."+GetPropertyName(item));
        }

        return string.Join(",", list);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static string GetTypeName(ColumnSchema column)
    {
        var type=column.SystemType;
        switch (column.SystemType.Name)
        {
            case "Guid":
            case "DateTime":
                return type.Name;
            case "Int32":
                return "int";
            case "Int64":
                return "long";
            case "Int16":
                return "short";
            case "Boolean":
                return "bool";
        }
        return type.Name.ToLower();
    }
    /// <summary>
    /// 获取主键
    /// </summary>
    /// <returns></returns>
    public string GetPrimaryKeyName()
    {
        if (this.Table.PrimaryKey.MemberColumns.Count == 0)
        {
            return string.Empty;
        }
        return GetPropertyName(this.Table.PrimaryKey.MemberColumns[0]);
    }
    public string GetPrimaryKeyNameVar()
    {
        string name = GetPrimaryKeyName();
        return char.ToLower(name[0]) + name.Substring(1);
    }
    public static string GetNullableColumnType(ColumnSchema column)
    {

        var theType = column.SystemType;
        var ret = (theType.IsGenericType && theType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)));
        if (ret == false && theType != typeof(string)&&column.AllowDBNull)
            return GetTypeName(column) + "?";
        return GetTypeName(column);
    }

    /// <summary>
    /// 获取主键数据类型
    /// </summary>
    /// <returns></returns>
    public string GetPrimaryKeyType()
    {
        if (this.Table.PrimaryKey.MemberColumns.Count == 0)
        {
            return string.Empty;
        }
        return this.Table.PrimaryKey.MemberColumns[0].SystemType.Name;
    }

    public static string GetNameSpace(string project,string layer,string module)
    {
        if (string.IsNullOrWhiteSpace(module))
        {
            return project + "." + layer;
        }
        return project + "." + layer + "." + module;
    }

    public string GetStringField()
    {
        foreach (var column in this.Table.Columns)
        {
            if (column.SystemType == typeof(string))
                return GetPropertyName(column);
        }
        return GetPropertyName(this.Table.Columns.Last());
    }
    public string GetOrderField()
    {
        foreach (var column in this.Table.Columns)
        {
            if (column.SystemType == typeof(DateTime))
                return GetPropertyName(column);
        }
        return GetPropertyName(this.Table.Columns.Last());
    }

    public static string GetVar(string name)
    {
        return char.ToLower(name[0]) + name.Substring(1);
    }
    
       public static string PascalCaseToUnderlineNameCase(string name, bool lower = true)
    {
        if (string.IsNullOrWhiteSpace(name))
            return string.Empty;
        StringBuilder builder = new StringBuilder(20);

        for (int i = 0; i < name.Length; i++)
        {
            if (char.IsUpper(name[i]) && char.IsLower(name[i+1])&&i>0)
            {
                builder.Append("_");
                builder.Append(name[i]);
            }
            else
            {
                builder.Append(name[i]);
            }

        }
        if (lower)
            return builder.ToString().ToLower();
        return builder.ToString().ToUpper();
    }

    public static string UnderlineNameCaseToPascalCase(string name)
    {
        if (string.IsNullOrWhiteSpace(name))
            return string.Empty;
        var words=name.Split('_').Select(x => x.Replace("_", "")).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
       return string.Join("", words.Select(x => x[0].ToString().ToUpper() + x.Substring(1).ToLower()));
    }
}