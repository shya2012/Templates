﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.UI;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 订单
    /// </summary>
   public class OrderPageParam:PageParam
    {
        /// <summary>
        /// 用户编号
        /// </summary>
        public  Guid? ResidentUserId{get;set;}
    }
}

