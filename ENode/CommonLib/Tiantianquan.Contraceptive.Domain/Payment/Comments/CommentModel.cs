﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Attributes;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 评论
    /// </summary>
   [Validator(typeof(CommentModelValidator))]
   public class CommentModel
    {
        /// <summary>
        /// 评论编号
        /// </summary>
        public  Guid CommentId{get;set;}
        /// <summary>
        /// 订单编号
        /// </summary>
        public  Guid? OrderId{get;set;}
        /// <summary>
        /// 用户编号
        /// </summary>
        public  Guid? ResidentUserId{get;set;}
        /// <summary>
        /// 头像
        /// </summary>
        public  String HeadImageUrl{get;set;}
        /// <summary>
        /// 昵称
        /// </summary>
        public  String FullName{get;set;}
        /// <summary>
        /// 手机号
        /// </summary>
        public  String Phone{get;set;}
        /// <summary>
        /// 评论内容
        /// </summary>
        public  String CommentContent{get;set;}
        /// <summary>
        /// 评论图片
        /// </summary>
        public  String CommentImages{get;set;}
        /// <summary>
        /// 评论时间
        /// </summary>
        public  DateTime? CreateTime{get;set;}
        /// <summary>
        /// 来源IP
        /// </summary>
        public  String IPAddress{get;set;}
    }
}

