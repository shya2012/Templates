﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 收货地址
    /// </summary>
   public class ShippingAddressDTO
    {
        /// <summary>
        /// 收货地址编号
        /// </summary>
        public  Guid ShippingAddressId{get;set;}
        /// <summary>
        /// 用户编号
        /// </summary>
        public  Guid? ResidentUserId{get;set;}
        /// <summary>
        /// FullName
        /// </summary>
        public  String FullName{get;set;}
        /// <summary>
        /// AddressName
        /// </summary>
        public  String AddressName{get;set;}
        /// <summary>
        /// AddressCode
        /// </summary>
        public  String AddressCode{get;set;}
        /// <summary>
        /// AddressStreet
        /// </summary>
        public  String AddressStreet{get;set;}
        /// <summary>
        /// Phone
        /// </summary>
        public  String Phone{get;set;}
    }
}

