﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 订单明细
    /// </summary>
   public class OrderDetailModelValidator:AbstractValidator<OrderDetailInfo>
    {
        public OrderDetailModelValidator()
        {




            RuleFor(x => x.ProductName).MaximumLength(128).WithMessage("商品名称最多128个字。");

            RuleFor(x => x.ProductImage).MaximumLength(256).WithMessage("商品图片最多256个字。");

            RuleFor(x => x.SkuProperty).MaximumLength(-1).WithMessage("Sku属性最多-1个字。");

            RuleFor(x => x.SkuPropertyValue).MaximumLength(256).WithMessage("Sku属性值最多256个字。");




        }
    }
}

