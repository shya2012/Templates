﻿//using NHibernate;
using DapperExtensions;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.UI
{
   public class PageParam
    {
        /// <summary>
        /// 页数
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// 分页大小
        /// </summary>
        public int Rows { get; set; }
        /// <summary>
        /// 模糊搜索
        /// </summary>
        public string Keywords { get; set; }
        /// <summary>
        /// 排序字段
        /// </summary>
        public string Sort { get; set; }
        /// <summary>
        /// 排序方向
        /// </summary>
        public string Order { get; set; }

        public PageParam()
        {
            this.Page = 1;
            this.Rows = 10;
            this.Keywords = string.Empty;
        }
        public int GetFirstResult()
        {
            int start= (this.Page - 1) * this.GetMaxResults();
            if (start < 0)
            {
                return start=0;
            }
            return start;
        }

        public int GetMaxResults()
        {
            if (this.Rows <= 0)
            {
                return 10;
            }

            return this.Rows;
        }

        public int GetStartRecordIndex()
        {
            return GetFirstResult();
        }

        public int GetEndRecordIndex()
        {
            return GetFirstResult() + GetMaxResults();
        }

        public const string Asc = "asc";

        public const string Desc = "desc";

        public string[] AscDict = new string[] {
            "asc",
            "ascending"
        };
        public string[] DescDict = new string[] {
            "desc",
            "descending"
        };

        public bool IsClientSort()
        {
            if (string.IsNullOrWhiteSpace(this.Order) || string.IsNullOrWhiteSpace(this.Sort))
            {
                return false;
            }
            if (!this.GetOrder().HasValue)
            {
                return false;
            }
            return true;
        }
        public ICriteria CreateOrderCriteria(ICriteria criteria)
        {
            if (!this.IsClientSort())
            {
                return criteria;
            }
            if (this.Order.ToLower() == Asc)
            {
                criteria.ClearOrders();
                criteria.AddOrder(NHibernate.Criterion.Order.Asc(this.Sort));
            }
            else if (this.Order.ToLower() == Desc)
            {
                criteria.ClearOrders();
                criteria.AddOrder(NHibernate.Criterion.Order.Desc(this.Sort));
            }
            return criteria;
        }

        public string GetClientOrderBy()
        {
            if (this.GetOrder().GetValueOrDefault())
            {
                this.Order = Asc;
            }
            else
            {
                this.Order = Desc;
            }
            return this.Sort + " " + this.Order;
        }
        public string GetClientOrderBy(string orderBy)
        {
            if (this.GetOrder().GetValueOrDefault())
            {
                this.Order = Asc;
            }
            else
            {
                this.Order = Desc;
            }

            if (this.IsClientSort())
                return this.Sort + " " + this.Order;
            return orderBy;
        }

        public bool? GetOrder()
        {
            if (string.IsNullOrWhiteSpace(this.Sort))
            {
                return null;
            }
            if (this.AscDict.Contains(this.Order.Trim().ToLower()))
            {
                return true;
            }
            if (this.DescDict.Contains(this.Order.Trim().ToLower()))
            {
                return false;
            }
            return null;
        }

        public ISort[] GetClientOrderBy(params ISort[] sorts)
        {
            List<ISort> results = new List<ISort>();
            if (this.IsClientSort())
            {
                return new[] { Predicates.Sort(this.Sort, this.GetOrder().GetValueOrDefault()) };
            }
            return sorts;
        }
    }
}
