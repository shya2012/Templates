﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.UI
{
    /// <summary>
    /// 树节点
    /// </summary>
    public class TreeNode
    {
        /// <summary>
        /// 无子节点
        /// </summary>
        public const string Open = "open";

        /// <summary>
        /// 有
        /// </summary>
        public const string Closed = "closed";
        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("id")]
        public object Id { get; set; }
        /// <summary>
        /// 文字
        /// </summary>
        [JsonProperty("text")]
        public string Text { get; set; }
        /// <summary>
        /// 状态，值为：open,closed
        /// </summary>
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 是否选择
        /// </summary>
        [JsonProperty("checked")]
        public bool Checked { get; set; }
        /// <summary>
        /// 其他属性
        /// </summary>
        [JsonProperty("attributes")]
        public object Attributes { get; set; }
        /// <summary>
        /// 子节点
        /// </summary>
        [JsonProperty("children")]
        public List<TreeNode> Children { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        [JsonProperty("iconCls")]
        public string IconCls { get; set; }

        /// <summary>
        /// target DOM object
        /// </summary>
        [JsonProperty("target")]
        public object Target { get; set; }

        /// <summary>
        /// target DOM object
        /// </summary>
        [JsonProperty("isLeaf")]
        public bool IsLeaf { get; set; }
    }
}
