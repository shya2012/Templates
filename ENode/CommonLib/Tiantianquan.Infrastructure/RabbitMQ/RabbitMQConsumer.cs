﻿using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace Tiantianquan.Infrastructure.RabbitMQ
{
    public class RabbitMQConsumer : IConsumer, IDisposable
    {
        private IConnection connection;
        private IModel channel;

        public RabbitMQConsumer(IMessageHandler messageHandler, RabbitMQOption option, ILogger<RabbitMQPublisher> logger)
        {
            Option = option;
            Logger = logger;
            MessageHandler = messageHandler;

            var factory = new ConnectionFactory() { HostName = Option.HostName };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();

            channel.QueueDeclare(queue: Option.Queue,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
        }

        public RabbitMQOption Option { get; private set; }

        public ILogger<RabbitMQPublisher> Logger { get; private set; }

        public IMessageHandler MessageHandler { get; private set; }

        public void Dispose()
        {
            this.Stop();
        }

        public void Start(object state)
        {
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;

                var message = Encoding.UTF8.GetString(body);
                int pos = message.IndexOf("|");
                string lable = message.Substring(0, pos);
                string json = message.Substring(pos + 1);

                Logger.LogInformation("收到消息：{0},{1}", lable, json);
                MessageHandler.Handle(lable, json);
            
            };
            channel.BasicConsume(queue: Option.Queue,
                                       autoAck: true,
                                       consumer: consumer);
        }

        public void Stop()
        {
            channel.Close();
            connection.Close();
        }
    }
}