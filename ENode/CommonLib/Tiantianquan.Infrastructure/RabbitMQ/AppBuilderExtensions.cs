﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;
using Tiantianquan.Infrastructure.RabbitMQ;

namespace Microsoft.AspNetCore.Builder
{
    public static partial class AppBuilderExtensions
    {
      

        public static IApplicationBuilder UseRabbitMQ(this IApplicationBuilder app)
        {
            IConsumer  consumer= app.ApplicationServices.GetService<IConsumer>();
            ThreadPool.QueueUserWorkItem(new WaitCallback(consumer.Start),new object());
            return app;
        }
    }
}
