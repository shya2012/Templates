﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.WebApi.Commons;

namespace Tiantianquan.Infrastructure.WebApi.Filters
{
    public class ValidatorFilterAttribute : ResultFilterAttribute
    {
      

        public override void OnResultExecuting(ResultExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                string message = context.ModelState.First(x => x.Value.Errors.Count > 0).Value.Errors.First().ErrorMessage;
                context.Result = new Result(false, message, context.ModelState);
                return;
            }
            base.OnResultExecuting(context);
        }
       
    }
    

   
}
