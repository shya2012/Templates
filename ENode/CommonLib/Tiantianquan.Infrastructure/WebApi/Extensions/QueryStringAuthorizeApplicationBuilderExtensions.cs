﻿using System;
using System.Collections.Generic;
using System.Text;
using Tiantianquan.Infrastructure.WebApi.Middlewares;

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// 中间件扩展
    /// </summary>
    public static partial class QueryStringAuthorizeApplicationBuilderExtensions
    {
        /// <summary>
        /// 注册错误日志管道
        /// </summary>
        /// <param name="builder">应用程序生成器</param>
        public static IApplicationBuilder UseQueryStringAuthorize(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<QueryStringAuthorizeMiddleware>();
        }
    }
}