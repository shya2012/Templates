﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApiClient;

namespace Tiantianquan.Infrastructure.Services
{


    public class ChinaMobileSmsService
    {
        public ChinaMobileSmsService(IMemoryCache memoryCache, IConfiguration configuration, ILogger<ChinaMobileSmsService> logger)
        {
            MemoryCache = memoryCache;
            this.MasUrl = configuration.GetSection("YunMas").GetValue<string>("MasUrl");
            this.ECName = configuration.GetSection("YunMas").GetValue<string>("ECName");
            this.UserName = configuration.GetSection("YunMas").GetValue<string>("UserName");
            this.UserPasswd = configuration.GetSection("YunMas").GetValue<string>("UserPasswd");
            this.Sign = configuration.GetSection("YunMas").GetValue<string>("Sign");
            this.yunMasWebApi = HttpApiClient.Create<IYunMasWebApi>(this.MasUrl);
            Logger = logger;

        }

        protected IMemoryCache MemoryCache { get; private set; }

        protected ILogger<ChinaMobileSmsService> Logger { get; private set; }


        public string PlatformName = "中国移动";

        /// <summary>
        /// 云MAS地址
        /// </summary>
        private string MasUrl = "";
        private string ECName = "";
        /// <summary>
        /// 账号
        /// </summary>
        private string UserName = "";
        /// <summary>
        /// 面貌
        /// </summary>
        private string UserPasswd = "";
        /// <summary>
        /// 签名
        /// </summary>
        private string Sign = "";

        private readonly IYunMasWebApi yunMasWebApi;

        const string SMSAccessTokenKey = "SMSAccessToken";

        public async Task<SMSResult> SendSms(string phones, string content)
        {
            SMSResult result  = new SMSResult();
            try
            {
                SMSAccessToken token = await this.GetSMSAccessToken();
                if (token == null)
                {
                    throw new FriendlyException("短信通道故障！");
                }
                SMSParameter SMSParameter = new SMSParameter(token, this.Sign, "", phones, content);

                // string sss = await this.yunMasWebApi.SendSms(SMSParameter.MasUserId, SMSParameter.Mobiles, SMSParameter.Content, SMSParameter.Sign, SMSParameter.Serial, SMSParameter.Mac);
                string json = await this.yunMasWebApi.SendSms(SMSParameter);
                
                if (json.IndexOf("\"RET-CODE\":\"SC:0000\"") > -1)
                {
                    result.RetCode = "OK";
                    result.RetDesc = "发送成功";
                }
                else
                {
                    Logger.LogError("发送短信token：" + token.ToJson(false, true));
                    Logger.LogError("发送短信参数：" + SMSParameter.ToJson(false, true));
                    Logger.LogError("发送短信结果:{0}", json);

                    JObject obj=JObject.Parse(json);
                    result.RetCode =obj.GetValue("RET-CODE").Value<string>();
                }
              
               // result = JsonConvert.DeserializeObject<SMSResult>(json);
                Console.WriteLine("---------------短信发送结果---------");
                Console.WriteLine(json.ToJson(false,true));
            }
            catch (Exception ex)
            {
                result.RetCode = ex.Message;
                result.RetDesc = ex.Message;
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }

            return result;
        }

        private async Task<SMSAccessToken> GetSMSAccessToken()
        {
            SMSAccessToken token = this.MemoryCache.Get<SMSAccessToken>(SMSAccessTokenKey);
            if (token == null 
                || string.IsNullOrWhiteSpace(token.AccessToken) 
                || string.IsNullOrWhiteSpace(token.MasUserId))
            {
                string json = await this.yunMasWebApi.Authorize(new SMSLoginParameter(this.ECName, this.UserName, this.UserPasswd));
                Logger.LogInformation("发送短信获取token:{0}",json);
                token = JsonConvert.DeserializeObject<SMSAccessToken>(json);
                this.MemoryCache.Set<SMSAccessToken>(SMSAccessTokenKey, token, TimeSpan.FromSeconds(3500));
            }

            return token;
        }

    }
}
