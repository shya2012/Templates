﻿using WebApiClient;
using WebApiClient.Attributes;

namespace Tiantianquan.Infrastructure.Services
{
    public interface IYunMasWebApi : IHttpApiClient
    {
        // GET webapi/user?account=laojiu
        // Return 原始string内容
        [HttpPost("/app/http/authorize")]
        ITask<string> Authorize([PathQuery]SMSLoginParameter parameter);

        // POST webapi/user  
        // Body Account=laojiu&password=123456
        // Return json或xml内容
        [HttpPost("/app/http/sendSms")]
        ITask<string> SendSms([PathQuery]SMSParameter parameter);
        // ITask<string> SendSms(string mas_user_id, string mobiles, string content, string sign, string serial, string mac);
    }
}
