﻿using WebApiClient.DataAnnotations;

namespace Tiantianquan.Infrastructure.Services
{
    public class SMSResult
    {
        [AliasAs("RET-CODE")]
        public string RetCode { get; set; }

        [AliasAs("RET-DESC")]
        public string RetDesc { get; set; }

        [AliasAs("MSG-GROUP")]
        public string MsgGroup { get; set; }

        public const string 请求成功 = "SC:0000";
    }
}
