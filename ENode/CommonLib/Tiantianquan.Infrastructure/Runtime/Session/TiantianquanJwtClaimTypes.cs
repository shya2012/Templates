﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.Runtime.Session
{
    public class TiantianquanJwtClaimTypes
    {
        public const string UserId = "UserId";
        public const string UserName = "UserName";
        public const string NickName = "NickName";
        public const string FullName = "FullName";
        public const string Phone = "Phone";
        public const string HeadImageUrl = "HeadImageUrl";
        public const string Gender = "Gender";
        public const string Email = "Email";
        public const string OrganizationId = "OrganizationId";
        public const string OrganizationName = "OrganizationName";
    }
}
