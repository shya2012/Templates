﻿using FluentValidation.Validators;

namespace Tiantianquan.Infrastructure.Validation
{
    public class SMSCodePropertyValidator : PropertyValidator
    {
        public SMSCodePropertyValidator() : base("短信验证码为6位数字。")
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (string.IsNullOrWhiteSpace(context.PropertyValue as string))
            {
                return true;
            }
            return Utils.RegexUtils.Validators.SMSCode.IsMatch(context.PropertyValue as string);
        }
    }
}
