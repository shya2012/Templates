﻿using DapperExtensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Domain;

namespace Tiantianquan.Infrastructure.Dapper
{
    public interface IDapperRepository<T> where T : EntityBase
    {
        bool Delete(T entity);
        Task<bool> DeleteAsync(T entity);
        T Get(object id);
        Task<T> GetAsync(object id);
        void Insert(T entity);
        Task InsertAsync(T entity);
        bool Update(T entity);
        Task<bool> UpdateAsync(T entity);

        List<T> GetAll(object predicate = null, IList<ISort> sort = null);

        Task<List<T>> GetAllAsync(object predicate = null, IList<ISort> sort = null);
    }
}