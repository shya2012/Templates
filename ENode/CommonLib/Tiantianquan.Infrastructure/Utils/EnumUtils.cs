﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.Utils
{
    public static class EnumUtils
    {
        public static List<EnumInfo> GetEnumInfo(Type type)
        {

            var names = Enum.GetNames(type);

            List<EnumInfo> list = new List<EnumInfo>();
            foreach (string name in names)
            {
                int value = (int)Enum.Parse(type, Convert.ToString(name));
                string description = EnumUtils.GetDescription(type, value);
                list.Add(new EnumInfo(name, description, value));
            }
            return list;
        }

        public static string GetJavascript(Type type)
        {
            List<EnumInfo> list = EnumUtils.GetEnumInfo(type);
            string description = type.Name;
            if (type.IsDefined(typeof(DescriptionAttribute), false))
            {
                description = type.GetCustomAttribute<DescriptionAttribute>().Description;
            }
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendFormat("/*{0}(命名空间：{1})*/\r\n", description,type.FullName);
            sb.AppendFormat("enums.{0}", type.Name).AppendLine(" = {");
            sb.AppendLine("    const: {");
            int counter = 1;
            foreach (EnumInfo ei in list)
            {
                sb.Append("        /*").Append(ei.Description).Append("*/").AppendLine();
                sb.Append("        ").Append(ei.Name).Append(" : ").Append(ei.Value);
                if (counter < list.Count)
                {
                    sb.Append(",");
                }
                sb.AppendLine();
                counter++;
            }
            sb.AppendLine("    },");
            sb.AppendLine("    items: [");
            counter = 1;
            foreach (EnumInfo ei in list)
            {
                sb.AppendLine("        {");
                sb.AppendFormat("            value: {0},\r\n", ei.Value);
                sb.AppendFormat("            text: '{0}'\r\n", ei.Description);
                sb.Append("        }");
                if (counter < list.Count)
                {
                    sb.Append(",");
                }
                sb.AppendLine();
                counter++;
            }

            sb.AppendLine("    ],");
            sb.AppendLine("    getText: function (value) {");
            sb.AppendLine("        var text = '';");
            sb.AppendLine("        switch (value) {");
            foreach (var ei in list)
            {
                sb.AppendFormat("            case {0}:\r\n", ei.Value);
                sb.AppendFormat("                text = '{0}';\r\n", ei.Description);
                sb.AppendLine("                break;");
            }
            sb.AppendLine("            default:");
            sb.AppendLine("                break;");
            sb.AppendLine("        }");
            sb.AppendLine("        return text;");
            sb.AppendLine("    }");
            sb.AppendLine("};");
            return sb.ToString();
        }

        public static string GetDescription<T>(int value)
        {
            return EnumUtils.GetDescription(typeof(T), value);
        }

        public static string GetDescription(Type type, int value)
        {
            string name = Enum.GetName(type, value);
            FieldInfo field = type.GetField(name);
            if (field == null)
                return string.Empty;
            if (!field.IsDefined(typeof(DescriptionAttribute)))
            {
                return name;
            }
            return field.GetCustomAttribute<DescriptionAttribute>().Description;
        }

    }

    public class EnumInfo
    {
        public EnumInfo(string name, string description, object value)
        {
            Name = name;
            Description = description;
            Value = value;
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public object Value { get; set; }
    }
}
