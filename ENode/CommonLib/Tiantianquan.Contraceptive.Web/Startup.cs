﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Tiantianquan.Infrastructure;
using Tiantianquan.Infrastructure.Dependency;
using Tiantianquan.Infrastructure.WebApi.Filters;
using UEditorNetCore;

namespace Tiantianquan.Contraceptive.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDistributedMemoryCache();
            services.AddSession();


            //services.AddUEditorService();
            // services.AddSwaggerX(@"bin\Debug\netcoreapp2.1\Tiantianquan.Weixiaobang.Web.xml", "weixiaobang", "weixiaobang restful api");
            services.AddAbpSession();
            services.AddLogging();
            services.AddDefaultFileStore();
            services.AddCaptchaService();
            services.AddMemoryCache();
            services.AddChinaMobileSMSService();

            services.AddDynamicScripts(Assembly.Load("Tiantianquan.Contraceptive.Domain"));

      
            services.AddUEditorService();

            services.RegisterPaymentModule();
            services.AddMvc(options => {
                options.Filters.Add<ValidatorFilterAttribute>();
            }).AddFluentValidation(fv => {
                fv.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
                fv.ImplicitlyValidateChildProperties = true;
                fv.RegisterValidatorsFromAssembly(Assembly.Load("Tiantianquan.Contraceptive.Domain"));

            }).AddJsonOptions(options => {
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
                options.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
                //options.SerializerSettings.DateFormatString = "yyyyMMddhhMMss";
                options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Include;

            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            IocManager.Initialize(app.ApplicationServices);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseSession();
            DefaultFilesOptions defaultFilesOptions = new DefaultFilesOptions(); ;
            defaultFilesOptions.DefaultFileNames.Add("/index.html");
            app.UseDefaultFiles(defaultFilesOptions);
            app.UseStaticFiles();
            app.UseAuthentication();
            // app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
