using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using Tiantianquan.Infrastructure.IdentityServer;

namespace Tiantianquan.SmartCA.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            #region 配置Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "TwBusManagement接口文档",
                    Description = "RESTful API for TwBusManagement",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "Alvin_Su",
                        Email = "asdasdasd@outlook.com",
                        Url = ""
                    }
                });

                //Set the comments path for the swagger json and ui.
                // var basePath = Environment.CurrentDirectory;
                //  var xmlPath = Path.Combine(basePath, "Tiantianquan.SmartCA.WebApi.xml");
                //  c.IncludeXmlComments(xmlPath);

                //  c.OperationFilter<HttpHeaderOperation>(); // 添加httpHeader参数
            });
            #endregion

            services.AddMvcCore()
          .AddAuthorization()
          .AddJsonFormatters();

            services.AddLogging();

            //string PrivateKey = Configuration["RSA"];
            //RSA：证书长度2048以上，否则抛异常
            //配置AccessToken的加密证书
            //var rsa = new RSACryptoServiceProvider();
            //从配置文件获取加密证书
            //rsa.ImportCspBlob(Convert.FromBase64String(PrivateKey));

            #region 配置授权中心
            services.AddIdentityServer()
   .AddDeveloperSigningCredential()
   //.AddSigningCredential(new RsaSecurityKey(rsa))
   .AddInMemoryApiResources(Config.GetApiResources())
   .AddInMemoryClients(Config.GetClients())
  .AddTestUsers(Config.GetUsers());
            //.AddResourceOwnerValidator<ResourceOwnerPasswordValidator>();
            #endregion



            #region 链接授权中心
            services.AddAuthentication("Bearer")
     .AddIdentityServerAuthentication(options =>
     {
         options.Authority = "http://localhost:25880";
         options.RequireHttpsMetadata = false;

         options.ApiName = "api1";
     });
            #endregion

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger(c=> {  });
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "TwBusManagement API V1");
                    //c.ShowRequestHeaders();
                });
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            loggerFactory.AddConsole();

            #region 授权服务器
            app.UseIdentityServer();
            #endregion


            #region WEBAPI访问控制

            app.UseAuthentication();
            #endregion

            app.UseMvc();
        }
    }
}
