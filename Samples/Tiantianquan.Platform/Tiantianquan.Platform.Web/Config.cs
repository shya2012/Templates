﻿using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;

namespace Tiantianquan.Platform.Web
{
    public class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResource(IdentityServerConstants.StandardScopes.OpenId,"用户信息",new[]{
                    JwtClaimTypes.Subject
                }){ Required=true },
                new IdentityResource("tiantianquan","昵称,头像,单位信息等。",new []{
                    JwtClaimTypes.NickName,
                    JwtClaimTypes.Name,
                    JwtClaimTypes.PhoneNumber,
                    JwtClaimTypes.Picture
                }) { Required = true }
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("zhxy", "智慧校园"){
                    Scopes=new List<Scope>(){
                         new Scope("zhxy","智慧校园",new []{
                                  JwtClaimTypes.NickName,
                    JwtClaimTypes.Name,
                    JwtClaimTypes.PhoneNumber,
                    JwtClaimTypes.Picture
                         })
                    }
                },
                new ApiResource("zcgl", "资产管理")
                {
                      Scopes=new List<Scope>(){
                         new Scope("zcgl","资产管理",new []{
                                JwtClaimTypes.NickName,
                    JwtClaimTypes.Name,
                    JwtClaimTypes.PhoneNumber,
                    JwtClaimTypes.Picture
                         })
                    }
                }
            };
        }

        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "alice",
                    Password = "password"
                },
                new TestUser
                {
                    SubjectId = "2",
                    Username = "bob",
                    Password = "password"
                }
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                // other clients omitted...

                // resource owner password grant client
                new Client
                {
                    ClientId = "zhxy.weixiaobang.cn",
                    ClientName = "教育教学系统",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris =           { "http://localhost:8182/callback.html" },
                    PostLogoutRedirectUris = { "http://localhost:8182/default.aspx" },
                    AllowedCorsOrigins =     { "http://localhost:8182" },

                    ClientUri="http://localhost:8182",
                    LogoUri="http://localhost:5000/images/logos/zhxy.png",

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        "tiantianquan",
                        "zhxy"
                    }
                },
                new Client
                {
                    ClientId = "zcgl.weixiaobang.cn",
                    ClientName = "资产管理系统",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris =           { "http://localhost:5003/callback.html" },
                    PostLogoutRedirectUris = { "http://localhost:5003/index.html" },
                    AllowedCorsOrigins =     { "http://localhost:5003" },

                    ClientUri="http://localhost:5003",
                    LogoUri="http://localhost:5000/images/logos/zcgl.png",

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        "tiantianquan",
                        "zcgl"
                    }
                }
            };
        }
    }
}