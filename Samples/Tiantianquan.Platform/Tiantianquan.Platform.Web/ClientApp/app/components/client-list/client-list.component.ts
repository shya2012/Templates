import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';


@Component({
    selector: 'client-list',
    templateUrl: './client-list.component.html'
})
export class ClientListComponent {
    public forecasts: WeatherForecast[];

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string) {
        http.get(baseUrl + 'api/Client/GetClients').subscribe(result => {
            this.forecasts = result.json() as WeatherForecast[];
        }, error => console.error(error));
    }
}

interface WeatherForecast {
    clientId: string;
    clientName: number;
    clientUri: number;
    logoUri: string;
}
