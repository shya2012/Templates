﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using Tiantianquan.Platform.Domain;

namespace Tiantianquan.Platform.Repository
{
    public class ApplicationDbContext:IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

          //  modelBuilder.Entity<ApplicationUser>().ToTable("PRIVATE_APPLICATION_USER");
          //  modelBuilder.Entity<IdentityRole>().ToTable("PRIVATE_APPLICATION_ROLE");

         //   modelBuilder.Entity<NavigationMenu>().ToTable("PRIVATE_NAVIGATION_MENU");
                
                
        }
    }
}
