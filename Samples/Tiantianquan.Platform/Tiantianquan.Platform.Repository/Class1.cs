﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using Tiantianquan.Platform.Domain;

namespace Tiantianquan.Platform.Repository
{
    public class ApplicationDbContext111111111111:DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>().ToTable("PRIVATE_APPLICATION_USER");
            modelBuilder.Entity<IdentityRole>().ToTable("PRIVATE_APPLICATION_ROLE");

            modelBuilder.Entity<NavigationMenu>(menu =>
            {
                menu.ToTable("PRIVATE_NAVIGATION_MENU");

                menu.HasKey(x => x.NavigationMenuId);

                menu.Property(x => x.NavigationMenuId).IsRequired();
                menu.Property(x => x.ParentNavigationMenuId);

                menu.Property(item => item.Name).HasColumnName("NAME").IsRequired().HasMaxLength(128);
                menu.Property(item => item.DisplayName).HasColumnName("DISPLAY_NAME").IsRequired().HasMaxLength(128);
                menu.Property(item => item.LinkAddress).HasColumnName("LINK_ADDRESS").IsRequired().HasMaxLength(256).HasDefaultValue("");
                menu.Property(item => item.Attributes).HasColumnName("ATTRIBUTES").HasMaxLength(1024).HasDefaultValue("{}");
                menu.Property(item => item.Remarks).HasDefaultValue("REMARKS").HasMaxLength(256);
                menu.Property(item => item.Style).HasMaxLength(128).HasDefaultValue("");

                menu.HasIndex(x => x.Name).IsUnique();
            });
                
                
        }
    }
}
