﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tiantianquan.Platform.Domain
{
    public class ApiResource : IdentityServer4.Models.ApiResource
    {
        //
        // 摘要:
        //     Indicates if this resource is enabled. Defaults to true.
        [Column("ENABLED")]
        public new bool Enabled { get; set; }
        //
        // 摘要:
        //     The unique name of the resource.
        [Column("NAME")]
        public new string Name { get; set; }
        //
        // 摘要:
        //     Display name of the resource.
        [Column("DISPLAY_NAME")]
        public new string DisplayName { get; set; }
        //
        // 摘要:
        //     Description of the resource.
        [Column("DESCRIPTION")]
        public new string Description { get; set; }
    }

    public class ApplicationUser: IdentityUser
    {

    }

    public class ApplicationRole : IdentityRole
    {

    }

    public class NavigationMenu
    {
        public virtual Guid NavigationMenuId { get; set; }

        [Column("NAME")]
        public virtual string Name { get; set; }

        public virtual string DisplayName { get; set; }

        public virtual string LinkAddress { get; set; }

        public virtual string Style { get; set; }

        public virtual Guid ParentNavigationMenuId { get; set; }

        public virtual string Remarks { get; set; }

        public virtual string Attributes { get; set; }
    }

    public class Permission
    {
        public virtual Guid PermissionId { get; set; }

        public virtual string ClientId { get; set; }

        public virtual string RoleId { get; set; }

        public virtual string UserId { get; set; }

        public virtual Guid NavigationMenuId { get; set; }

        public virtual bool IsGranted { get; set; }

        public virtual DateTime GrantedTime { get; set; }

        public virtual Guid? GrantedUserId { get; set; }
    }
}
