﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Tiantianquan.Ebook.Web.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string IdCard { get; set; }

        public string FullName { get; set; }

        public string BlankCardNo { get; set; }

        public string Location { get; set; }
    }
}
