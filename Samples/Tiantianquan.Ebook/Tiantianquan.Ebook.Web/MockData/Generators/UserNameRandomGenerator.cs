﻿using NPinyin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.MockData.Core;
using Tiantianquan.Infrastructure.Utils;

namespace Tiantianquan.Infrastructure.MockData.Generators
{
    public class UserNameRandomGenerator : RandomGeneratorBase, IRandomGenerator
    {
        /// <summary>
        /// 实例
        /// </summary>
        public static readonly UserNameRandomGenerator Instance = new UserNameRandomGenerator();


        public override List<string> BatchGenerate(int maxLength)
        {
            List<string> list = new List<string>();
            for (int i = 0; i < maxLength; i++)
            {
               
                list.Add(Generate());
            }

            return list;
        }

        public override string Generate()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Pinyin.GetPinyin(ChineseNameRandomGenerator.Instance.Generate()).Replace(" ", ""));
            DateTime birthday = RandomUtils.RandomDateTime(DateTime.Now.AddYears(-50), DateTime.Now.AddYears(7));
            switch (RandomUtils.RandomInt(1, 5))
            {
                case 1:
                    sb.Append(birthday.ToString("yyyy"));
                    break;
                case 2:
                    sb.Append(birthday.ToString("yyyyMM"));
                    break;
                case 3:
                    sb.Append(birthday.ToString("yyyyMMdd"));
                    break;
                default:
                    break;
            }
            return sb.ToString();
        }
    }
}
