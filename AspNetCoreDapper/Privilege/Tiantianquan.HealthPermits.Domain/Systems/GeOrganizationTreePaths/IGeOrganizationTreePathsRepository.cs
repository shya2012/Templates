﻿using System.Collections.Generic;
using Tiantianquan.Common.Enums;
using Tiantianquan.Common.Repositories;
using Tiantianquan.Common.UI;
using System;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 机构树
    /// </summary>
    public interface IGeOrganizationTreePathsRepository : IRepository<GeOrganizationTreePaths>
    {
     /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchParams">分页查询条件</param>
        /// <returns></returns>
        List<GeOrganizationTreePaths> GetGeOrganizationTreePathsByPage(GeOrganizationTreePathsQueryModel search);
        
         /// <summary>
        /// 查总记录数
        /// </summary>
        /// <param name="searchParams"></param>
        /// <returns></returns>
        int GetGeOrganizationTreePathsRecordCount(GeOrganizationTreePathsQueryModel search);
    }
}
