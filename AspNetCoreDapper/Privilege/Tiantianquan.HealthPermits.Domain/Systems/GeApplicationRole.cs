﻿using System;
using System.ComponentModel;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 角色信息
    /// </summary>
    [Description("角色信息")]
    public class GeApplicationRole : EntityBase
    {
        /// <summary>
        /// 角色编号
        /// </summary>
        [Description("角色编号")]
        public virtual Guid   ApplicationRoleId {get;set;}
        /// <summary>
        /// 角色名称
        /// </summary>
        [Description("角色名称")]
        public virtual String   RoleName {get;set;}
        /// <summary>
        /// 角色描述
        /// </summary>
        [Description("角色描述")]
        public virtual String   RoleDescription {get;set;}
        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        public virtual DateTime   CreationTime {get;set;}
        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        public virtual DateTime ?  ModifyTime {get;set;}
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeApplicationRole()
        {
            RoleName = String.Empty;
            RoleDescription = String.Empty;
            CreationTime = DateTime.Now;
        }
    }
}

