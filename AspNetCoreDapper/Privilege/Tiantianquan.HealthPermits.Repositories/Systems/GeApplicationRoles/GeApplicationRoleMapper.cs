﻿using System;
using System.ComponentModel;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 角色信息
    /// </summary>
    public class GeApplicationRoleMapper : ClassMapper<GeApplicationRole>
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeApplicationRoleMapper()
        {
            this.Table("PRIVILEGE_APPLICATION_ROLE");
            #region 主键
            this.Map(x => x.ApplicationRoleId).Column("APPLICATION_ROLE_ID").Key(KeyType.Assigned);    
            #endregion
            #region 外键
            #endregion
            #region 其他
            this.Map(x => x.RoleName).Column("ROLE_NAME");
            this.Map(x => x.RoleDescription).Column("ROLE_DESCRIPTION");
            this.Map(x => x.CreationTime).Column("CREATION_TIME");
            this.Map(x => x.ModifyTime).Column("MODIFY_TIME");
            #endregion
            this.AutoMap();
        }
    }
}

