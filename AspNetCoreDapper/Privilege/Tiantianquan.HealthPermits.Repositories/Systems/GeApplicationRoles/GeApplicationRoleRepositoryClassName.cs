﻿using Abp.Web;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.HealthPermits.Domain.Entities.Systems;
using Tiantianquan.HealthPermits.Domain.IRepositories.Systems;

namespace Tiantianquan.HealthPermits.NHibernate.Repositories.Systems
{
    /// <summary>
    /// 角色信息
    /// </summary>
    public  class GeApplicationRoleRepositoryClassName : RepositoryImpl<GeApplicationRole>, IGeApplicationRoleRepository
    {
       
    }
}