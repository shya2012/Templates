﻿using System;
using System.ComponentModel;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 机构树
    /// </summary>
    public class GeOrganizationTreePathsMapper : ClassMapper<GeOrganizationTreePaths>
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeOrganizationTreePathsMapper()
        {
            this.Table("PRIVILEGE_ORGANIZATION_TREE_PATHS");
            #region 主键
            this.Map(x => x.AncestorOrganizationId).Column("ANCESTOR_ORGANIZATION_ID").Key(KeyType.Assigned);    
            this.Map(x => x.DescendantOrganizationId).Column("DESCENDANT_ORGANIZATION_ID").Key(KeyType.Assigned);    
            #endregion
            #region 外键
            this.Map(x => x.AncestorOrganizationId).Column("ANCESTOR_ORGANIZATION_ID");
            this.Map(x => x.DescendantOrganizationId).Column("DESCENDANT_ORGANIZATION_ID");
            #endregion
            #region 其他
            this.Map(x => x.OrganizationDeep).Column("ORGANIZATION_DEEP");
            #endregion
            this.AutoMap();
        }
    }
}

