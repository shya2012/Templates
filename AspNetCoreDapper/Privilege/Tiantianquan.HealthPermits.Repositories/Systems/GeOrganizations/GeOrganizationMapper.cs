﻿using System;
using System.ComponentModel;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 组织机构
    /// </summary>
    public class GeOrganizationMapper : ClassMapper<GeOrganization>
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeOrganizationMapper()
        {
            this.Table("PRIVILEGE_ORGANIZATION");
            #region 主键
            this.Map(x => x.OrganizationId).Column("ORGANIZATION_ID").Key(KeyType.Assigned);    
            #endregion
            #region 外键
            this.Map(x => x.ParentOrganizationId).Column("PARENT_ORGANIZATION_ID");
            #endregion
            #region 其他
            this.Map(x => x.OrganizationCode).Column("ORGANIZATION_CODE");
            this.Map(x => x.OrganizationName).Column("ORGANIZATION_NAME");
            this.Map(x => x.OrganizationPinyin).Column("ORGANIZATION_PINYIN");
            this.Map(x => x.OrganizationClasses).Column("ORGANIZATION_CLASSES");
            this.Map(x => x.OrganizationAddress).Column("ORGANIZATION_ADDRESS");
            this.Map(x => x.FormsOfOwnership).Column("FORMS_OF_OWNERSHIP");
            this.Map(x => x.Beds).Column("BEDS");
            this.Map(x => x.ServiceObject).Column("SERVICE_OBJECT");
            this.Map(x => x.MedicalSubjects).Column("MEDICAL_SUBJECTS");
            this.Map(x => x.TotalAmountOfInvestment).Column("TOTAL_AMOUNT_OF_INVESTMENT");
            this.Map(x => x.RegisteredCapital).Column("REGISTERED_CAPITAL");
            this.Map(x => x.BusinessNature).Column("BUSINESS_NATURE");
            this.Map(x => x.Other).Column("OTHER");
            this.Map(x => x.ValidityOfCertificate).Column("VALIDITY_OF_CERTIFICATE");
            this.Map(x => x.ApprovalNumber).Column("APPROVAL_NUMBER");
            this.Map(x => x.OrganizationTelphone).Column("ORGANIZATION_TELPHONE");
            #endregion
            this.AutoMap();
        }
    }
}

