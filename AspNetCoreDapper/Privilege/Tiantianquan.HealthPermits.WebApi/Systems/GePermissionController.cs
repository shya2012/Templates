﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tiantianquan.Common.UI;
using Tiantianquan.HealthPermits.Domain.Systems;
using Tiantianquan.HealthPermits.Application.Systems;

namespace Tiantianquan.HealthPermits.WebApi.Controllers.Systems
{
     /// <summary>
    /// 权限信息
    /// </summary>
    [Au]
    [Route("/Systems/{controller}/{action}")]
    public class GePermissionController:ApiController
    {
        public  GePermissionService  GePermissionService { get;private set; }

        public GePermissionController(GePermissionService gePermissionService)
        {
            this.GePermissionService = gePermissionService;
        }
        
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchParams"></param>
        /// <returns></returns>
        [HttpPost]
        public PageResult<GePermission> GetGePermissionByPage([FromBody]GePermissionQueryModel search)
        {
            return this.GePermissionService.GetGePermissionByPage(search);
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public GePermissionModel GetGePermissionById(Guid permissionId)
        {
            return this.GePermissionService.GetGePermissionById(permissionId);
        }
        /// <summary>
        /// 新增
        /// </summary>
        [HttpPost]
        public void Post([FromBody]GePermissionModel model)
        {
           this.GePermissionService.Save(model);
        }
        /// <summary>
        /// 修改
        /// </summary>
        [HttpPost]
        public void Put(Guid permissionId,[FromBody]GePermissionModel model)
        {
            this.GePermissionService.Update(permissionId, model);
        }
        /// <summary>
        /// 删除
        /// </summary>
        [HttpPost]
        public void Delete(Guid permissionId)
        {
            this.GePermissionService.Delete(permissionId);
        }
       
    }
}


