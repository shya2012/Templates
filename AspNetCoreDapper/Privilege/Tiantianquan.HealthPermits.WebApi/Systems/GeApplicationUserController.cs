﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tiantianquan.Common.UI;
using Tiantianquan.HealthPermits.Domain.Systems;
using Tiantianquan.HealthPermits.Application.Systems;

namespace Tiantianquan.HealthPermits.WebApi.Controllers.Systems
{
     /// <summary>
    /// 用户信息
    /// </summary>
    [Au]
    [Route("/Systems/{controller}/{action}")]
    public class GeApplicationUserController:ApiController
    {
        public  GeApplicationUserService  GeApplicationUserService { get;private set; }

        public GeApplicationUserController(GeApplicationUserService geApplicationUserService)
        {
            this.GeApplicationUserService = geApplicationUserService;
        }
        
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchParams"></param>
        /// <returns></returns>
        [HttpPost]
        public PageResult<GeApplicationUser> GetGeApplicationUserByPage([FromBody]GeApplicationUserQueryModel search)
        {
            return this.GeApplicationUserService.GetGeApplicationUserByPage(search);
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public GeApplicationUserModel GetGeApplicationUserById(Guid applicationUserId)
        {
            return this.GeApplicationUserService.GetGeApplicationUserById(applicationUserId);
        }
        /// <summary>
        /// 新增
        /// </summary>
        [HttpPost]
        public void Post([FromBody]GeApplicationUserModel model)
        {
           this.GeApplicationUserService.Save(model);
        }
        /// <summary>
        /// 修改
        /// </summary>
        [HttpPost]
        public void Put(Guid applicationUserId,[FromBody]GeApplicationUserModel model)
        {
            this.GeApplicationUserService.Update(applicationUserId, model);
        }
        /// <summary>
        /// 删除
        /// </summary>
        [HttpPost]
        public void Delete(Guid applicationUserId)
        {
            this.GeApplicationUserService.Delete(applicationUserId);
        }
       
    }
}


