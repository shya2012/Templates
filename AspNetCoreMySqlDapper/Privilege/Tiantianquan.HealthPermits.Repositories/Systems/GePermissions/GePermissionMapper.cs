﻿using System;
using System.ComponentModel;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 权限信息
    /// </summary>
    public class GePermissionMapper : ClassMapper<GePermission>
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public GePermissionMapper()
        {
            this.Table("PRIVILEGE_PERMISSION");
            #region 主键
            this.Map(x => x.PermissionId).Column("PERMISSION_ID").Key(KeyType.Assigned);    
            #endregion
            #region 外键
            this.Map(x => x.NavigationMenuId).Column("NAVIGATION_MENU_ID");
            this.Map(x => x.ApplicationRoleId).Column("APPLICATION_ROLE_ID");
            this.Map(x => x.ApplicationUserId).Column("APPLICATION_USER_ID");
            #endregion
            #region 其他
            this.Map(x => x.IsGranted).Column("IS_GRANTED");
            this.Map(x => x.CreationTime).Column("CREATION_TIME");
            #endregion
            this.AutoMap();
        }
    }
}

