﻿using System;
using System.ComponentModel;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 用户信息
    /// </summary>
    public class GeApplicationUserMapper : ClassMapper<GeApplicationUser>
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeApplicationUserMapper()
        {
            this.Table("PRIVILEGE_APPLICATION_USER");
            #region 主键
            this.Map(x => x.ApplicationUserId).Column("APPLICATION_USER_ID").Key(KeyType.Assigned);    
            #endregion
            #region 外键
            this.Map(x => x.OrganizationId).Column("ORGANIZATION_ID");
            #endregion
            #region 其他
            this.Map(x => x.UserName).Column("USER_NAME");
            this.Map(x => x.PasswordSalt).Column("PASSWORD_SALT");
            this.Map(x => x.PasswordHash).Column("PASSWORD_HASH");
            this.Map(x => x.FullName).Column("FULL_NAME");
            this.Map(x => x.NickName).Column("NICK_NAME");
            this.Map(x => x.HeadImageUrl).Column("HEAD_IMAGE_URL");
            this.Map(x => x.Phone).Column("PHONE");
            this.Map(x => x.Email).Column("EMAIL");
            this.Map(x => x.Address).Column("ADDRESS");
            this.Map(x => x.Locked).Column("LOCKED");
            this.Map(x => x.LockedTime).Column("LOCKED_TIME");
            this.Map(x => x.Actived).Column("ACTIVED");
            this.Map(x => x.ActivedTime).Column("ACTIVED_TIME");
            this.Map(x => x.Sex).Column("SEX");
            this.Map(x => x.Birthday).Column("BIRTHDAY");
            this.Map(x => x.CreationTime).Column("CREATION_TIME");
            this.Map(x => x.ModifyTime).Column("MODIFY_TIME");
            this.Map(x => x.OpenId).Column("OPEN_ID");
            #endregion
            this.AutoMap();
        }
    }
}

