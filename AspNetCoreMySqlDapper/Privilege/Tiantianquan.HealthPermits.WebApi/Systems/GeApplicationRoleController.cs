﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tiantianquan.Common.UI;
using Tiantianquan.HealthPermits.Domain.Systems;
using Tiantianquan.HealthPermits.Application.Systems;

namespace Tiantianquan.HealthPermits.WebApi.Controllers.Systems
{
     /// <summary>
    /// 角色信息
    /// </summary>
    [Au]
    [Route("/Systems/{controller}/{action}")]
    public class GeApplicationRoleController:ApiController
    {
        public  GeApplicationRoleService  GeApplicationRoleService { get;private set; }

        public GeApplicationRoleController(GeApplicationRoleService geApplicationRoleService)
        {
            this.GeApplicationRoleService = geApplicationRoleService;
        }
        
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchParams"></param>
        /// <returns></returns>
        [HttpPost]
        public PageResult<GeApplicationRole> GetGeApplicationRoleByPage([FromBody]GeApplicationRoleQueryModel search)
        {
            return this.GeApplicationRoleService.GetGeApplicationRoleByPage(search);
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public GeApplicationRoleModel GetGeApplicationRoleById(Guid applicationRoleId)
        {
            return this.GeApplicationRoleService.GetGeApplicationRoleById(applicationRoleId);
        }
        /// <summary>
        /// 新增
        /// </summary>
        [HttpPost]
        public void Post([FromBody]GeApplicationRoleModel model)
        {
           this.GeApplicationRoleService.Save(model);
        }
        /// <summary>
        /// 修改
        /// </summary>
        [HttpPost]
        public void Put(Guid applicationRoleId,[FromBody]GeApplicationRoleModel model)
        {
            this.GeApplicationRoleService.Update(applicationRoleId, model);
        }
        /// <summary>
        /// 删除
        /// </summary>
        [HttpPost]
        public void Delete(Guid applicationRoleId)
        {
            this.GeApplicationRoleService.Delete(applicationRoleId);
        }
       
    }
}


