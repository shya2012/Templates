﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tiantianquan.Common.UI;
using Tiantianquan.HealthPermits.Domain.Systems;
using Tiantianquan.HealthPermits.Application.Systems;

namespace Tiantianquan.HealthPermits.WebApi.Controllers.Systems
{
     /// <summary>
    /// 组织机构
    /// </summary>
    [Au]
    [Route("/Systems/{controller}/{action}")]
    public class GeOrganizationController:ApiController
    {
        public  GeOrganizationService  GeOrganizationService { get;private set; }

        public GeOrganizationController(GeOrganizationService geOrganizationService)
        {
            this.GeOrganizationService = geOrganizationService;
        }
        
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchParams"></param>
        /// <returns></returns>
        [HttpPost]
        public PageResult<GeOrganization> GetGeOrganizationByPage([FromBody]GeOrganizationQueryModel search)
        {
            return this.GeOrganizationService.GetGeOrganizationByPage(search);
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public GeOrganizationModel GetGeOrganizationById(Guid organizationId)
        {
            return this.GeOrganizationService.GetGeOrganizationById(organizationId);
        }
        /// <summary>
        /// 新增
        /// </summary>
        [HttpPost]
        public void Post([FromBody]GeOrganizationModel model)
        {
           this.GeOrganizationService.Save(model);
        }
        /// <summary>
        /// 修改
        /// </summary>
        [HttpPost]
        public void Put(Guid organizationId,[FromBody]GeOrganizationModel model)
        {
            this.GeOrganizationService.Update(organizationId, model);
        }
        /// <summary>
        /// 删除
        /// </summary>
        [HttpPost]
        public void Delete(Guid organizationId)
        {
            this.GeOrganizationService.Delete(organizationId);
        }
       
    }
}


