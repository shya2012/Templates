﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tiantianquan.Common.UI;
using Tiantianquan.HealthPermits.Domain.Systems;
using Tiantianquan.HealthPermits.Application.Systems;

namespace Tiantianquan.HealthPermits.WebApi.Controllers.Systems
{
     /// <summary>
    /// 导航菜单
    /// </summary>
    [Au]
    [Route("/Systems/{controller}/{action}")]
    public class GeNavigationMenuController:ApiController
    {
        public  GeNavigationMenuService  GeNavigationMenuService { get;private set; }

        public GeNavigationMenuController(GeNavigationMenuService geNavigationMenuService)
        {
            this.GeNavigationMenuService = geNavigationMenuService;
        }
        
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchParams"></param>
        /// <returns></returns>
        [HttpPost]
        public PageResult<GeNavigationMenu> GetGeNavigationMenuByPage([FromBody]GeNavigationMenuQueryModel search)
        {
            return this.GeNavigationMenuService.GetGeNavigationMenuByPage(search);
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public GeNavigationMenuModel GetGeNavigationMenuById(Guid navigationMenuId)
        {
            return this.GeNavigationMenuService.GetGeNavigationMenuById(navigationMenuId);
        }
        /// <summary>
        /// 新增
        /// </summary>
        [HttpPost]
        public void Post([FromBody]GeNavigationMenuModel model)
        {
           this.GeNavigationMenuService.Save(model);
        }
        /// <summary>
        /// 修改
        /// </summary>
        [HttpPost]
        public void Put(Guid navigationMenuId,[FromBody]GeNavigationMenuModel model)
        {
            this.GeNavigationMenuService.Update(navigationMenuId, model);
        }
        /// <summary>
        /// 删除
        /// </summary>
        [HttpPost]
        public void Delete(Guid navigationMenuId)
        {
            this.GeNavigationMenuService.Delete(navigationMenuId);
        }
       
    }
}


