﻿using System.Collections.Generic;
using Tiantianquan.Common.Enums;
using Tiantianquan.Common.Repositories;
using Tiantianquan.Common.UI;
using System;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 组织机构
    /// </summary>
    public interface IGeOrganizationRepository : IRepository<GeOrganization>
    {
     /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchParams">分页查询条件</param>
        /// <returns></returns>
        List<GeOrganization> GetGeOrganizationByPage(GeOrganizationQueryModel search);
        
         /// <summary>
        /// 查总记录数
        /// </summary>
        /// <param name="searchParams"></param>
        /// <returns></returns>
        int GetGeOrganizationRecordCount(GeOrganizationQueryModel search);
    }
}
