﻿using System;
using System.ComponentModel;
using FluentValidation;
using FluentValidation.Attributes;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 组织机构
    /// </summary>
    [Description("组织机构")]
    [Validator(typeof(GeOrganizationModelValidator))]
    public class GeOrganizationModel 
    {
        /// <summary>
        /// 组织机构编号
        /// </summary>
        [Description("组织机构编号")]
        public Guid   OrganizationId {get;set;}
        /// <summary>
        /// 上级组织机构编号
        /// </summary>
        [Description("上级组织机构编号")]
        public Guid   ParentOrganizationId {get;set;}
        /// <summary>
        /// 组织机构代码
        /// </summary>
        [Description("组织机构代码")]
        public String   OrganizationCode {get;set;}
        /// <summary>
        /// 组织机构名称
        /// </summary>
        [Description("组织机构名称")]
        public String   OrganizationName {get;set;}
        /// <summary>
        /// 组织机构拼音
        /// </summary>
        [Description("组织机构拼音")]
        public String   OrganizationPinyin {get;set;}
        /// <summary>
        /// 类别
        /// </summary>
        [Description("类别")]
        public Int32   OrganizationClasses {get;set;}
        /// <summary>
        /// 选址
        /// </summary>
        [Description("选址")]
        public String   OrganizationAddress {get;set;}
        /// <summary>
        /// 所有制形式
        /// </summary>
        [Description("所有制形式")]
        public Int32   FormsOfOwnership {get;set;}
        /// <summary>
        /// 床位
        /// </summary>
        [Description("床位")]
        public Int32   Beds {get;set;}
        /// <summary>
        /// 服务对象
        /// </summary>
        [Description("服务对象")]
        public String   ServiceObject {get;set;}
        /// <summary>
        /// 医疗科目
        /// </summary>
        [Description("医疗科目")]
        public String   MedicalSubjects {get;set;}
        /// <summary>
        /// 投资总额
        /// </summary>
        [Description("投资总额")]
        public String   TotalAmountOfInvestment {get;set;}
        /// <summary>
        /// 注册资金
        /// </summary>
        [Description("注册资金")]
        public String   RegisteredCapital {get;set;}
        /// <summary>
        /// BUSINESS_NATURE
        /// </summary>
        [Description("BUSINESS_NATURE")]
        public Int32   BusinessNature {get;set;}
        /// <summary>
        /// 其他
        /// </summary>
        [Description("其他")]
        public String   Other {get;set;}
        /// <summary>
        /// 证书有效期
        /// </summary>
        [Description("证书有效期")]
        public DateTime   ValidityOfCertificate {get;set;}
        /// <summary>
        /// 批准文号
        /// </summary>
        [Description("批准文号")]
        public String   ApprovalNumber {get;set;}
        /// <summary>
        /// 联系电话
        /// </summary>
        [Description("联系电话")]
        public String   OrganizationTelphone {get;set;}
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeOrganizationModel()
        {
          //  OrganizationCode = String.Empty;
          //  OrganizationName = String.Empty;
          //  OrganizationPinyin = String.Empty;
          //  OrganizationAddress = String.Empty;
          //  ServiceObject = String.Empty;
          //  MedicalSubjects = String.Empty;
          //  TotalAmountOfInvestment = String.Empty;
          //  RegisteredCapital = String.Empty;
          //  Other = String.Empty;
           // ValidityOfCertificate = DateTime.Now;
          //  ApprovalNumber = String.Empty;
          //  OrganizationTelphone = String.Empty;
        }
    }
}

