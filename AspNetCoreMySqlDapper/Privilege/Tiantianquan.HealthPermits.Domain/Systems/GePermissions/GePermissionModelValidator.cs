﻿using System;
using System.ComponentModel;
using FluentValidation;
using FluentValidation.Attributes;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 权限信息
    /// </summary>
    [Description("权限信息")]
    public class GePermissionModelValidator:AbstractValidator<GePermissionModel> 
    {
      
        /// <summary>
        /// 构造函数
        /// </summary>
        public GePermissionModelValidator()
        {
            RuleFor(item => item.PermissionId).NotNull().WithMessage("权限编号不能为空。");
              
            RuleFor(item => item.NavigationMenuId).NotNull().WithMessage("菜单编号不能为空。");
              
            RuleFor(item => item.ApplicationRoleId).NotNull().WithMessage("角色编号不能为空。");
              
            RuleFor(item => item.ApplicationUserId).NotNull().WithMessage("用户编号不能为空。");
              
            RuleFor(item => item.IsGranted).NotNull().WithMessage("是否授权不能为空。");
              
            RuleFor(item => item.CreationTime).NotNull().WithMessage("授权时间不能为空。");
              
        }
    }
}

