﻿using System;
using System.ComponentModel;
using FluentValidation;
using FluentValidation.Attributes;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 用户信息
    /// </summary>
    [Description("用户信息")]
    public class GeApplicationUserModelValidator:AbstractValidator<GeApplicationUserModel> 
    {
      
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeApplicationUserModelValidator()
        {
            RuleFor(item => item.ApplicationUserId).NotNull().WithMessage("用户编号不能为空。");
              
            RuleFor(item => item.OrganizationId).NotNull().WithMessage("组织机构编号不能为空。");
              
            RuleFor(item => item.UserName).NotNull().WithMessage("用户名不能为空。");
            RuleFor(item => item.UserName).NotEmpty().WithMessage("用户名不能为空。");
           // RuleFor(item => item.UserName).MinimumLength(10).WithMessage("用户名最少10个字。");
            RuleFor(item => item.UserName).MaximumLength(128).WithMessage("用户名最多128个字。");
              
            RuleFor(item => item.PasswordSalt).NotNull().WithMessage("密码盐值不能为空。");
            RuleFor(item => item.PasswordSalt).NotEmpty().WithMessage("密码盐值不能为空。");
           // RuleFor(item => item.PasswordSalt).MinimumLength(10).WithMessage("密码盐值最少10个字。");
            RuleFor(item => item.PasswordSalt).MaximumLength(128).WithMessage("密码盐值最多128个字。");
              
            RuleFor(item => item.PasswordHash).NotNull().WithMessage("密码哈希值不能为空。");
            RuleFor(item => item.PasswordHash).NotEmpty().WithMessage("密码哈希值不能为空。");
           // RuleFor(item => item.PasswordHash).MinimumLength(10).WithMessage("密码哈希值最少10个字。");
            RuleFor(item => item.PasswordHash).MaximumLength(256).WithMessage("密码哈希值最多256个字。");
              
            RuleFor(item => item.FullName).NotNull().WithMessage("真实姓名不能为空。");
            RuleFor(item => item.FullName).NotEmpty().WithMessage("真实姓名不能为空。");
           // RuleFor(item => item.FullName).MinimumLength(10).WithMessage("真实姓名最少10个字。");
            RuleFor(item => item.FullName).MaximumLength(128).WithMessage("真实姓名最多128个字。");
              
            RuleFor(item => item.NickName).NotNull().WithMessage("昵称不能为空。");
            RuleFor(item => item.NickName).NotEmpty().WithMessage("昵称不能为空。");
           // RuleFor(item => item.NickName).MinimumLength(10).WithMessage("昵称最少10个字。");
            RuleFor(item => item.NickName).MaximumLength(128).WithMessage("昵称最多128个字。");
              
            RuleFor(item => item.HeadImageUrl).NotNull().WithMessage("头像不能为空。");
            RuleFor(item => item.HeadImageUrl).NotEmpty().WithMessage("头像不能为空。");
           // RuleFor(item => item.HeadImageUrl).MinimumLength(10).WithMessage("头像最少10个字。");
            RuleFor(item => item.HeadImageUrl).MaximumLength(256).WithMessage("头像最多256个字。");
              
            RuleFor(item => item.Phone).NotNull().WithMessage("手机号不能为空。");
            RuleFor(item => item.Phone).NotEmpty().WithMessage("手机号不能为空。");
           // RuleFor(item => item.Phone).MinimumLength(10).WithMessage("手机号最少10个字。");
            RuleFor(item => item.Phone).MaximumLength(128).WithMessage("手机号最多128个字。");
              
            RuleFor(item => item.Email).NotNull().WithMessage("电子邮箱不能为空。");
            RuleFor(item => item.Email).NotEmpty().WithMessage("电子邮箱不能为空。");
           // RuleFor(item => item.Email).MinimumLength(10).WithMessage("电子邮箱最少10个字。");
            RuleFor(item => item.Email).MaximumLength(128).WithMessage("电子邮箱最多128个字。");
              
            RuleFor(item => item.Address).NotNull().WithMessage("地址不能为空。");
            RuleFor(item => item.Address).NotEmpty().WithMessage("地址不能为空。");
           // RuleFor(item => item.Address).MinimumLength(10).WithMessage("地址最少10个字。");
            RuleFor(item => item.Address).MaximumLength(256).WithMessage("地址最多256个字。");
              
            RuleFor(item => item.Locked).NotNull().WithMessage("是否锁定不能为空。");
              
            RuleFor(item => item.LockedTime).NotNull().WithMessage("锁定时间不能为空。");
              
            RuleFor(item => item.Actived).NotNull().WithMessage("是否激活不能为空。");
              
            RuleFor(item => item.ActivedTime).NotNull().WithMessage("激活时间不能为空。");
              
            RuleFor(item => item.Sex).NotNull().WithMessage("性别不能为空。");
              
            RuleFor(item => item.Birthday).NotNull().WithMessage("出生日期不能为空。");
              
            RuleFor(item => item.CreationTime).NotNull().WithMessage("创建时间不能为空。");
              
            RuleFor(item => item.ModifyTime).NotNull().WithMessage("修改时间不能为空。");
              
            RuleFor(item => item.OpenId).NotNull().WithMessage("第三方登陆OPEN_ID不能为空。");
            RuleFor(item => item.OpenId).NotEmpty().WithMessage("第三方登陆OPEN_ID不能为空。");
           // RuleFor(item => item.OpenId).MinimumLength(10).WithMessage("第三方登陆OPEN_ID最少10个字。");
            RuleFor(item => item.OpenId).MaximumLength(256).WithMessage("第三方登陆OPEN_ID最多256个字。");
              
        }
    }
}

