﻿using System;
using System.ComponentModel;
using FluentValidation;
using FluentValidation.Attributes;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 角色信息
    /// </summary>
    [Description("角色信息")]
    public class GeApplicationRoleModelValidator:AbstractValidator<GeApplicationRoleModel> 
    {
      
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeApplicationRoleModelValidator()
        {
            RuleFor(item => item.ApplicationRoleId).NotNull().WithMessage("角色编号不能为空。");
              
            RuleFor(item => item.RoleName).NotNull().WithMessage("角色名称不能为空。");
            RuleFor(item => item.RoleName).NotEmpty().WithMessage("角色名称不能为空。");
           // RuleFor(item => item.RoleName).MinimumLength(10).WithMessage("角色名称最少10个字。");
            RuleFor(item => item.RoleName).MaximumLength(128).WithMessage("角色名称最多128个字。");
              
            RuleFor(item => item.RoleDescription).NotNull().WithMessage("角色描述不能为空。");
            RuleFor(item => item.RoleDescription).NotEmpty().WithMessage("角色描述不能为空。");
           // RuleFor(item => item.RoleDescription).MinimumLength(10).WithMessage("角色描述最少10个字。");
            RuleFor(item => item.RoleDescription).MaximumLength(256).WithMessage("角色描述最多256个字。");
              
            RuleFor(item => item.CreationTime).NotNull().WithMessage("创建时间不能为空。");
              
            RuleFor(item => item.ModifyTime).NotNull().WithMessage("修改时间不能为空。");
              
        }
    }
}

