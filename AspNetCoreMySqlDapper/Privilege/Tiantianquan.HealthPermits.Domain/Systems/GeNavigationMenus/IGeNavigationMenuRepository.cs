﻿using System.Collections.Generic;
using Tiantianquan.Common.Enums;
using Tiantianquan.Common.Repositories;
using Tiantianquan.Common.UI;
using System;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 导航菜单
    /// </summary>
    public interface IGeNavigationMenuRepository : IRepository<GeNavigationMenu>
    {
     /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchParams">分页查询条件</param>
        /// <returns></returns>
        List<GeNavigationMenu> GetGeNavigationMenuByPage(GeNavigationMenuQueryModel search);
        
         /// <summary>
        /// 查总记录数
        /// </summary>
        /// <param name="searchParams"></param>
        /// <returns></returns>
        int GetGeNavigationMenuRecordCount(GeNavigationMenuQueryModel search);
    }
}
