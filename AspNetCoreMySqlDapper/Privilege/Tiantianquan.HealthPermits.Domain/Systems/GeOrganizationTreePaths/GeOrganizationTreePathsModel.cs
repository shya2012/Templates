﻿using System;
using System.ComponentModel;
using FluentValidation;
using FluentValidation.Attributes;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 机构树
    /// </summary>
    [Description("机构树")]
    [Validator(typeof(GeOrganizationTreePathsModelValidator))]
    public class GeOrganizationTreePathsModel 
    {
        /// <summary>
        /// AncestorOrganizationId
        /// </summary>
        [Description("AncestorOrganizationId")]
        public Guid   AncestorOrganizationId {get;set;}
        /// <summary>
        /// DescendantOrganizationId
        /// </summary>
        [Description("DescendantOrganizationId")]
        public Guid   DescendantOrganizationId {get;set;}
        /// <summary>
        /// OrganizationDeep
        /// </summary>
        [Description("OrganizationDeep")]
        public Int32   OrganizationDeep {get;set;}
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeOrganizationTreePathsModel()
        {
        }
    }
}

