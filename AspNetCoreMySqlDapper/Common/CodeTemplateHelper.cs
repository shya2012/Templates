﻿    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CodeSmith.Engine;
    using SchemaExplorer;

    public class CodeTemplateHelper
    {
        public string TableName;
        public string TableDescription;
        public string TablePrefix;
        public string ClassName;
        public string ServiceClassName;
        public string WebApiControllerClassName;
        public string IRepositoryInterfaceName;
        public string RepositoryClassName;
        public string ClassModelClassName;
        public string ClassListModelClassName;
        public string ClassQueryModelClassName;
        public string ClassMapperClassName;

        public TableSchema Table;

        public CodeTemplateHelper(TableSchema table, string tablePrefix)
        {
            this.Table = table;
            TableName = table.Name;
            TablePrefix = tablePrefix;

            this.ToClassName();
            this.ToServiceClassName();
            this.ToWebApiControllerClassName();
            this.ToIRepositoryInterfaceName();
            this.ToRepositoryClassName();
            this.ToClassModelClassName();
            this.ToClassListModelClassName();
            this.ToClassQueryModelClassName();

            this.TableDescription = this.ClassName;
            if (!string.IsNullOrWhiteSpace(table.Description))
            {
                this.TableDescription = table.Description;
            }
        }

        public void ToClassName()
        {
            string text = this.TableName.Substring(this.TablePrefix.Length);
            StringBuilder builder = new StringBuilder();
            foreach (string word in text.Split('_'))
            {
                int counter = 0;
                foreach (char c in word)
                {
                    if (c == '_')
                        continue;
                    if (counter == 0)
                        builder.Append(char.ToUpperInvariant(c));
                    else
                        builder.Append(char.ToLowerInvariant(c));
                    counter++;
                }
            }
            this.ClassName = builder.ToString();
        }

        public void ToServiceClassName()
        {
            this.ServiceClassName = this.ClassName + "Service";
        }

        public void ToWebApiControllerClassName()
        {
            this.WebApiControllerClassName = this.ClassName + "Controller";
        }

        public void ToIRepositoryInterfaceName()
        {
            this.IRepositoryInterfaceName = "I" + this.ClassName + "Repository";
        }

        public void ToRepositoryClassName()
        {
            this.RepositoryClassName =  this.ClassName + "Repository";
        }

        public void ToClassModelClassName()
        {
            this.ClassModelClassName = this.ClassName + "Model";
        }
        public void ToClassListModelClassName()
        {
            this.ClassListModelClassName = this.ClassName + "ListModel";
        }
        public void ToClassQueryModelClassName()
        {
            this.ClassQueryModelClassName = this.ClassName + "QueryModel";
        }
        public void ToClassMapperClassName()
        {
            this.ClassMapperClassName = this.ClassName + "Mapper";
        }

        public string GetMethodPrimaryKey()
        {
            List<string> list = new List<string>();
            foreach (ColumnSchema column in this.Table.PrimaryKey.MemberColumns)
            {
                string typename=column.SystemType.Name;
               
                if(column.NativeType=="char"&&column.Size==108){
                    typename="Guid";
               }
                list.Add(string.Format("{0} {1}", typename, StringUtil.ToCamelCase(ToPropertyName(column))));
            }
            return string.Join(",", list);
        }

        public string GetCallMethodPrimaryKey()
        {
            List<string> list = new List<string>();
            foreach (ColumnSchema column in this.Table.PrimaryKey.MemberColumns)
            {
                list.Add(string.Format("{0}",StringUtil.ToCamelCase(ToPropertyName(column))));
            }
            return string.Join(",", list);
        }


        public string GetSelectPageSQL()
        {
            string columns= string.Join(",", this.Table.Columns.Where(c => c.Size != -1).Select(c => "[" +c.Name+ "] AS "+ToPropertyName(c)));
            return string.Format("SELECT {0} FROM [{1}]", columns, this.Table.Name);
        }

        public static string ToPropertyName(ColumnSchema column)
        {
            string text = column.Name;
            StringBuilder builder = new StringBuilder();
            foreach (string word in text.Split('_'))
            {
                int counter = 0;
                foreach (char c in word)
                {
                    if (c == '_')
                        continue;
                    if (counter == 0)
                        builder.Append(char.ToUpperInvariant(c));
                    else
                        builder.Append(char.ToLowerInvariant(c));
                    counter++;
                }
            }
            return builder.ToString();
        }

        public static string ToPropertyNameObject(ColumnSchema column)
        {
            string text = column.Name;
            StringBuilder builder = new StringBuilder();
            foreach (string word in text.Split('_'))
            {
                int counter = 0;
                foreach (char c in word)
                {
                    if (c == '_')
                        continue;
                    if (counter == 0)
                        builder.Append(char.ToUpperInvariant(c));
                    else
                        builder.Append(char.ToLowerInvariant(c));
                    counter++;
                }
            }
            return builder.ToString().Substring(0,1).ToLowerInvariant()+builder.ToString().Substring(1);
        }
        
        public static string ToPropertyDescription(ColumnSchema column)
        {
            string text = ToPropertyName(column);
            if (!string.IsNullOrWhiteSpace(column.Description))
            {
                text = column.Description;
            }
            return text;
        }
        
        
        public static bool IsNullableType(Type theType)
            {
                return theType.IsValueType;
    //            return (theType.IsGenericType && theType.
    //              GetGenericTypeDefinition().Equals
    //              (typeof(Nullable<>)));
            }
            
            public static string GetCodeType(ColumnSchema column){
                if(column.NativeType=="char"&&column.Size==108){
                    return "Guid";
                }
                if(column.NativeType=="bit"){
                    return "bool";
                }
                return column.SystemType.Name;
            }
            
                    public static Type GetCodeType2(ColumnSchema column){
                if(column.NativeType=="char"&&column.Size==108){
                    return typeof(Guid);
                }
                if(column.NativeType=="bit"){
                    return typeof(bool);
                }
                return column.SystemType;
            }
                    
                    public static object GetDefaultValue(ColumnSchema column){
                        Type targetType=GetCodeType2(column);
                        return targetType.IsValueType ? Activator.CreateInstance(targetType) : null;  
                    }
            
            public System.Collections.Generic.Dictionary<string,object> GetJsonSchema(){
                System.Collections.Generic.Dictionary<string,object> values=new System.Collections.Generic.Dictionary<string,object>();
                foreach(var column in this.Table.Columns){
                    values.Add(ToPropertyName(column), "");
                }
                return values;
            }
    }

