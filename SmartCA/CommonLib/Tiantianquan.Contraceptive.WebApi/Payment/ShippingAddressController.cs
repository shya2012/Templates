﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tiantianquan.Infrastructure.WebApi.Controllers;
using Tiantianquan.Infrastructure.Utils;
using Tiantianquan.Infrastructure.Utilities;
using Tiantianquan.Infrastructure.UI;
using Tiantianquan.Contraceptive.Domain.Payment;
using Tiantianquan.Contraceptive.Application.Payment;

namespace Tiantianquan.Contraceptive.WebApi.Payment
{
    /// <summary>
    /// 收货地址
    /// </summary>
   [Authorize]
   public class ShippingAddressController:WebApiControllerBase
    {
        protected ShippingAddressService ShippingAddressService { get; }

        public ShippingAddressController(ShippingAddressService shippingAddressService)
        {
            this.ShippingAddressService = shippingAddressService;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchParams"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/ShippingAddress/GetShippingAddressByPage")]
        public PageResult<ShippingAddressInfo> GetShippingAddressByPage([FromBody]ShippingAddressPageParam pageParam)
        {
            return this.ShippingAddressService.GetShippingAddressByPage(pageParam);
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="shippingAddressId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("~/api/Payment/ShippingAddress/GetShippingAddressById")]
        public ShippingAddressInfo GetShippingAddressById(Guid shippingAddressId)
        {
            return this.ShippingAddressService.GetShippingAddressById(shippingAddressId);
        }
        /// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/ShippingAddress/Put")]
        public void Put([FromBody]ShippingAddressModel model)
        {
            this.ShippingAddressService.Insert(model);
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="shippingAddressId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/ShippingAddress/Post")]
        public void Post(Guid shippingAddressId, [FromBody]ShippingAddressModel model)
        {
            this.ShippingAddressService.Update(shippingAddressId, model);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="shippingAddressId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/ShippingAddress/Delete")]
        public void Delete(Guid shippingAddressId)
        {
            this.ShippingAddressService.Delete(shippingAddressId);
        }
    }
}

