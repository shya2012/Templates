﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Domain;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 订单
    /// </summary>
   public class OrderInfo:EntityBase
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        public virtual Guid OrderId{get;set;}
        /// <summary>
        /// 用户编号
        /// </summary>
        public virtual Guid? ResidentUserId{get;set;}
        /// <summary>
        /// 订单号
        /// </summary>
        public virtual String OrderNo{get;set;}
        /// <summary>
        /// 标题
        /// </summary>
        public virtual String OrderTitle{get;set;}
        /// <summary>
        /// 图片
        /// </summary>
        public virtual String OrderImage{get;set;}
        /// <summary>
        /// 商品金额
        /// </summary>
        public virtual Int64? ProductAmount{get;set;}
        /// <summary>
        /// 快递金额
        /// </summary>
        public virtual Int64? ExpressAmount{get;set;}
        /// <summary>
        /// 总金额
        /// </summary>
        public virtual Int64? TotalAmount{get;set;}
        /// <summary>
        /// 支付状态
        /// </summary>
        public virtual Int32? PayStatus{get;set;}
        /// <summary>
        /// 订单明细
        /// </summary>
        public virtual String OrderDetail{get;set;}
        /// <summary>
        /// 物流单号
        /// </summary>
        public virtual String ExpressNo{get;set;}
        /// <summary>
        /// 物流查询时间
        /// </summary>
        public virtual DateTime? LastExpressQueryTime{get;set;}
        /// <summary>
        /// 收货人
        /// </summary>
        public virtual String FullName{get;set;}
        /// <summary>
        /// 收货地址1
        /// </summary>
        public virtual String AddressCode{get;set;}
        /// <summary>
        /// 收货地址2
        /// </summary>
        public virtual String AddressName{get;set;}
        /// <summary>
        /// 收货街道地址
        /// </summary>
        public virtual String AddressStreet{get;set;}
        /// <summary>
        /// 手机号码
        /// </summary>
        public virtual String Phone{get;set;}
        /// <summary>
        /// 年龄
        /// </summary>
        public virtual Int32? Age{get;set;}
        /// <summary>
        /// 性别
        /// </summary>
        public virtual Int32? Sex{get;set;}
        /// <summary>
        /// 下单时间
        /// </summary>
        public virtual DateTime? CreateTime{get;set;}
        /// <summary>
        /// 支付时间
        /// </summary>
        public virtual DateTime? PayTime{get;set;}
        /// <summary>
        /// 订单状态
        /// </summary>
        public virtual Int32? OrderStatus{get;set;}
    }
}

