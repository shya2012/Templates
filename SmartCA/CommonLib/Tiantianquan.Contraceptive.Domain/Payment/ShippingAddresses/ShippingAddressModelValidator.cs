﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 收货地址
    /// </summary>
   public class ShippingAddressModelValidator:AbstractValidator<ShippingAddressInfo>
    {
        public ShippingAddressModelValidator()
        {


            RuleFor(x => x.FullName).MaximumLength(256).WithMessage("FullName最多256个字。");

            RuleFor(x => x.AddressName).MaximumLength(256).WithMessage("AddressName最多256个字。");

            RuleFor(x => x.AddressCode).MaximumLength(256).WithMessage("AddressCode最多256个字。");

            RuleFor(x => x.AddressStreet).MaximumLength(256).WithMessage("AddressStreet最多256个字。");

            RuleFor(x => x.Phone).MaximumLength(256).WithMessage("Phone最多256个字。");
        }
    }
}

