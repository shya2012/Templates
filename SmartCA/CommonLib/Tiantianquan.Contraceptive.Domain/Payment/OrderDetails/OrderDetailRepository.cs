﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Repositories;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 订单明细
    /// </summary>
   public interface OrderDetailRepository:IRepository<OrderDetailInfo,Guid>
    {
        /// <summary>
        /// 获取订单明细分页数据
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        List<OrderDetailInfo> GetOrderDetailByPage(OrderDetailPageParam pageParam);
        /// <summary>
        /// 获取订单明细总记录数
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        int GetOrderDetailRecordCount(OrderDetailPageParam pageParam);
    }
}

