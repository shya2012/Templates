﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Domain;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 订单明细
    /// </summary>
   public class OrderDetailInfo:EntityBase
    {
        /// <summary>
        /// 订单明细编号
        /// </summary>
        public virtual Guid OrderDetailId{get;set;}
        /// <summary>
        /// 订单编号
        /// </summary>
        public virtual Guid? OrderId{get;set;}
        /// <summary>
        /// 商品编码
        /// </summary>
        public virtual Guid? ProductId{get;set;}
        /// <summary>
        /// SKU编号
        /// </summary>
        public virtual Guid? SkuId{get;set;}
        /// <summary>
        /// 商品名称
        /// </summary>
        public virtual String ProductName{get;set;}
        /// <summary>
        /// 商品图片
        /// </summary>
        public virtual String ProductImage{get;set;}
        /// <summary>
        /// Sku属性
        /// </summary>
        public virtual String SkuProperty{get;set;}
        /// <summary>
        /// Sku属性值
        /// </summary>
        public virtual String SkuPropertyValue{get;set;}
        /// <summary>
        /// 成交单价
        /// </summary>
        public virtual Int64? TradPrice{get;set;}
        /// <summary>
        /// 成交数量
        /// </summary>
        public virtual Int64? TradQuantity{get;set;}
        /// <summary>
        /// 总金额
        /// </summary>
        public virtual Int64? TotalAmount{get;set;}
        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual DateTime? CreateTime{get;set;}
    }
}

