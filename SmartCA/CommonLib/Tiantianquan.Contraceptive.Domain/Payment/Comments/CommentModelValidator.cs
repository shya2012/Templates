﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 评论
    /// </summary>
   public class CommentModelValidator:AbstractValidator<CommentInfo>
    {
        public CommentModelValidator()
        {



            RuleFor(x => x.HeadImageUrl).MaximumLength(256).WithMessage("头像最多256个字。");

            RuleFor(x => x.FullName).MaximumLength(256).WithMessage("昵称最多256个字。");

            RuleFor(x => x.Phone).MaximumLength(256).WithMessage("手机号最多256个字。");

            RuleFor(x => x.CommentContent).MaximumLength(512).WithMessage("评论内容最多512个字。");

            RuleFor(x => x.CommentImages).MaximumLength(4000).WithMessage("评论图片最多4000个字。");


            RuleFor(x => x.IPAddress).MaximumLength(256).WithMessage("来源IP最多256个字。");
        }
    }
}

