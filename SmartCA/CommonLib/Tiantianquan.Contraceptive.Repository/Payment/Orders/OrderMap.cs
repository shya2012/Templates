﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Tiantianquan.Contraceptive.Domain.Payment;

namespace Tiantianquan.Contraceptive.Repository.Payment
{

    public class OrderMap:ClassMap<OrderInfo>
    {
        public OrderMap()
        {
            this.Table("Payment_Order");
            this.Id(x => x.OrderId).GeneratedBy.Assigned();
            this.Map(x => x.ResidentUserId).Nullable();
            this.Map(x => x.OrderNo).Not.Nullable().Length(20);
            this.Map(x => x.OrderTitle).Nullable().Length(128);
            this.Map(x => x.OrderImage).Nullable().Length(256);
            this.Map(x => x.ProductAmount).Nullable();
            this.Map(x => x.ExpressAmount).Nullable();
            this.Map(x => x.TotalAmount).Nullable();
            this.Map(x => x.PayStatus).Nullable();
            this.Map(x => x.OrderDetail).Nullable();
            this.Map(x => x.ExpressNo).Nullable().Length(256);
            this.Map(x => x.LastExpressQueryTime).Nullable();
            this.Map(x => x.FullName).Nullable().Length(256);
            this.Map(x => x.AddressCode).Nullable().Length(256);
            this.Map(x => x.AddressName).Nullable().Length(256);
            this.Map(x => x.AddressStreet).Nullable().Length(256);
            this.Map(x => x.Phone).Nullable().Length(256);
            this.Map(x => x.Age).Nullable();
            this.Map(x => x.Sex).Nullable();
            this.Map(x => x.CreateTime).Nullable();
            this.Map(x => x.PayTime).Nullable();
            this.Map(x => x.OrderStatus).Nullable();
        }
    }
}

