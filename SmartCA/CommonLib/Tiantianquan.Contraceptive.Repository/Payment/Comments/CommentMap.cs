﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Tiantianquan.Contraceptive.Domain.Payment;

namespace Tiantianquan.Contraceptive.Repository.Payment
{

    public class CommentMap:ClassMap<CommentInfo>
    {
        public CommentMap()
        {
            this.Table("Payment_Comment");
            this.Id(x => x.CommentId).GeneratedBy.Assigned();
            this.Map(x => x.OrderId).Nullable();
            this.Map(x => x.ResidentUserId).Nullable();
            this.Map(x => x.HeadImageUrl).Nullable().Length(256);
            this.Map(x => x.FullName).Nullable().Length(256);
            this.Map(x => x.Phone).Nullable().Length(256);
            this.Map(x => x.CommentContent).Nullable().Length(512);
            this.Map(x => x.CommentImages).Nullable().Length(4000);
            this.Map(x => x.CreateTime).Nullable();
            this.Map(x => x.IPAddress).Nullable().Length(256);
        }
    }
}

