﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Tiantianquan.Contraceptive.Domain.Payment;

namespace Tiantianquan.Contraceptive.Repository.Payment
{

    public class OrderDetailMap:ClassMap<OrderDetailInfo>
    {
        public OrderDetailMap()
        {
            this.Table("Payment_OrderDetail");
            this.Id(x => x.OrderDetailId).GeneratedBy.Assigned();
            this.Map(x => x.OrderId).Nullable();
            this.Map(x => x.ProductId).Nullable();
            this.Map(x => x.SkuId).Nullable();
            this.Map(x => x.ProductName).Nullable().Length(128);
            this.Map(x => x.ProductImage).Nullable().Length(256);
            this.Map(x => x.SkuProperty).Nullable();
            this.Map(x => x.SkuPropertyValue).Nullable().Length(256);
            this.Map(x => x.TradPrice).Nullable();
            this.Map(x => x.TradQuantity).Nullable();
            this.Map(x => x.TotalAmount).Nullable();
            this.Map(x => x.CreateTime).Nullable();
        }
    }
}

