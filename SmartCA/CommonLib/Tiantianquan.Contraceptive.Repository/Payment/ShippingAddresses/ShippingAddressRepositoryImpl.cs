﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Criterion;
using Tiantianquan.Infrastructure.Repositories;
using Tiantianquan.Contraceptive.Domain.Payment;

namespace Tiantianquan.Contraceptive.Repository.Payment
{
    /// <summary>
    /// 收货地址
    /// </summary>
   public class ShippingAddressRepositoryImpl:Repository<ShippingAddressInfo,Guid>,ShippingAddressRepository
    {
        #region 字段常量
        internal class FieldNames
        {
            /// <summary>
            /// 收货地址编号
            /// </summary>
            public const string ShippingAddressId = "ShippingAddressId";
            /// <summary>
            /// 用户编号
            /// </summary>
            public const string ResidentUserId = "ResidentUserId";
            /// <summary>
            /// FullName
            /// </summary>
            public const string FullName = "FullName";
            /// <summary>
            /// AddressName
            /// </summary>
            public const string AddressName = "AddressName";
            /// <summary>
            /// AddressCode
            /// </summary>
            public const string AddressCode = "AddressCode";
            /// <summary>
            /// AddressStreet
            /// </summary>
            public const string AddressStreet = "AddressStreet";
            /// <summary>
            /// Phone
            /// </summary>
            public const string Phone = "Phone";
        }
        #endregion
        /// <summary>
        /// 获取收货地址分页数据
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        public List<ShippingAddressInfo> GetShippingAddressByPage(ShippingAddressPageParam pageParam)
        {
            var criteria = Session.CreateCriteria<ShippingAddressInfo>();
            if (!string.IsNullOrWhiteSpace(pageParam.Keywords))
            {
                criteria.Add(Restrictions.Like(FieldNames.FullName, "%" + pageParam.Keywords.Trim() + "%"));
            }

            criteria.AddOrder(NHibernate.Criterion.Order.Asc(FieldNames.Phone));
            if (pageParam.IsClientSort())
            {
                pageParam.CreateOrderCriteria(criteria);
            }
            criteria.SetFirstResult(pageParam.GetFirstResult());
            criteria.SetMaxResults(pageParam.GetMaxResults());
            List<ShippingAddressInfo> items = criteria.List<ShippingAddressInfo>().ToList();
            return items;
        }
        /// <summary>
        /// 获取收货地址总记录数
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        public int GetShippingAddressRecordCount(ShippingAddressPageParam pageParam)
        {
            var criteria = Session.CreateCriteria<ShippingAddressInfo>();
            if (!string.IsNullOrWhiteSpace(pageParam.Keywords))
            {
                criteria.Add(Restrictions.Like(FieldNames.FullName, "%" + pageParam.Keywords.Trim() + "%"));
            }
            criteria.SetProjection(Projections.RowCount());
            int total = (int)criteria.UniqueResult();
            return total;
        }
    }
}

