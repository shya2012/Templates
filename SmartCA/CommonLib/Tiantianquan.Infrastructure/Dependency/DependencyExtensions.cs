﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Dependency;

namespace Microsoft.AspNetCore.Builder
{
    public static partial class DependencyExtensions
    {
        public static void UseIocManager(this IApplicationBuilder app)
        {
            IocManager.Initialize(app.ApplicationServices);
        }
    }
}

