﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Exceptionless.Json;
using Microsoft.AspNetCore.Http;

namespace Tiantianquan.Infrastructure.Runtime.Session
{
    public class PasswordClaimsAbpSession : IAbpSession
    {
        public PasswordClaimsAbpSession(IHttpContextAccessor httpContextAccessor)
        {
            HttpContextAccessor = httpContextAccessor;
            this.HttpContext = this.HttpContextAccessor.HttpContext;

            this.UserId = Conv.ToGuid(this.GetClaimValueByType(ClaimTypes.NameIdentifier));
            this.UserName = this.GetClaimValueByType(ClaimTypes.Name);
            this.FullName = this.GetClaimValueByType(ClaimTypes.Surname);
            this.OrganizationId = Conv.ToGuid(this.GetClaimValueByType(ClaimTypes.Country));

          //  Console.WriteLine(JsonConvert.SerializeObject(HttpContext.User.Claims.Select(c => new { c.Type, c.Value }), Formatting.Indented));

        }

        public IHttpContextAccessor HttpContextAccessor { get; private set; }
        public HttpContext HttpContext { get; private set; }


        public string GetClaimValueByType(string claimType)
        {
            try
            {
                var claims = this.HttpContext.User.Claims;
                return claims.FirstOrDefault(c => c.Type == claimType)?.Value;
            }
            catch (Exception ex)
            {
               // Console.WriteLine(ex.Message);
               // Console.WriteLine(ex.StackTrace);
            }
            return string.Empty;
        }

        public Guid UserId { get; private set; }
        public string UserName { get; private set; }

        public Guid? OrganizationId { get; private set; }
        public string OrganizationName { get; private set; }

        public string NickName { get; private set; }

        public string FullName { get; private set; }

        public string Subject { get; private set; }
    }
}
