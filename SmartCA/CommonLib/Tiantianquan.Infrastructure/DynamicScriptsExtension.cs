﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Tiantianquan.Infrastructure
{
    public class DynamicScriptsOption
    {
        public DynamicScriptsOption()
        {
            this.Assemblies = new List<Assembly>();
        }
        
        public List<Assembly> Assemblies { get;  }
    }


    public static class DynamicScriptsExtension
    {
        public static IServiceCollection AddDynamicScripts(this IServiceCollection services , params Assembly[] assemblies)
        {
            DynamicScriptsOption option = new DynamicScriptsOption();
            option.Assemblies.Add(typeof(DynamicScriptsExtension).Assembly);
            option.Assemblies.AddRange(assemblies);
            services.AddSingleton<DynamicScriptsOption>(option);
            //services.Configure<DynamicScriptsOption>(option=> {
            //    option.Assemblies.Add(typeof(DynamicScriptsExtension).Assembly);
            //    option.Assemblies.AddRange(assemblies);
               
            //});
            return services;
        }
    }
}
