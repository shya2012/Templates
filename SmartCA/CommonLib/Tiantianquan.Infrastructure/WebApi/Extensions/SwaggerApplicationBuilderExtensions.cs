﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// Swagger扩展
    /// </summary>
    public static partial class SwaggerApplicationBuilderExtensions
    {
        /// <summary>
        /// 配置自定义Swagger服务
        /// </summary>
        /// <param name="builder">应用程序生成器</param>
        /// <param name="swaggerUiSetup">swaggerUI设置</param>
        public static IApplicationBuilder UseSwaggerX( this IApplicationBuilder builder,Action<SwaggerUIOptions> swaggerUiSetup = null ) {
            builder.UseSwagger();
            builder.UseSwaggerUI( options => {
                options.IndexStream = () => typeof(SwaggerApplicationBuilderExtensions).GetTypeInfo().Assembly.GetManifestResourceStream("Tiantianquan.Infrastructure.Swaggers.index.html");
                if ( swaggerUiSetup == null ) {
                    options.SwaggerEndpoint( "/swagger/v1/swagger.json", "api v1" );
                    return;
                }
                swaggerUiSetup( options );
            } );
            return builder;
        }

        public static IServiceCollection AddSwaggerX(this IServiceCollection services,string xmldocument,string title,string description,string version="v1")
        {
            #region 配置Swagger

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(version, new Info
                {
                    Version = version,
                    Title = title,
                    Description =description,
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "甜甜圈科技",
                        Email = "tiantianquan@qq.com",
                        Url = "http://www.weixiaobang.cn"
                    }
                });

                //Set the comments path for the swagger json and ui.
                var basePath = Environment.CurrentDirectory;
                var xmlPath = Path.Combine(basePath, xmldocument);
                Console.WriteLine(xmlPath);
                if (File.Exists(xmlPath))
                {
                    c.IncludeXmlComments(xmlPath);
                }

                // c.OperationFilter<HttpHeaderOperation>(); // 添加httpHeader参数
            });

            #endregion 配置Swagger

            return services;
        }
    }
}
