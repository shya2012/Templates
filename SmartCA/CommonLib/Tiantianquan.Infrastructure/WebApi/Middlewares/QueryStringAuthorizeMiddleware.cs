﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using System;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.WebApi.Middlewares
{
    public class QueryStringAuthorizeMiddleware
    {
        /// <summary>
        /// 方法
        /// </summary>
        private readonly RequestDelegate _next;

        private readonly ILogger<QueryStringAuthorizeMiddleware> _logger;

        /// <summary>
        /// 初始化错误日志中间件
        /// </summary>
        /// <param name="next">方法</param>
        public QueryStringAuthorizeMiddleware(RequestDelegate next, ILogger<QueryStringAuthorizeMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        /// <summary>
        /// 执行方法
        /// </summary>
        /// <param name="context">Http上下文</param>
        public async Task Invoke(HttpContext context)
        {
            StringValues token = new StringValues();
            if (context.Request.Query.TryGetValue("token", out token))
            {
                if (!context.Request.Headers.ContainsKey("Authorization"))
                {
                    context.Request.Headers.Add("Authorization", token.ToString());
                }
            }

            await _next(context);

        }
    }
}