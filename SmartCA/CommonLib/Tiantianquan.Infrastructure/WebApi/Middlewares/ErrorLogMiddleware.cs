﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Exceptionless.Extensions;
using Microsoft.Extensions.Logging;
using System.Text;
using System.Collections.Generic;
using Tiantianquan.Infrastructure.Extensions;

namespace Tiantianquan.Infrastructure.WebApi.Middlewares
{
    /// <summary>
    /// 错误日志中间件
    /// </summary>
    public class ErrorLogMiddleware
    {
        /// <summary>
        /// 方法
        /// </summary>
        private readonly RequestDelegate _next;

        private readonly ILogger<ErrorLogMiddleware> _logger;

        /// <summary>
        /// 初始化错误日志中间件
        /// </summary>
        /// <param name="next">方法</param>
        public ErrorLogMiddleware(RequestDelegate next, ILogger<ErrorLogMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        /// <summary>
        /// 执行方法
        /// </summary>
        /// <param name="context">Http上下文</param>
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                WriteLog(context, ex);
                throw;
            }
        }

        /// <summary>
        /// 记录错误日志
        /// </summary>
        private void WriteLog(HttpContext context, Exception ex)
        {
            if (context == null)
                return;
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("请求地址：" + context.GetAbsouteUri());
            builder.AppendLine("请求方法：" + context.Request.Method);
            builder.AppendLine("请求参数：" + context.GetArguments());
            builder.AppendLine("错误消息：" + ex.GetInnermostException().GetMessage());
            builder.AppendLine("堆栈信息：" + ex.GetInnermostException().StackTrace);
            builder.AppendLine();
            this._logger.LogError(ex,builder.ToString());

        }
    }
}
