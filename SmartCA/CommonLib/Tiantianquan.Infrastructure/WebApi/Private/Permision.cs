﻿using DapperExtensions.Mapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Text;
using Tiantianquan.Infrastructure.Dependency;
using Tiantianquan.Infrastructure.Utils;

namespace Tiantianquan.Infrastructure.WebApi.Private
{
    public class ActionPermision
    {
        public Guid PermisionId { get; set; }

        public string PermisionName { get; set; }

        public string GroupName { get; set; }

        public string ActionPath { get; set; }

        public string ControllerName { get; set; }

        public string ActionName { get; set; }

        public ActionPermision() {
            this.PermisionId = GuidUtils.SecureSequentialGuid();
        }
    }

    public class ActionPermisionMapper : ClassMapper<ActionPermision>
    {
        public ActionPermisionMapper()
        {

        }
    }
}
