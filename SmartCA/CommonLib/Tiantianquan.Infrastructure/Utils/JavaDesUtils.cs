﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.Utils
{
    /// <summary>
    /// 和JAVA系统对接时DES加密算法调整为一致
    /// Java中默认DES加密方式为ECB（电子密码本模式），而C#中默认DES加密方式为CBC（加密分组链接模式）这二者是最常见的DES加密方式，且加密key都为8位，
    /// 原文链接：http://www.cnblogs.com/CreateMyself/p/7295879.html
    /// 引用链接：http://luanxiyuan.iteye.com/blog/1938348
    /// </summary>
    public class JavaDesUtils
    {
       
        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="originalString">明文</param>
        /// <param name="key">密钥,长度固定8位</param>
        /// <returns></returns>
        public static string Encrypt(string originalString, string key)
        {
            if (String.IsNullOrEmpty(originalString))
            {
                throw new ArgumentNullException
                       ("The string which needs to be encrypted can not be null.");
            }
            byte[] textBytes = Encoding.UTF8.GetBytes(originalString);
            int mod = (8 - (textBytes.Length % 8));
            for (int i = 0; i < mod; i++)
            {
                originalString = " " + originalString;
            }
            var bytes = Encoding.UTF8.GetBytes(key);
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider()
            {
                Mode = CipherMode.ECB,
                Padding = PaddingMode.None
            };

            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateEncryptor(bytes, bytes), CryptoStreamMode.Write);
            StreamWriter writer = new StreamWriter(cryptoStream);
            writer.Write(originalString);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();
            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
        }
        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="cryptedString">密文</param>
        /// <param name="key">密钥,长度固定8位</param>
        /// <returns></returns>
        public static string Decrypt(string cryptedString, string key)
        {
            if (String.IsNullOrEmpty(cryptedString))
            {
                throw new ArgumentNullException
                   ("The string which needs to be decrypted can not be null.");
            }

            var bytes = Encoding.UTF8.GetBytes(key);
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider()
            {
                Mode = CipherMode.ECB,
                Padding = PaddingMode.None
            };
            MemoryStream memoryStream = new MemoryStream
                    (Convert.FromBase64String(cryptedString));
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateDecryptor(bytes, bytes), CryptoStreamMode.Read);
            StreamReader reader = new StreamReader(cryptoStream);
            return reader.ReadToEnd().Trim();
        }
    }
}
