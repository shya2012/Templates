﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Tiantianquan.Infrastructure.Utils
{
    /// <summary>
    /// Password utility functions
    /// </summary>
    public static class PasswordUtils
    {
        /// <summary>
        /// Get PBKDF2 checksum
        /// </summary>
        /// <param name="data">The data</param>
        /// <param name="slat">The slat, length should be 8</param>
        /// <param name="iterations">Iteration times</param>
        /// <param name="hashLength">Hash length</param>
        /// <returns></returns>
        public static string PBKDF2Sum(
            string data, string slat, int iterations = 1024, int hashLength = 32)
        {
            var hash = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(data), Encoding.UTF8.GetBytes(slat), iterations).GetBytes(hashLength);
            return BitConverter.ToString(hash).Replace("-", "");
        }

        /// <summary>
        /// Get md5 checksum
        /// </summary>
        public static string Md5Sum(string data)
        {
            var hash= MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(data));
            return BitConverter.ToString(hash).Replace("-", "");
        }

        /// <summary>
        /// Get sha1 checksum
        /// </summary>
        public static string Sha1Sum(string data)
        {
            var hash = SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(data));
            return BitConverter.ToString(hash).Replace("-", "");
        }

        public static bool Check(string password,string salt,string passwordHash, PasswordHashType passwordHashType)
        {
            string passwordValidationHash = string.Empty;
            switch (passwordHashType)
            {
                case PasswordHashType.Md5:
                    passwordValidationHash = Md5Sum(password);
                    break;
                case PasswordHashType.Sha1:
                    passwordValidationHash = Sha1Sum(password);
                    break;
                case PasswordHashType.PBKDF2:
                    passwordValidationHash = PBKDF2Sum(password, salt);
                    break;
            }
           return passwordValidationHash == passwordHash;
        }
    }

 
    /// <summary>
    /// 密码类型
    /// </summary>
    public enum PasswordHashType
    {
        /// <summary>
        /// PBKDF2
        /// </summary>
        PBKDF2 = 0,

        /// <summary>
        /// Md5
        /// </summary>
        Md5 = 1,

        /// <summary>
        /// Sha1
        /// </summary>
        Sha1 = 2
    }
}