﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using Tiantianquan.Infrastructure.RabbitMQ;

namespace Microsoft.AspNetCore.Builder
{
    public static partial class ServiceCollectionExtensions
    {
        public static IServiceCollection AddRabbitMQ<T>(this IServiceCollection services, Action<RabbitMQOption> action) where T : IMessageHandler
        {
            RabbitMQOption option = new RabbitMQOption();
            action(option);
            services.AddSingleton<RabbitMQOption>(option);
            services.AddSingleton(typeof(IMessageHandler), typeof(T));
            services.AddSingleton<IConsumer, RabbitMQConsumer>();
            services.AddSingleton<IPublisher, RabbitMQPublisher>();
            return services;
        }

        public static IServiceCollection AddMessageHandler<TData,TMessageHandler>(this IServiceCollection services)
        {
            services.AddScoped(typeof(IMessageHandler<TData>), typeof(TMessageHandler));
            return services;
        }
    }
}
