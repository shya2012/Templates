﻿using System;
using System.Collections.Generic;

namespace Tiantianquan.Infrastructure.RabbitMQ
{
    public interface IPublisher
    {
        void Publish<T>(string routeKey,string label,T data);

    }
}
