﻿using System;
using System.Security.Cryptography;
using System.Text;
using WebApiClient.DataAnnotations;

namespace Tiantianquan.Infrastructure.Services
{
    public class SMSParameter
    {
        [AliasAs("mas_user_id")]
        public string MasUserId { get; set; }

        [AliasAs("mobiles")]
        public string Mobiles { get; set; }

        [AliasAs("content")]
        public string Content { get; set; }

        [AliasAs("sign")]
        public string Sign { get; set; }

        [AliasAs("serial")]
        public string Serial { get; set; }

        [AliasAs("mac")]
        public string Mac { get; set; }

        public SMSParameter(SMSAccessToken token, string sign, string serial, string mobiles, string content)
        {
            this.Mobiles = mobiles;
            this.Content = content;
            this.MasUserId = token.MasUserId;
            this.Sign = sign;
            this.Serial = serial;
            string MacString = token.MasUserId + mobiles + content + sign + serial + token.AccessToken;
            this.Mac = GetMd5Hash(MacString);
        }

        public string GetMd5Hash(string text)
        {
            MD5 md5Hash = MD5.Create();

            // 将输入字符串转换为字节数组并计算哈希数据  
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(text));
            return BitConverter.ToString(data).Replace("-", "");
        }
    }
}
