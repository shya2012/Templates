﻿using WebApiClient.DataAnnotations;

namespace Tiantianquan.Infrastructure.Services
{
    public class SMSLoginParameter
    {
        public SMSLoginParameter(string eCName, string userName, string userPasswd)
        {
            ECName = eCName;
            UserName = userName;
            UserPasswd = userPasswd;
        }

        [AliasAs("ec_name")]
        public string ECName { get; set; }

        [AliasAs("user_name")]
        public string UserName { get; set; }

        [AliasAs("user_passwd")]
        public string UserPasswd { get; set; }


    }
}
