﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Tiantianquan.Infrastructure.Services.Tencent
{
    public class TencentYunHelper
    {
        /// <summary>
        /// 多次有效的签名
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="secretId"></param>
        /// <param name="secretKey"></param>
        /// <returns></returns>
        public static string GetManyTimesSign(string appId, string secretId, string secretKey)
        {
            string orignal = string.Format("a={0}&b={1}&k={2}&e={3}&t={4}&r={5}&f={6}",
                 appId,
                 string.Empty,
                 secretId,
                 DateTimeHelper.GetWeixinDateTime(DateTime.Now.AddDays(1)),
                 DateTimeHelper.GetWeixinDateTime(DateTime.Now),
                 new Random().Next(),
                 string.Empty
                 );
            byte[] hmacDigest = HMACSHA1(secretKey, orignal);
            byte[] orignalBytes = Encoding.UTF8.GetBytes(orignal);
            byte[] signContent = new byte[hmacDigest.Length + orignal.Length];
            Array.Copy(hmacDigest, 0, signContent, 0, hmacDigest.Length);
            Array.Copy(orignalBytes, 0, signContent, hmacDigest.Length, orignalBytes.Length);
            //MemoryStream stream = new MemoryStream();
            //stream.Write(hmacDigest,0, hmacDigest.Length);
            //stream.Write(orignalBytes, 0, orignalBytes.Length);
            //stream.Flush();
            //byte[] signContent = stream.GetBuffer();
            //stream.Close();
            string sign = Convert.ToBase64String(signContent);
            return sign;
        }
        /// <summary>
        /// 单次有效的签名
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="secretId"></param>
        /// <param name="secretKey"></param>
        /// <returns></returns>
        public static string GetOnceSign(string appId, string secretId, string secretKey)
        {
            string orignal = string.Format("a={0}&b={1}&k={2}&e={3}&t={4}&r={5}&f={6}",
                 appId,
                 string.Empty,
                 secretId,
                 0,
                 DateTimeHelper.GetWeixinDateTime(DateTime.Now),
                 new Random().Next(),
                 string.Empty
                 );
            byte[] hmacDigest = HMACSHA1(secretKey, orignal);
            byte[] orignalBytes = Encoding.UTF8.GetBytes(orignal);
            byte[] signContent = new byte[hmacDigest.Length + orignal.Length];
            Array.Copy(hmacDigest, 0, signContent, 0, hmacDigest.Length);
            Array.Copy(orignalBytes, 0, signContent, hmacDigest.Length, orignalBytes.Length);
            //MemoryStream stream = new MemoryStream();
            //stream.Write(hmacDigest,0, hmacDigest.Length);
            //stream.Write(orignalBytes, 0, orignalBytes.Length);
            //stream.Flush();
            //byte[] signContent = stream.GetBuffer();
            //stream.Close();
            string sign = Convert.ToBase64String(signContent);
            return sign;
        }

        public static byte[] HMACSHA1(string key, string data)
        {
            byte[] byteData = Encoding.UTF8.GetBytes(data);
            byte[] byteKey = Encoding.UTF8.GetBytes(key);
            HMACSHA1 hmac = new HMACSHA1(byteKey);
            CryptoStream cs = new CryptoStream(Stream.Null, hmac, CryptoStreamMode.Write);
            cs.Write(byteData, 0, byteData.Length);
            cs.Close();
            return hmac.Hash;
        }
    }
}
