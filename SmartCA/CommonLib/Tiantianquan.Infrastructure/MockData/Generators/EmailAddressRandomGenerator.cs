﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tiantianquan.Infrastructure.MockData.Core;
using Tiantianquan.Infrastructure.Utils;

namespace Tiantianquan.Infrastructure.MockData.Generators
{
    /// <summary>
    /// 邮箱地址生成器
    /// </summary>
    public class EmailAddressRandomGenerator:RandomGeneratorBase,IRandomGenerator
    {
        /// <summary>
        /// 实例
        /// </summary>
        public static readonly EmailAddressRandomGenerator Instance = new EmailAddressRandomGenerator();

        /// <summary>
        /// 初始化一个<see cref="EmailAddressRandomGenerator"/>类型的实例
        /// </summary>
        private EmailAddressRandomGenerator() { }

        /// <summary>
        /// 生成邮箱地址
        /// </summary>
        /// <returns></returns>
        public override string Generate()
        {
            return BatchGenerate(1).First();
        }

        /// <summary>
        /// 批量生成邮箱地址
        /// </summary>
        /// <param name="maxLength">生成数量</param>
        /// <returns></returns>
        public override List<string> BatchGenerate(int maxLength)
        {
            List<string> list=new List<string>();
            for (int i = 0; i < maxLength; i++)
            {
                list.Add(GetEmailAddress());
            }
            return list;
        }

        /// <summary>
        /// 获取邮箱地址
        /// </summary>
        /// <returns></returns>
        private string GetEmailAddress()
        {
            
            StringBuilder sb=new StringBuilder();
            sb.Append(UserNameRandomGenerator.Instance.Generate());
            sb.Append("@");
            sb.Append(EmailHosts.ElementAt(RandomUtils.RandomInt(0,EmailHosts.Count)).Value);
            return sb.ToString();
        }

        public static Dictionary<string, string> EmailHosts = new Dictionary<string, string>() {
            { "qq","qq.com"},
            { "163","163.com"},
            { "yeah","yeah.net"},
            { "126","126.com"},
            { "hotmail","hotmail.com"},
            { "gmail","gmail.com"},
            { "sina","sina.com"},
            { "sohu","sohu.com"},
            { "21cn","21cn.com"},
            { "139","139.com"},
            { "189","189.cn"},
            { "tom","tom.com"},
            { "foxmail","foxmail.com"},
            { "aliyun","aliyun.com"}
        };
    }
}
