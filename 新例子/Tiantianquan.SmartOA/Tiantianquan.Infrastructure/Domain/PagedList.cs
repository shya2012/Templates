﻿using System.Collections.Generic;

namespace Tiantianquan.Infrastructure.Domain
{
    public class PagedList<TEntity>:List<TEntity>
    {
        public PagedList(int total,IEnumerable<TEntity> items)
        {
            Total = total;
            this.AddRange(items);
        }

        public int Total { get; set; }
    }
}