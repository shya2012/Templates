﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.Domain
{
    public interface IRepository<TEntity,TKey>
    {
        void Save(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);

        TEntity Find(TKey id);

        IQueryable<TEntity> GetAll();

        PagedList<TEntity> GetPagedList(PageInput page);
    }
}
