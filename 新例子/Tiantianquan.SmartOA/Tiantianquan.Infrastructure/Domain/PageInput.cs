﻿namespace Tiantianquan.Infrastructure.Domain
{
    public class PageInput
    {
        public int Page { get; set; }

        public int PageSize { get; set; }

        public string Order { get; set; }

        public string Sort { get; set; }

        public string Keywords { get; set; }
    }
}