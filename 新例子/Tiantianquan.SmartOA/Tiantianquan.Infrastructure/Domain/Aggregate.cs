﻿using FluentNHibernate.Mapping;
using System;

namespace Tiantianquan.Infrastructure
{
    public class Aggregate<TKey>
    {
        public Aggregate(TKey id)
        {
            Id = id;
            CreatedAt = DateTime.Now;
            Sequeue = DateTime.Now.Second;
        }

        public virtual TKey Id { get; set; }

        public virtual long Sequeue { get; set; }

        public virtual DateTime CreatedAt { get; set; }

        public virtual DateTime? UpdatedAt { get; set; }

        public virtual DateTime? DeletedAt { get; set; }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }

    public class Aggregate : Aggregate<Guid>
    {
        public Aggregate(Guid id) : base(id)
        {
        }

        public Aggregate():base(Guid.NewGuid())
        {
        }
    }

    public class Person : Aggregate
    {
        public virtual string FullName { get; set; }
    }

    public class PersonMap : ClassMap<Person>
    {
        public PersonMap()
        {
            this.Table("STD_PERSON");
            this.Id(x => x.Id).GeneratedBy.Assigned();
            this.Map(x => x.Sequeue).Index("IDX_STD_PERSON_SEQUEUE");
            this.Map(x => x.CreatedAt);
            this.Map(x => x.UpdatedAt);
            this.Map(x => x.DeletedAt).Index("IDX_STD_PERSON_DELETEAT");
            this.Map(x => x.FullName);
        }
    }
}
