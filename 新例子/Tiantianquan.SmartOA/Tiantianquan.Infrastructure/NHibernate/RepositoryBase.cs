﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Domain;

namespace Tiantianquan.Infrastructure.NHibernate
{
    public class RepositoryBase<TEntity, TKey> : IRepository<TEntity, TKey>
    {
        protected ISession Session
        {
            get
            {
                return null;
            }
        }
        public void Delete(TEntity entity)
        {
            Session.Delete(entity);
        }

        public TEntity Find(TKey id)
        {
            return Session.Get<TEntity>(id);
        }

        public IQueryable<TEntity> GetAll()
        {
            return Session.Query<TEntity>();
        }

        public PagedList<TEntity> GetPagedList(PageInput page)
        {
            throw new NotImplementedException();
        }

        public void Save(TEntity entity)
        {
            Session.Save(entity);
        }

        public void Update(TEntity entity)
        {
            Session.Evict(entity);
            Session.Update(entity);
        }
    }
}
