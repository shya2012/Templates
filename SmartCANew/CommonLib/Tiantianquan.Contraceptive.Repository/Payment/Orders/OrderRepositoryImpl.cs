﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Criterion;
using Tiantianquan.Infrastructure.Repositories;
using Tiantianquan.Contraceptive.Domain.Payment;

namespace Tiantianquan.Contraceptive.Repository.Payment
{
    /// <summary>
    /// 订单
    /// </summary>
   public class OrderRepositoryImpl:Repository<OrderInfo,Guid>,OrderRepository
    {
        #region 字段常量
        internal class FieldNames
        {
            /// <summary>
            /// 订单编号
            /// </summary>
            public const string OrderId = "OrderId";
            /// <summary>
            /// 用户编号
            /// </summary>
            public const string ResidentUserId = "ResidentUserId";
            /// <summary>
            /// 订单号
            /// </summary>
            public const string OrderNo = "OrderNo";
            /// <summary>
            /// 标题
            /// </summary>
            public const string OrderTitle = "OrderTitle";
            /// <summary>
            /// 图片
            /// </summary>
            public const string OrderImage = "OrderImage";
            /// <summary>
            /// 商品金额
            /// </summary>
            public const string ProductAmount = "ProductAmount";
            /// <summary>
            /// 快递金额
            /// </summary>
            public const string ExpressAmount = "ExpressAmount";
            /// <summary>
            /// 总金额
            /// </summary>
            public const string TotalAmount = "TotalAmount";
            /// <summary>
            /// 支付状态
            /// </summary>
            public const string PayStatus = "PayStatus";
            /// <summary>
            /// 订单明细
            /// </summary>
            public const string OrderDetail = "OrderDetail";
            /// <summary>
            /// 物流单号
            /// </summary>
            public const string ExpressNo = "ExpressNo";
            /// <summary>
            /// 物流查询时间
            /// </summary>
            public const string LastExpressQueryTime = "LastExpressQueryTime";
            /// <summary>
            /// 收货人
            /// </summary>
            public const string FullName = "FullName";
            /// <summary>
            /// 收货地址1
            /// </summary>
            public const string AddressCode = "AddressCode";
            /// <summary>
            /// 收货地址2
            /// </summary>
            public const string AddressName = "AddressName";
            /// <summary>
            /// 收货街道地址
            /// </summary>
            public const string AddressStreet = "AddressStreet";
            /// <summary>
            /// 手机号码
            /// </summary>
            public const string Phone = "Phone";
            /// <summary>
            /// 年龄
            /// </summary>
            public const string Age = "Age";
            /// <summary>
            /// 性别
            /// </summary>
            public const string Sex = "Sex";
            /// <summary>
            /// 下单时间
            /// </summary>
            public const string CreateTime = "CreateTime";
            /// <summary>
            /// 支付时间
            /// </summary>
            public const string PayTime = "PayTime";
            /// <summary>
            /// 订单状态
            /// </summary>
            public const string OrderStatus = "OrderStatus";
        }
        #endregion
        /// <summary>
        /// 获取订单分页数据
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        public List<OrderInfo> GetOrderByPage(OrderPageParam pageParam)
        {
            var criteria = Session.CreateCriteria<OrderInfo>();
            if (!string.IsNullOrWhiteSpace(pageParam.Keywords))
            {
                criteria.Add(Restrictions.Like(FieldNames.OrderNo, "%" + pageParam.Keywords.Trim() + "%"));
            }

            criteria.AddOrder(NHibernate.Criterion.Order.Asc(FieldNames.LastExpressQueryTime));
            if (pageParam.IsClientSort())
            {
                pageParam.CreateOrderCriteria(criteria);
            }
            criteria.SetFirstResult(pageParam.GetFirstResult());
            criteria.SetMaxResults(pageParam.GetMaxResults());
            List<OrderInfo> items = criteria.List<OrderInfo>().ToList();
            return items;
        }
        /// <summary>
        /// 获取订单总记录数
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        public int GetOrderRecordCount(OrderPageParam pageParam)
        {
            var criteria = Session.CreateCriteria<OrderInfo>();
            if (!string.IsNullOrWhiteSpace(pageParam.Keywords))
            {
                criteria.Add(Restrictions.Like(FieldNames.OrderNo, "%" + pageParam.Keywords.Trim() + "%"));
            }
            criteria.SetProjection(Projections.RowCount());
            int total = (int)criteria.UniqueResult();
            return total;
        }
    }
}

