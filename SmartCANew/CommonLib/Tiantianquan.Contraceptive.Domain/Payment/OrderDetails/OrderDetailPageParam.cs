﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.UI;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 订单明细
    /// </summary>
   public class OrderDetailPageParam:PageParam
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        public  Guid? OrderId{get;set;}
        /// <summary>
        /// 商品编码
        /// </summary>
        public  Guid? ProductId{get;set;}
        /// <summary>
        /// SKU编号
        /// </summary>
        public  Guid? SkuId{get;set;}
    }
}

