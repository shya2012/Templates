﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Domain;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 收货地址
    /// </summary>
   public class ShippingAddressInfo:EntityBase
    {
        /// <summary>
        /// 收货地址编号
        /// </summary>
        public virtual Guid ShippingAddressId{get;set;}
        /// <summary>
        /// 用户编号
        /// </summary>
        public virtual Guid? ResidentUserId{get;set;}
        /// <summary>
        /// FullName
        /// </summary>
        public virtual String FullName{get;set;}
        /// <summary>
        /// AddressName
        /// </summary>
        public virtual String AddressName{get;set;}
        /// <summary>
        /// AddressCode
        /// </summary>
        public virtual String AddressCode{get;set;}
        /// <summary>
        /// AddressStreet
        /// </summary>
        public virtual String AddressStreet{get;set;}
        /// <summary>
        /// Phone
        /// </summary>
        public virtual String Phone{get;set;}
    }
}

