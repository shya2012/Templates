﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Repositories;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 收货地址
    /// </summary>
   public interface ShippingAddressRepository:IRepository<ShippingAddressInfo,Guid>
    {
        /// <summary>
        /// 获取收货地址分页数据
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        List<ShippingAddressInfo> GetShippingAddressByPage(ShippingAddressPageParam pageParam);
        /// <summary>
        /// 获取收货地址总记录数
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        int GetShippingAddressRecordCount(ShippingAddressPageParam pageParam);
    }
}

