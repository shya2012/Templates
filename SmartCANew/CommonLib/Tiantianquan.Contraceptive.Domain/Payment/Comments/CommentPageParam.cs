﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.UI;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 评论
    /// </summary>
   public class CommentPageParam:PageParam
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        public  Guid? OrderId{get;set;}
        /// <summary>
        /// 用户编号
        /// </summary>
        public  Guid? ResidentUserId{get;set;}
    }
}

