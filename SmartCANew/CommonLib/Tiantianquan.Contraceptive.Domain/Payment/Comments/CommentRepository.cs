﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Repositories;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 评论
    /// </summary>
   public interface CommentRepository:IRepository<CommentInfo,Guid>
    {
        /// <summary>
        /// 获取评论分页数据
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        List<CommentInfo> GetCommentByPage(CommentPageParam pageParam);
        /// <summary>
        /// 获取评论总记录数
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        int GetCommentRecordCount(CommentPageParam pageParam);
    }
}

