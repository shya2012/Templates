﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Repositories;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 订单
    /// </summary>
   public interface OrderRepository:IRepository<OrderInfo,Guid>
    {
        /// <summary>
        /// 获取订单分页数据
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        List<OrderInfo> GetOrderByPage(OrderPageParam pageParam);
        /// <summary>
        /// 获取订单总记录数
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        int GetOrderRecordCount(OrderPageParam pageParam);
    }
}

