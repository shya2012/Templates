﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Attributes;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 订单
    /// </summary>
   [Validator(typeof(OrderModelValidator))]
   public class OrderModel
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        public  Guid OrderId{get;set;}
        /// <summary>
        /// 用户编号
        /// </summary>
        public  Guid? ResidentUserId{get;set;}
        /// <summary>
        /// 订单号
        /// </summary>
        public  String OrderNo{get;set;}
        /// <summary>
        /// 标题
        /// </summary>
        public  String OrderTitle{get;set;}
        /// <summary>
        /// 图片
        /// </summary>
        public  String OrderImage{get;set;}
        /// <summary>
        /// 商品金额
        /// </summary>
        public  Int64? ProductAmount{get;set;}
        /// <summary>
        /// 快递金额
        /// </summary>
        public  Int64? ExpressAmount{get;set;}
        /// <summary>
        /// 总金额
        /// </summary>
        public  Int64? TotalAmount{get;set;}
        /// <summary>
        /// 支付状态
        /// </summary>
        public  Int32? PayStatus{get;set;}
        /// <summary>
        /// 订单明细
        /// </summary>
        public  String OrderDetail{get;set;}
        /// <summary>
        /// 物流单号
        /// </summary>
        public  String ExpressNo{get;set;}
        /// <summary>
        /// 物流查询时间
        /// </summary>
        public  DateTime? LastExpressQueryTime{get;set;}
        /// <summary>
        /// 收货人
        /// </summary>
        public  String FullName{get;set;}
        /// <summary>
        /// 收货地址1
        /// </summary>
        public  String AddressCode{get;set;}
        /// <summary>
        /// 收货地址2
        /// </summary>
        public  String AddressName{get;set;}
        /// <summary>
        /// 收货街道地址
        /// </summary>
        public  String AddressStreet{get;set;}
        /// <summary>
        /// 手机号码
        /// </summary>
        public  String Phone{get;set;}
        /// <summary>
        /// 年龄
        /// </summary>
        public  Int32? Age{get;set;}
        /// <summary>
        /// 性别
        /// </summary>
        public  Int32? Sex{get;set;}
        /// <summary>
        /// 下单时间
        /// </summary>
        public  DateTime? CreateTime{get;set;}
        /// <summary>
        /// 支付时间
        /// </summary>
        public  DateTime? PayTime{get;set;}
        /// <summary>
        /// 订单状态
        /// </summary>
        public  Int32? OrderStatus{get;set;}
    }
}

