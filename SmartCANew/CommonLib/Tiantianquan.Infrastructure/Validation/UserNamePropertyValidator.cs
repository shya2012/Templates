﻿using FluentValidation.Resources;
using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Tiantianquan.Infrastructure.Validation
{
    public class UserNamePropertyValidator : PropertyValidator
    {
        protected Regex Pattern = new Regex(@"^[a-zA-Z_@.]{5,30}$");
        public UserNamePropertyValidator() : base("账号只能由字母数字和下划线@.组成，长度5-30位。")
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (string.IsNullOrWhiteSpace(context.PropertyValue as string))
            {
                return true;
            }
           return Pattern.IsMatch(context.PropertyValue as string);
        }
    }
}
