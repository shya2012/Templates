//using IdentityModel;
//using Microsoft.AspNetCore.Http;
//using Newtonsoft.Json;
//using System;
//using System.Linq;
//using System.Security.Claims;
//using System.Threading;

//namespace Tiantianquan.Infrastructure.Runtime.Session
//{
//    /// <summary>
//    /// Implements <see cref="IAbpSession"/> to get session properties from claims of <see cref="Thread.CurrentPrincipal"/>.
//    /// </summary>
//    public class ClaimsAbpSession : IAbpSession
//    {
//        public ClaimsAbpSession(IHttpContextAccessor httpContextAccessor)
//        {
//            HttpContextAccessor = httpContextAccessor;
//            this.HttpContext = this.HttpContextAccessor.HttpContext;
//            this.UserId = Conv.ToGuid(this.GetClaimValueByType(TiantianquanJwtClaimTypes.UserId));
//            this.UserName= this.GetClaimValueByType(TiantianquanJwtClaimTypes.UserName);
//            this.OrganizationId = Conv.ToGuidOrNull(this.GetClaimValueByType(TiantianquanJwtClaimTypes.OrganizationId));
//            this.OrganizationName= this.GetClaimValueByType(TiantianquanJwtClaimTypes.OrganizationName);
//            this.NickName= this.GetClaimValueByType(TiantianquanJwtClaimTypes.NickName);
//            this.FullName = this.GetClaimValueByType(TiantianquanJwtClaimTypes.FullName);

//            this.Subject= this.GetClaimValueByType(JwtClaimTypes.Subject);

//           // Console.WriteLine(JsonConvert.SerializeObject(HttpContext.User.Claims.Select(c => new { c.Type, c.Value }), Formatting.Indented));
//        }

//        public IHttpContextAccessor HttpContextAccessor { get; private set; }
//        public HttpContext HttpContext { get; private set; }


//        protected string GetClaimValueByType(string claimType)
//        {
//            var claims = this.HttpContext.User.Claims;
//            return claims.FirstOrDefault(c => c.Type == claimType)?.Value;
//        }
//        public Guid UserId { get; private set; }
//        public string UserName { get; private set; }

//        public Guid? OrganizationId { get; private set; }
//        public string OrganizationName { get; private set; }

//        public string NickName { get; private set; }

//        public string FullName { get; private set; }

//        public string Subject { get; private set; }
//    }
//}