﻿using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tiantianquan.Infrastructure.NPOI
{
    public class ExcelCell
    {
        /// <summary>
        /// 
        /// </summary>
        public ExcelRow Row { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ICell Cell { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ColumnIndex { get; set; }

        public const int WidthConstant = 256;
        public ExcelCell(ExcelRow row, ICellStyle style, int width = 10)
        {
            this.Row = row;
            this.ColumnIndex = this.Row.ColumnIndex++;

            this.Cell = this.Row.Row.CreateCell(this.ColumnIndex, CellType.String);
            this.Cell.SetCellValue(string.Empty);
            this.Row.Sheet.SetColumnWidth(this.ColumnIndex, width * WidthConstant);
            this.Cell.CellStyle = style;

        }


        public ExcelCell SetValue(string value,bool wrapText=false)
        {
            this.Cell.SetCellValue(value);
            this.Cell.SetCellType(CellType.String);
            this.Cell.CellStyle.WrapText = wrapText;
            return this;
        }

        public ExcelCell SetValue(bool value)
        {
            this.Cell.SetCellValue(value);
            this.Cell.SetCellType(CellType.Boolean);
            return this;
        }

        public ExcelCell SetValue(double value)
        {
            this.Cell.SetCellValue(value);
            this.Cell.SetCellType(CellType.Numeric);
            return this;
        }

        public ExcelCell SetValue(DateTime value)
        {
            this.Cell.SetCellValue(value);
            return this;
        }

        public ExcelCell SetRowSpan(int rowspan)
        {
            this.Row.Sheet.AddMergedRegion(new CellRangeAddress(this.Row.RowIndex, this.Row.RowIndex + rowspan - 1, this.ColumnIndex, this.ColumnIndex));
            return this;
        }

        public ExcelCell SetColSpan(int colspan)
        {
            this.Row.Sheet.AddMergedRegion(new CellRangeAddress(this.Row.RowIndex, this.Row.RowIndex, this.ColumnIndex, this.ColumnIndex + colspan - 1));
            return this;
        }


    }
}
