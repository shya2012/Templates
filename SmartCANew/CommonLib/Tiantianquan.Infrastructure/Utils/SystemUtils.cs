﻿using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using Tiantianquan.Infrastructure.Dependency;

namespace Tiantianquan.Infrastructure.Utils {
	/// <summary>
	/// System utility functions
	/// </summary>
	public static class SystemUtils {


        public const string ContentType_ObjectStream = "application/octet-stream";

		/// <summary>
		/// Get used memory in bytes
		/// </summary>
		/// <returns></returns>
		public static long GetUsedMemoryBytes() {
			using (var process = Process.GetCurrentProcess()) {
				return process.WorkingSet64;
			}
		}



        public static void ConvertToFriendlyException(Action action,string message)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                ILogger logger=IocManager.GetService<ILoggerFactory>().CreateLogger("SystemUtils");
                logger.LogError(ex, ex.Message);
                throw new FriendlyException(message);
            }
        }
	}
}
