﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Extensions;
using Tiantianquan.Infrastructure.Utils;

namespace Tiantianquan.Infrastructure.Services
{
    public interface IFileStore
    {
        string SaveFileIfExist(string name);
    }

    public class DefaultFileStore : IFileStore
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        public DefaultFileStore(IHttpContextAccessor httpContextAccessor,
            IHostingEnvironment hostingEnvironment,
            IConfiguration configuration)
        {
            this._httpContextAccessor = httpContextAccessor;
            this._hostingEnvironment = hostingEnvironment;
            this._configuration = configuration;
        }
        public string SaveFileIfExist(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return string.Empty;
            }
            IFormFile file=this._httpContextAccessor.HttpContext.Request.Form.Files.GetFile(name);
            if (file == null)
            {
                return string.Empty;
            }

            string extension = MimeUtils.GetExtension(file.ContentType);
            if (string.IsNullOrWhiteSpace(extension))
            {
                 extension = Path.GetExtension(file.FileName);
            }
            Console.WriteLine(file.ContentType);
            string relativePath = string.Format("\\Upload\\{0}\\{1}{2}{3}",
                  DateTime.Now.ToString("yyyyMM"),
                  DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                  RandomUtils.RandomInt(1000, 9999),
                  extension
                  ).ToLower();
           
            string absolutePath = this._hostingEnvironment.WebRootPath + relativePath;
            PathUtils.EnsureParentDirectory(absolutePath);

            Stream stream = file.OpenReadStream();
            //对大于1MB的图片进行压缩，最大宽度为1024像素
            if (MimeUtils.IsImage(file.ContentType)&&file.Length>1024*1000)
            {
                ThumbUtils.SendSmallImage(stream, absolutePath, 1024, 1024);
            }
            else
            {
                byte[] buffer = new byte[file.Length];
                stream.Read(buffer, 0, buffer.Length);
                System.IO.File.WriteAllBytes(absolutePath, buffer);
            }
            stream.Close();

            string host= this._httpContextAccessor.HttpContext.GetHostUrl();
            return host + relativePath.Replace("\\","/");
        }
    }

    public class AliyunOssService : IFileStore
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;
        public AliyunOssService(IHttpContextAccessor httpContextAccessor,
            IConfiguration configuration)
        {
            this._httpContextAccessor = httpContextAccessor;
            this._configuration = configuration;
        }

        public string SaveFileIfExist(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return string.Empty;
            }
            string endpoint = this._configuration.GetSection("AliyunOss").GetValue<string>("endpoint");
            string accessKeyId = this._configuration.GetSection("AliyunOss").GetValue<string>("accessKeyId");
            string accessKeySecret = this._configuration.GetSection("AliyunOss").GetValue<string>("accessKeySecret");

            string bucketName = this._configuration.GetSection("AliyunOss").GetValue<string>("bucketName");

            Aliyun.OSS.OssClient client = new Aliyun.OSS.OssClient(endpoint,accessKeyId,accessKeySecret);

            IFormFile file = this._httpContextAccessor.HttpContext.Request.Form.Files.GetFile(name);
            if (file == null)
            {
                return string.Empty;
            }
            string key = Guid.NewGuid().ToString("N");

            Stream stream = file.OpenReadStream();
            var result=client.PutObject(bucketName, key, stream);
            stream.Close();
            return result.RequestId;
        }
    }



}
