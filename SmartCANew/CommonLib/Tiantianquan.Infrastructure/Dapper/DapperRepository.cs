﻿using Dapper;
using DapperExtensions;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Domain;

namespace Tiantianquan.Infrastructure.Dapper
{
    public class DapperRepository<T> : IDapperRepository<T> where T : EntityBase
    {

        protected virtual string Database => "default";

        

        public virtual void Insert(T entity)
        {
            using (var connection = Engine.GetDbConnection(this.Database))
            {
                connection.Insert<T>(entity);
            }
        }

        public virtual async Task InsertAsync(T entity)
        {
            using (var connection = Engine.GetDbConnection(this.Database))
            {
                await connection.InsertAsync<T>(entity);
            }
        }

        public virtual bool Update(T entity)
        {
            using (var connection = Engine.GetDbConnection(this.Database))
            {
                return connection.Update<T>(entity);
            }
        }

        public virtual async Task<bool> UpdateAsync(T entity)
        {
            using (var connection = Engine.GetDbConnection(this.Database))
            {
                return await connection.UpdateAsync<T>(entity);
            }
        }

        public virtual bool Delete(T entity)
        {
            using (var connection = Engine.GetDbConnection(this.Database))
            {
                return connection.Delete<T>(entity);
            }
        }

        public virtual async Task<bool> DeleteAsync(T entity)
        {
            using (var connection = Engine.GetDbConnection(this.Database))
            {
                return await connection.DeleteAsync<T>(entity);
            }
        }

        public virtual T Get(object id)
        {
            using (var connection = Engine.GetDbConnection(this.Database))
            {
                return connection.Get<T>(id);
            }
        }

        public virtual async Task<T> GetAsync(object id)
        {
            using (var connection = Engine.GetDbConnection(this.Database))
            {
                return await connection.GetAsync<T>(id);
            }
        }

        public List<T> GetAll(object predicate = null, IList<ISort> sort = null)
        {
            using (var connection = Engine.GetDbConnection(this.Database))
            {
                return connection.GetList<T>(predicate,sort).ToList();
            }
        }

        public  Task<List<T>> GetAllAsync(object predicate = null, IList<ISort> sort = null)
        {
            using (var connection = Engine.GetDbConnection(this.Database))
            {
                return  Task.FromResult(connection.GetListAsync<T>(predicate,sort).Result.ToList());
            }
        }

        public IEnumerable<T> GetPage(object predicate, IList<ISort> sorts,int page,int resultsPerPage)
        {
            using (var connection = Engine.GetDbConnection(this.Database))
            {
               return connection.GetPage<T>(predicate, sorts,page-1,resultsPerPage);
            }
        }
        public  Task<IEnumerable<T>> GetPageAsync(object predicate, IList<ISort> sorts, int page, int resultsPerPage)
        {
            using (var connection = Engine.GetDbConnection(this.Database))
            {
              return  connection.GetPageAsync<T>(predicate, sorts, page, resultsPerPage);
            }
        }
        public Task<int> CountAsync(object predicate)
        {
            using (var connection = Engine.GetDbConnection(this.Database))
            {
                return connection.CountAsync<T>(predicate);
            }
        }
        public int Count(object predicate)
        {
            using (var connection = Engine.GetDbConnection(this.Database))
            {
                return connection.Count<T>(predicate);
            }
        }
    }
}
