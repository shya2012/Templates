﻿using CodeSmith.Engine;
using SchemaExplorer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class CodeHelper
{
    private TableSchema Table;

    private string Prefix;

    public CodeHelper(TableSchema table, string prefix)
    {
        Table = table;
        Prefix = prefix;
    }

    /// <summary>
    /// 判断是否是关键字
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static bool IsKeyword(String str)
    {
        List<String> list = new List<String>();
        list.Add("abstract");
        list.Add("as");
        list.Add("base");
        list.Add("bool");
        list.Add("break");
        list.Add("byte");
        list.Add("case");
        list.Add("catch");
        list.Add("char");
        list.Add("checked");
        list.Add("class");
        list.Add("const");
        list.Add("continue");
        list.Add("decimal");
        list.Add("default");
        list.Add("delegate");
        list.Add("do");
        list.Add("double");
        list.Add("else");
        list.Add("enum");
        list.Add("event");
        list.Add("explicit");
        list.Add("extern");
        list.Add("false");
        list.Add("finally");
        list.Add("fixed");
        list.Add("float");
        list.Add("for");
        list.Add("foreach");
        list.Add("goto");
        list.Add("if");
        list.Add("implicit");
        list.Add("in");
        list.Add("int");
        list.Add("interface");
        list.Add("internal");
        list.Add("is");
        list.Add("lock");
        list.Add("long");
        list.Add("namespace");
        list.Add("new");
        list.Add("null");
        list.Add("object");
        list.Add("operator");
        list.Add("out");
        list.Add("override");
        list.Add("params");
        list.Add("private");
        list.Add("protected");
        list.Add("public");
        list.Add("readonly");
        list.Add("ref");
        list.Add("return");
        list.Add("sbyte");
        list.Add("sealed");
        list.Add("short");
        list.Add("sizeof");
        list.Add("stackalloc");
        list.Add("static");
        list.Add("string");
        list.Add("struct");
        list.Add("switch");
        list.Add("this");
        list.Add("throw");
        list.Add("true");
        list.Add("try");
        list.Add("typeof");
        list.Add("uint");
        list.Add("ulong");
        list.Add("unchecked");
        list.Add("unsafe");
        list.Add("ushort");
        list.Add("using");
        list.Add("virtual");
        list.Add("void");
        list.Add("while");
        return list.Contains(str);
    }

    public static string GetPascalName(string text, string prefix = "",bool toSingular=true)
    {
        if (!string.IsNullOrWhiteSpace(prefix))
        {
            if (prefix.EndsWith("_"))
                text = text.Substring(prefix.Length);
            else
                text = text.Substring(prefix.Length + 1);
        }

        string[] names = text.Split('_');
        string result = string.Empty;
        foreach (string item in names)
        {
            if (string.IsNullOrWhiteSpace(item))
            {
                continue;
            }
            if (names.Length > 1)
            {
                result += StringUtil.ToPascalCase(item);
            }
            else
            {
                if (char.IsUpper(item[0]))
                {
                    result = item;
                }
                else
                {
                    result = StringUtil.ToPascalCase(item);
                }
            }
        }
        if(toSingular)
        {
            if(StringUtil.IsPlural(result))
                result=StringUtil.ToSingular(result);
        }
        return result;
    }

    public static string GetDescription(ColumnSchema column)
    {
        if (string.IsNullOrWhiteSpace(column.Description))
            return column.Name;
        return column.Description;
    }

    public static string GetPropertyName(ColumnSchema column)
    {
        return GetPascalName(column.Name,"",false);
    }

    public static string GetGetPropertyVar(ColumnSchema column)
    {
        string name = GetPascalName(column.Name,"",false);
        name = char.ToUpper(name[0]) + name.Substring(1);
        if (IsKeyword(name))
        {
            return "@" + name;
        }
        return name;
    }



    public string GetTableDescription()
    {
        if (string.IsNullOrWhiteSpace(Table.Description))
            return Table.Name;
        return Table.Description;
    }

    public string GetClassName()
    {
        return GetPascalName(this.Table.Name, Prefix);
    }
    public string GetClassNameVar()
    {
        string name = GetPascalName(this.Table.Name, Prefix);
        name = char.ToLower(name[0]) + name.Substring(1);
        if (IsKeyword(name))
        {
            return "@" + name;
        }
        return name;
    }
    public string GetDbModelClassName()
    {
        return GetPascalName(this.Table.Name, Prefix)+"Info";
    }
    public string GetRepositoryInterfaceName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "Repository";
    }

    public string GetRepositoryClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "RepositoryImpl";
    }

    public string GetMapClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "Map";
    }

    public string GetModelClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "Model";
    }

    public string GetModelValidatorClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "Validator";
    }

    public string GetListClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "ListModel";
    }

    public string GetServiceClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "Service";
    }

    public string GetControllerClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "Controller";
    }

    public string GetPageParamClassName()
    {
        return GetPascalName(this.Table.Name, Prefix) + "PageParam";
    }

    /// <summary>
    /// 获取主键
    /// </summary>
    /// <returns></returns>
    public string GetPrimaryKeyName()
    {
        if (this.Table.PrimaryKey.MemberColumns.Count == 0)
        {
            return string.Empty;
        }
        return GetPropertyName(this.Table.PrimaryKey.MemberColumns[0]);
    }
    public string GetPrimaryKeyNameVar()
    {
        string name = GetPrimaryKeyName();
        return char.ToLower(name[0]) + name.Substring(1);
    }
    public static string GetNullableColumnType(ColumnSchema column)
    {

        var theType = column.SystemType;
        var ret = (theType.IsGenericType && theType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)));
        if (ret == false && theType != typeof(string)&&column.AllowDBNull)
            return theType.Name + "?";
        return theType.Name;
    }

    /// <summary>
    /// 获取主键数据类型
    /// </summary>
    /// <returns></returns>
    public string GetPrimaryKeyType()
    {
        if (this.Table.PrimaryKey.MemberColumns.Count == 0)
        {
            return string.Empty;
        }
        return this.Table.PrimaryKey.MemberColumns[0].SystemType.Name;
    }

    public static string GetNameSpace(string project,string layer,string module)
    {
        if (string.IsNullOrWhiteSpace(module))
        {
            return project + "." + layer;
        }
        return project + "." + layer + "." + module;
    }

    public string GetStringField()
    {
        foreach (var column in this.Table.Columns)
        {
            if (column.SystemType == typeof(string))
                return GetPropertyName(column);
        }
        return GetPropertyName(this.Table.Columns.Last());
    }
    public string GetOrderField()
    {
        foreach (var column in this.Table.Columns)
        {
            if (column.SystemType == typeof(DateTime))
                return GetPropertyName(column);
        }
        return GetPropertyName(this.Table.Columns.Last());
    }

    public static string GetVar(string name)
    {
        return char.ToLower(name[0]) + name.Substring(1);
    }
    
            /// <summary>
        /// 判断一列是否为自增长
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static bool IsIdentity(SchemaExplorer.ColumnSchema column){
            return (bool)column.ExtendedProperties["CS_IsIdentity"].Value;
        }

    public static string PascalCaseToUnderlineNameCase(string name, bool lower = true)
    {
        if (string.IsNullOrWhiteSpace(name))
            return string.Empty;
        StringBuilder builder = new StringBuilder(20);

        for (int i = 0; i < name.Length; i++)
        {
            if (i<name.Length-1&&char.IsUpper(name[i]) && char.IsLower(name[i+1])&&i>0)
            {
                builder.Append("_");
                builder.Append(name[i]);
            }
            else
            {
                builder.Append(name[i]);
            }

        }
        if (lower)
            return builder.ToString().ToLower();
        return builder.ToString().ToUpper();
    }

    public static string UnderlineNameCaseToPascalCase(string name)
    {
        if (string.IsNullOrWhiteSpace(name))
            return string.Empty;
        var words=name.Split('_').Select(x => x.Replace("_", "")).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
       return string.Join("", words.Select(x => x[0].ToString().ToUpper() + x.Substring(1).ToLower()));
    }
}