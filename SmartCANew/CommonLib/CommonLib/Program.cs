﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(CodeHelper.GetPascalName("UserName"));
            Console.WriteLine(CodeHelper.GetPascalName("USER_NAME"));
            Console.WriteLine(CodeHelper.GetPascalName("user_name"));
            Console.WriteLine(CodeHelper.GetPascalName("Sys_UserName","Sys_"));
            Console.WriteLine(CodeHelper.GetPascalName("Sys_User_Name", "Sys_"));
            Console.WriteLine(CodeHelper.GetPascalName("sys_user_name", "sys"));

            Console.WriteLine(CodeHelper.PascalCaseToUnderlineNameCase("UserName"));
            Console.WriteLine(CodeHelper.PascalCaseToUnderlineNameCase("userName"));
            Console.WriteLine(CodeHelper.UnderlineNameCaseToPascalCase("sys_user_name"));
            Console.WriteLine(CodeHelper.UnderlineNameCaseToPascalCase("_user_name"));
            Console.Read();
        }
    }

   
}
