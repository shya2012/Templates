﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeSmith.Engine;
using SchemaExplorer;

public class CodeTemplateHelper
{

    private TableSchema Table;
    private string ModuleName;

    public CodeTemplateHelper(String moduleName,TableSchema table)
    {
        this.Table = table;
        this.ModuleName=moduleName;
    }

    /// <summary>
    /// 实体类名
    /// </summary>
    /// <param name="schema"></param>
    /// <returns></returns>
    public String GetClassName()
    {
        String tableName = Table.Name;
        int position = tableName.IndexOf("_");
        if (position > 0&&!string.IsNullOrEmpty(this.ModuleName))
        {
            tableName = tableName.Substring(position+1);
        }
        if (!char.IsUpper(tableName[0]))
        {
           // tableName = StringUtil.ToPascalCase(tableName);
        }
        return tableName;
    }

    /// <summary>
    /// 实体描述
    /// </summary>
    /// <param name="schema"></param>
    /// <returns></returns>
    public String GetClassDescription()
    {
        if (string.IsNullOrWhiteSpace(Table.Description))
            return Table.Name;
        return Table.Description.Replace("\r", "").Replace("\n", "").Replace(" ","");
    }

    /// <summary>
    /// 从列获取属性名
    /// </summary>
    /// <param name="schema"></param>
    /// <returns></returns>
    public static String GetProtityName(SchemaExplorer.ColumnSchema column)
    {
        String tableName = column.Name;
        int position = tableName.IndexOf("_");
        if (position > 0)
        {
            tableName = tableName.Substring(position);
        }
        if (!char.IsUpper(tableName[0]))
        {
            tableName = StringUtil.ToPascalCase(tableName);
        }
        return tableName;
    }

    /// <summary>
    /// 从属性获取属性描述
    /// </summary>
    /// <param name="schema"></param>
    /// <returns></returns> 
    public static String GetProtityDescription(SchemaExplorer.ColumnSchema column)
    {
        if (string.IsNullOrWhiteSpace(column.Description))
            return column.Name;
        return column.Description.Replace("\r", "").Replace("\n", "");
    }
    /// <summary>
    /// 属性对象
    /// </summary>
    /// <param name="column"></param>
    /// <returns></returns>
    public static String GetProtityObjectName(SchemaExplorer.ColumnSchema column)
    {
        String str = StringUtil.ToCamelCase(GetProtityName(column));
        if (IsKeyword(str))
            return "@" + str;
        return str;
    }
     public static string IsRequired(ColumnSchema column )
    {
        if (column.AllowDBNull)
            return string.Empty;
        return ".IsRequired()";
    }
    public static string HasMaxLength(ColumnSchema column)
    {
        if (column.SystemType.FullName=="System.String"&&column.Size>0)
        return ".HasMaxLength("+column.Size+")";
        return string.Empty;
    }
    
    public static string IsRequiredNH(ColumnSchema column )
    {
        if (column.AllowDBNull)
            return ".Nullable()";
        return ".Not.Nullable()";
    }
    public static string HasMaxLengthNH(ColumnSchema column)
    {
        if (column.SystemType.FullName=="System.String"&&column.Size>0)
        return ".Length("+column.Size+")";
        return string.Empty;
    }
    /// <summary>
    /// WEBAPI控制器类名
    /// </summary>
    /// <returns></returns>
    public String GetWebApiClassName()
    {
        return GetClassName() + "Controller";
    }


    /// <summary>
    /// 模型类名
    /// </summary>
    /// <returns></returns>
    public String GetModelClassName()
    {
        return GetClassName() + "Info";
    }

    /// <summary>
    /// 数据仓储接口
    /// </summary>
    /// <returns></returns>
    public String GetRepositoryInterfaceName()
    {
        return "I" + GetClassName() + "Repository";
    }
    /// <summary>
    /// 数据仓储类名
    /// </summary>
    /// <returns></returns>
    public String GetRepositoryClassName()
    {
        return "R" + GetClassName() + "";
    }
    /// <summary>
    /// 服务接口名
    /// </summary>
    /// <returns></returns>
    public String GetServiceInterfaceName()
    {
        return "I" + GetClassName() + "Manager";
    }
    /// <summary>
    /// 服务类名
    /// </summary>
    /// <returns></returns>
    public String GetServiceClassName()
    {
        return "" + GetClassName() + "Service";
    }

    /// <summary>
    /// 读写DTO（无主键）
    /// </summary>
    /// <returns></returns>
    public String GetDtoClassName()
    {
        return GetClassName() + "Dto";
    }
    /// <summary>
    /// 列表DTO(无外键)
    /// </summary>
    /// <returns></returns>
    public String GetListDtoClassName()
    {
        return GetClassName() + "ListDto";
    }
    /// <summary>
    /// 查询DTO(继承QueryDto)
    /// </summary>
    /// <returns></returns>   
    public String GetDataGridInputClassName()
    {
        return GetClassName() + "SearchParams";
    }
    /// <summary>
    /// 获取MVC控制器类名
    /// </summary>
    /// <returns></returns>
    public String GetWebControllerClassName()
    {
        return GetClassName() + "Controller";
    }
    /// <summary>
    /// 获取对象名称（单数）
    /// </summary>
    /// <returns></returns>
    public String GetClassObjectName()
    {
        String str = StringUtil.ToCamelCase(GetClassName());
        if (IsKeyword(str))
            return "@" + str;
        return str;
    }

    /// <summary>
    /// 获取对象名称(复数)
    /// </summary>
    /// <returns></returns>
    public String GetClassesObjectName()
    {
        String str = StringUtil.ToCamelCase(StringUtil.ToPlural(GetClassName()));
        if (IsKeyword(str))
            return "@" + str;
        return str;
    }
    /// <summary>
    /// 主键函数参数（删除或者查找方法的参数）
    /// </summary>
    /// <returns></returns>
    public String GetFuncPrimaryArguments()
    {
        StringBuilder builder = new StringBuilder();
        foreach (var column in this.Table.PrimaryKey.MemberColumns)
        {
            builder.Append(column.SystemType.FullName).Append(" ").Append(StringUtil.ToCamelCase(column.Name)).Append(',');
        }
        return builder.ToString().Trim(',');
    }
    /// <summary>
    /// 主键调用参数（删除或者查找调用方法的参数）
    /// </summary>
    /// <returns></returns>
    public String GetCallPrimaryAruments()
    {
        StringBuilder builder = new StringBuilder();
        foreach (var column in this.Table.PrimaryKey.MemberColumns)
        {
            builder.Append(StringUtil.ToCamelCase(column.Name)).Append(',');
        }
        return builder.ToString().Trim(',');
    }
    
 
 
    /// <summary>
    /// 判断一列是否为自增长
    /// </summary>
    /// <param name="column"></param>
    /// <returns></returns>
    public static bool IsIdentity(SchemaExplorer.ColumnSchema column)
    {
        return (bool)column.ExtendedProperties["CS_IsIdentity"].Value;
    }
    /// <summary>
    /// 默认排序字段
    /// </summary>
    /// <returns></returns>
    public String GetOrderColumnName()
    {
        if (this.Table.PrimaryKey.MemberColumns.Count > 0)
            return "[" + this.Table.PrimaryKey.MemberColumns[0].Name + "]";
        return "[" + this.Table.Columns[0].Name + "]";
    }
    /// <summary>
    /// 默认搜索字段
    /// </summary>
    /// <returns></returns>
    public String GetSearchColumnName()
    {
        foreach (var column in this.Table.NonForeignKeyColumns)
        {
            if (column.IsPrimaryKeyMember)
                continue;
            if (column.SystemType.FullName == "System.String")
                return "[" + column.Name + "]";
        }
        return "[" + this.Table.Columns[0].Name + "]";
    }
    /// <summary>
    /// 主键字段
    /// </summary>
    /// <returns></returns>
    public String GetPrimaryFileds()
    {
        StringBuilder builder = new StringBuilder();
        foreach (var column in this.Table.PrimaryKey.MemberColumns)
        {
            builder.Append("[").Append(column.Name).Append("]").Append(",");
        }
        return builder.ToString().Trim(',');
    }

    /// <summary>
    /// 判断是否是关键字
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static bool IsKeyword(String str)
    {
        List<String> list = new List<String>();
        list.Add("abstract");
        list.Add("as");
        list.Add("base");
        list.Add("bool");
        list.Add("break");
        list.Add("byte");
        list.Add("case");
        list.Add("catch");
        list.Add("char");
        list.Add("checked");
        list.Add("class");
        list.Add("const");
        list.Add("continue");
        list.Add("decimal");
        list.Add("default");
        list.Add("delegate");
        list.Add("do");
        list.Add("double");
        list.Add("else");
        list.Add("enum");
        list.Add("event");
        list.Add("explicit");
        list.Add("extern");
        list.Add("false");
        list.Add("finally");
        list.Add("fixed");
        list.Add("float");
        list.Add("for");
        list.Add("foreach");
        list.Add("goto");
        list.Add("if");
        list.Add("implicit");
        list.Add("in");
        list.Add("int");
        list.Add("interface");
        list.Add("internal");
        list.Add("is");
        list.Add("lock");
        list.Add("long");
        list.Add("namespace");
        list.Add("new");
        list.Add("null");
        list.Add("object");
        list.Add("operator");
        list.Add("out");
        list.Add("override");
        list.Add("params");
        list.Add("private");
        list.Add("protected");
        list.Add("public");
        list.Add("readonly");
        list.Add("ref");
        list.Add("return");
        list.Add("sbyte");
        list.Add("sealed");
        list.Add("short");
        list.Add("sizeof");
        list.Add("stackalloc");
        list.Add("static");
        list.Add("string");
        list.Add("struct");
        list.Add("switch");
        list.Add("this");
        list.Add("throw");
        list.Add("true");
        list.Add("try");
        list.Add("typeof");
        list.Add("uint");
        list.Add("ulong");
        list.Add("unchecked");
        list.Add("unsafe");
        list.Add("ushort");
        list.Add("using");
        list.Add("virtual");
        list.Add("void");
        list.Add("while");
        return list.Contains(str);
    }
    /// <summary>
    /// 获取主键注释
    /// </summary>
    /// <returns></returns>
    public String GetPrimaryAnnotation()
    {
        StringBuilder builder = new StringBuilder();
        int n = 0;
        foreach (var column in this.Table.PrimaryKey.MemberColumns)
        {
            n++;
            builder.Append("/// <param name=\"")
            .Append(GetProtityObjectName(column)).Append("\">")
            .Append(GetProtityDescription(column))
            .Append("</param>");
            if (n < this.Table.PrimaryKey.MemberColumns.Count)
            {
                builder.AppendLine().Append("\t\t");
            }
        }
        return builder.ToString();
    }
    
  /// <summary>
    /// 获取主键
    /// </summary>
    /// <returns></returns>
    public string GetPrimaryKeyName()
    {
        if (this.Table.PrimaryKey.MemberColumns.Count == 0)
        {
            return string.Empty;
        }
        return StringUtil.ToPascalCase(this.Table.PrimaryKey.MemberColumns[0].Name);
    }
    
    public string GetNullablePrimaryKeyType(){
        var column=this.Table.PrimaryKey.MemberColumns[0];
        var theType=column.SystemType;
        var ret= (theType.IsGenericType && theType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)));
        if(ret==false&&theType!=typeof(string))
        return theType.Name+"?";
        return theType.Name;
    }

    /// <summary>
    /// 获取主键数据类型
    /// </summary>
    /// <returns></returns>
    public string GetPrimaryKeyType()
    {
        if (this.Table.PrimaryKey.MemberColumns.Count == 0)
        {
            return string.Empty;
        }
        return this.Table.PrimaryKey.MemberColumns[0].SystemType.Name;
    }
}
