﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeSmith.Engine;


 public  class CodeTemplateHelper{
        
     private SchemaExplorer.TableSchema schema;
     
     public CodeTemplateHelper(SchemaExplorer.TableSchema schema){
         this.schema=schema;
     }
     
        /// <summary>
        /// 实体类名
        /// </summary>
        /// <param name="schema"></param>
        /// <returns></returns>
        public String GetClassName(){
            String tableName=schema.Name;
           int position= tableName.IndexOf("_");
            if(position>0){
                tableName=tableName.Substring(position);
            }
            if(StringUtil.IsPlural(tableName)){
                tableName=StringUtil.ToSingular(tableName);
            }
            if(!char.IsUpper(tableName[0])){
                tableName=StringUtil.ToPascalCase(tableName);
            }
            return tableName;
        }
        
        /// <summary>
        /// 实体描述
        /// </summary>
        /// <param name="schema"></param>
        /// <returns></returns>
        public String GetClassDescription(){
            if(string.IsNullOrWhiteSpace(schema.Description))
                return schema.Name;
            return schema.Description.Replace("\r","").Replace("\n","");
        }
        
        /// <summary>
        /// 从列获取属性名
        /// </summary>
        /// <param name="schema"></param>
        /// <returns></returns>
       public static String GetProtityName(SchemaExplorer.ColumnSchema column){
            String tableName=column.Name;
           int position= tableName.IndexOf("_");
            if(position>0){
                tableName=tableName.Substring(position);
            }
            if(!char.IsUpper(tableName[0])){
                tableName=StringUtil.ToPascalCase(tableName);
            }
            return tableName;
        }
       
       /// <summary>
       /// 从属性获取属性描述
       /// </summary>
       /// <param name="schema"></param>
       /// <returns></returns> 
        public static String GetProtityDescription(SchemaExplorer.ColumnSchema column){
            if(string.IsNullOrWhiteSpace(column.Description))
                return column.Name;
            return column.Description.Replace("\r","").Replace("\n","");
        }
        /// <summary>
        /// 属性对象
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static String GetProtityObjectName(SchemaExplorer.ColumnSchema column){
           String str= StringUtil.ToCamelCase(GetProtityName(column));
            if(IsKeyword(str))
                return "@"+str;
            return str;
        }
        /// <summary>
        /// WEBAPI控制器类名
        /// </summary>
        /// <returns></returns>
        public String GetWebApiClassName(){
            return GetClassName()+"Controller";
        }
        
        
        /// <summary>
        /// 模型类名
        /// </summary>
        /// <returns></returns>
        public String GetModelClassName(){
              return GetClassName()+"Info";
        }
        
        /// <summary>
        /// 数据仓储接口
        /// </summary>
        /// <returns></returns>
        public String GetRepositoryInterfaceName(){
            return "I"+GetClassName()+"Repository";
        }
        /// <summary>
        /// 数据仓储类名
        /// </summary>
        /// <returns></returns>
        public String GetRepositoryClassName(){
            return "R"+GetClassName()+"";
        }
        /// <summary>
        /// 服务接口名
        /// </summary>
        /// <returns></returns>
        public String GetServiceInterfaceName(){
            return "I"+GetClassName()+"Service";
        }
        /// <summary>
        /// 服务类名
        /// </summary>
        /// <returns></returns>
        public String GetServiceClassName(){
            return ""+GetClassName()+"Service";
        }
        
        /// <summary>
        /// 读写DTO（无主键）
        /// </summary>
        /// <returns></returns>
        public String GetDtoClassName(){
            return GetClassName()+"Dto";
        } 
        /// <summary>
        /// 列表DTO(无外键)
        /// </summary>
        /// <returns></returns>
        public String GetListDtoClassName(){
            return GetClassName()+"ListDto";
        }
        /// <summary>
        /// 查询DTO(继承QueryDto)
        /// </summary>
        /// <returns></returns>   
        public String GetDataGridInputClassName(){
            return GetClassName()+"PageRequest";
        }
        
        /// <summary>
        /// 获取对象名称（单数）
        /// </summary>
        /// <returns></returns>
        public String GetClassObjectName(){
            String str= StringUtil.ToCamelCase( GetClassName() );
            if(IsKeyword(str))
                return "@"+str;
            return str;
        }
        
        /// <summary>
        /// 获取对象名称(复数)
        /// </summary>
        /// <returns></returns>
        public String GetClassesObjectName(){
            String str= StringUtil.ToCamelCase( StringUtil.ToPlural(GetClassName()) );
            if(IsKeyword(str))
                return "@"+str;
            return str;
        }
        /// <summary>
        /// 主键函数参数（删除或者查找方法的参数）
        /// </summary>
        /// <returns></returns>
        public String GetFuncPrimaryArguments(){
            StringBuilder builder=new StringBuilder();
            foreach(var column in this.schema.PrimaryKey.MemberColumns){
                builder.Append(column.SystemType.FullName).Append(" ").Append(StringUtil.ToCamelCase(column.Name)).Append(',');
            }
            return builder.ToString().Trim(',');
        }
        /// <summary>
        /// 主键调用参数（删除或者查找调用方法的参数）
        /// </summary>
        /// <returns></returns>
        public String GetCallPrimaryAruments(){
             StringBuilder builder=new StringBuilder();
            foreach(var column in this.schema.PrimaryKey.MemberColumns){
                builder.Append(StringUtil.ToCamelCase(column.Name)).Append(',');
            }
            return builder.ToString().Trim(',');
        }
        /// <summary>
        /// WHERE 语句
        /// </summary>
        /// <returns></returns>
        private String GetWhereSql(){
            StringBuilder builder=new StringBuilder();
            foreach(var column in this.schema.PrimaryKey.MemberColumns){
                builder.AppendLine().Append("\t\t\t\t\t\t");
                builder.Append(" [").Append( column.Name ).Append("] = @").Append(column.Name).Append(',');
            }
            return builder.ToString().Trim(',').Replace(","," AND ");
        }
        /// <summary>
        /// 更新列语句（除了主键）
        /// </summary>
        /// <returns></returns>
        private String GetUpdateColumnSql(){
            StringBuilder builder=new StringBuilder();
            foreach(var column in this.schema.NonPrimaryKeyColumns){
                builder.AppendLine().Append("\t\t\t\t\t\t");
                builder.Append("[").Append( column.Name ).Append("] = @").Append(column.Name).Append(',');
            }
            return builder.ToString().Trim(',');
        }
        /// <summary>
        /// 获取所有列的查询语句
        /// </summary>
        /// <returns></returns>
       public String GetReadColumnSql(){
            StringBuilder builder=new StringBuilder();
            foreach(var column in this.schema.Columns){
                builder.AppendLine().Append("\t\t\t\t\t\t");
                builder.Append(" [").Append( column.Name ).Append("]").Append(',');
            }
            return builder.ToString().Trim(',');
        }
       /// <summary>
       /// INSERT的列
       /// </summary>
       /// <returns></returns>
       private String GetInsertSql1(){
            StringBuilder builder=new StringBuilder();
            foreach(var column in this.schema.Columns){
                if(IsIdentity(column))
                    continue;
                builder.AppendLine().Append("\t\t\t\t\t\t");
                builder.Append("[").Append( column.Name ).Append("]").Append(',');
            }
            return builder.ToString().Trim(',');
       }
       /// <summary>
       /// INSERT变量
       /// </summary>
       /// <returns></returns>
        private String GetInsertSql2(){
            StringBuilder builder=new StringBuilder();
            foreach(var column in this.schema.Columns){
                if(IsIdentity(column))
                    continue;
                builder.AppendLine().Append("\t\t\t\t\t\t");
                builder.Append("@").Append( column.Name ).Append(',');
            }
            return builder.ToString().Trim(',');
       }
        /// <summary>
        /// 生成INSERT 语句
        /// </summary>
        /// <returns></returns>
        public String GetInsertSql(){
            return String.Format("INSERT INTO [{0}] ({1}) VALUES ({2})",this.schema.Name,GetInsertSql1(),GetInsertSql2());
        }
        /// <summary>
        /// 生成UPDATE 语句
        /// </summary>
        /// <returns></returns>
        public String GetUpdateSql(){
            return String.Format("UPDATE [{0}] SET {1} WHERE {2}",this.schema.Name,GetUpdateColumnSql(),GetWhereSql());
        }
        
        /// <summary>
        /// 生成DELETE 语句
        /// </summary>
        /// <returns></returns>
        public String GetDeleteSql(){
            return String.Format("DELETE FROM  [{0}]  WHERE {1}",this.schema.Name,GetWhereSql());
        }
        
        /// <summary>
        /// 生成FIND 语句
        /// </summary>
        /// <returns></returns>
        public String GetFindSql(){
            return String.Format("SELECT {0} FROM  [{1}] WHERE {2}",GetReadColumnSql(),this.schema.Name,GetWhereSql());
        }
        
        /// <summary>
        /// 生成FINDALL 语句
        /// </summary>
        /// <returns></returns>
        public String GetFindAllSql(){
            return String.Format("SELECT  {0} FROM  [{1}] ",GetReadColumnSql(),this.schema.Name);
        }
        /// <summary>
        /// 主键参数个数
        /// </summary>
        /// <returns></returns>
        public int GetPrimaryParamsCount(){
            return this.schema.PrimaryKey.MemberColumns.Count;
        }
        /// <summary>
        /// 更新参数个数
        /// </summary>
        /// <returns></returns>
        public int GetUpdateParamsCount(){
            return this.schema.Columns.Count;
        }
        /// <summary>
        /// 插入语句参数个数 排除自增长列
        /// </summary>
        /// <returns></returns>
        public int GetInsertParamsCount(){
            int total=this.schema.Columns.Count;
            foreach(var column in this.schema.Columns){
                if(IsIdentity(column)){
                    total--;
                }
            }
            return total;
        }
        /// <summary>
        /// 判断一列是否为自增长
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static bool IsIdentity(SchemaExplorer.ColumnSchema column){
            return (bool)column.ExtendedProperties["CS_IsIdentity"].Value;
        }
        /// <summary>
        /// 默认排序字段
        /// </summary>
        /// <returns></returns>
        public String GetOrderColumnName(){
            if(this.schema.PrimaryKey.MemberColumns.Count>0)
                return "["+this.schema.PrimaryKey.MemberColumns[0].Name+"]";
            return "["+this.schema.Columns[0].Name+"]";
        }
        /// <summary>
        /// 默认搜索字段
        /// </summary>
        /// <returns></returns>
        public String GetSearchColumnName(){
            foreach(var column in this.schema.NonForeignKeyColumns){
                if(column.IsPrimaryKeyMember)
                    continue;
                if(column.SystemType.FullName=="System.String")
                    return "["+column.Name+"]";
            }
            return "["+this.schema.Columns[0].Name+"]";
        }
        /// <summary>
        /// 主键字段
        /// </summary>
        /// <returns></returns>
        public String GetPrimaryFileds(){
            StringBuilder builder=new StringBuilder();
            foreach(var column in this.schema.PrimaryKey.MemberColumns){
                builder.Append("[").Append(column.Name).Append("]").Append(",");
            }
            return builder.ToString().Trim(',');
        }
        
        /// <summary>
        /// 判断是否是关键字
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsKeyword(String str){
            List<String> list=new List<String>();
            list.Add("abstract");
            list.Add("as");
            list.Add("base");
            list.Add("bool");
            list.Add("break");
            list.Add("byte");
            list.Add("case");
            list.Add("catch");
            list.Add("char");
            list.Add("checked");
            list.Add("class");
            list.Add("const");
            list.Add("continue");
            list.Add("decimal");
            list.Add("default");
            list.Add("delegate");
            list.Add("do");
            list.Add("double");
            list.Add("else");
            list.Add("enum");
            list.Add("event");
            list.Add("explicit");
            list.Add("extern");
            list.Add("false");
            list.Add("finally");
            list.Add("fixed");
            list.Add("float");
            list.Add("for");
            list.Add("foreach");
            list.Add("goto");
            list.Add("if");
            list.Add("implicit");
            list.Add("in");
            list.Add("int");
            list.Add("interface");
            list.Add("internal");
            list.Add("is");
            list.Add("lock");
            list.Add("long");
            list.Add("namespace");
            list.Add("new");
            list.Add("null");
            list.Add("object");
            list.Add("operator");
            list.Add("out");
            list.Add("override");
            list.Add("params");
            list.Add("private");
            list.Add("protected");
            list.Add("public");
            list.Add("readonly");
            list.Add("ref");
            list.Add("return");
            list.Add("sbyte");
            list.Add("sealed");
            list.Add("short");
            list.Add("sizeof");
            list.Add("stackalloc");
            list.Add("static");
            list.Add("string");
            list.Add("struct");
            list.Add("switch");
            list.Add("this");
            list.Add("throw");
            list.Add("true");
            list.Add("try");
            list.Add("typeof");
            list.Add("uint");
            list.Add("ulong");
            list.Add("unchecked");
            list.Add("unsafe");
            list.Add("ushort");
            list.Add("using");
            list.Add("virtual");
            list.Add("void");
            list.Add("while");
            return list.Contains(str);
        }
        /// <summary>
        /// 获取主键注释
        /// </summary>
        /// <returns></returns>
        public String GetPrimaryAnnotation(){
            StringBuilder builder=new StringBuilder();
            int n=0;
            foreach(var column in this.schema.PrimaryKey.MemberColumns){
                n++;
                builder.Append("/// <param name=\"")
                .Append(GetProtityObjectName(column)).Append("\">")
                .Append( GetProtityDescription(column) )
                .Append("</param>");
                if(n<this.schema.PrimaryKey.MemberColumns.Count){
                    builder.AppendLine().Append("\t\t");
                }
            }
            return builder.ToString();
        }
        /// <summary>
    /// 获取主键
    /// </summary>
    /// <returns></returns>
    public string GetPrimaryKeyName()
    {
        if (this.schema.PrimaryKey.MemberColumns.Count == 0)
        {
            return string.Empty;
        }
        return StringUtil.ToPascalCase(this.schema.PrimaryKey.MemberColumns[0].Name);
    }
    
    public string GetNullablePrimaryKeyType(){
        var column=this.schema.PrimaryKey.MemberColumns[0];
        var theType=column.SystemType;
        var ret= (theType.IsGenericType && theType.
      GetGenericTypeDefinition().Equals
      (typeof(Nullable<>)));
    if(ret==false)
        return theType.Name+"?";
        return theType.Name;
    }

    /// <summary>
    /// 获取主键数据类型
    /// </summary>
    /// <returns></returns>
    public string GetPrimaryKeyType()
    {
        if (this.schema.PrimaryKey.MemberColumns.Count == 0)
        {
            return string.Empty;
        }
        return this.schema.PrimaryKey.MemberColumns[0].SystemType.Name;
    }
    }
