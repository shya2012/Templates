﻿using System;
using System.ComponentModel;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 导航菜单
    /// </summary>
    public class GeNavigationMenuMapper : ClassMapper<GeNavigationMenu>
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeNavigationMenuMapper()
        {
            this.Table("PRIVILEGE_NAVIGATION_MENU");
            #region 主键
            this.Map(x => x.NavigationMenuId).Column("NAVIGATION_MENU_ID").Key(KeyType.Assigned);    
            #endregion
            #region 外键
            this.Map(x => x.ParentNavigationMenuId).Column("PARENT_NAVIGATION_MENU_ID");
            #endregion
            #region 其他
            this.Map(x => x.DisplayName).Column("DISPLAY_NAME");
            this.Map(x => x.Remark).Column("REMARK");
            this.Map(x => x.LinkAddress).Column("LINK_ADDRESS");
            this.Map(x => x.Style).Column("STYLE");
            this.Map(x => x.Icon).Column("ICON");
            this.Map(x => x.OtherAttributes).Column("OTHER_ATTRIBUTES");
            this.Map(x => x.Sequence).Column("SEQUENCE");
            this.Map(x => x.CreationTime).Column("CREATION_TIME");
            this.Map(x => x.ModifyTime).Column("MODIFY_TIME");
            #endregion
            this.AutoMap();
        }
    }
}

