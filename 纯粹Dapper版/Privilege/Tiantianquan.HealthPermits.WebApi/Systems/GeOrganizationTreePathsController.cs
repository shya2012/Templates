﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tiantianquan.Common.UI;
using Tiantianquan.HealthPermits.Domain.Systems;
using Tiantianquan.HealthPermits.Application.Systems;

namespace Tiantianquan.HealthPermits.WebApi.Controllers.Systems
{
     /// <summary>
    /// 机构树
    /// </summary>
    [Au]
    [Route("/Systems/{controller}/{action}")]
    public class GeOrganizationTreePathsController:ApiController
    {
        public  GeOrganizationTreePathsService  GeOrganizationTreePathsService { get;private set; }

        public GeOrganizationTreePathsController(GeOrganizationTreePathsService geOrganizationTreePathsService)
        {
            this.GeOrganizationTreePathsService = geOrganizationTreePathsService;
        }
        
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchParams"></param>
        /// <returns></returns>
        [HttpPost]
        public PageResult<GeOrganizationTreePaths> GetGeOrganizationTreePathsByPage([FromBody]GeOrganizationTreePathsQueryModel search)
        {
            return this.GeOrganizationTreePathsService.GetGeOrganizationTreePathsByPage(search);
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public GeOrganizationTreePathsModel GetGeOrganizationTreePathsById(Guid ancestorOrganizationId,Guid descendantOrganizationId)
        {
            return this.GeOrganizationTreePathsService.GetGeOrganizationTreePathsById(ancestorOrganizationId,descendantOrganizationId);
        }
        /// <summary>
        /// 新增
        /// </summary>
        [HttpPost]
        public void Post([FromBody]GeOrganizationTreePathsModel model)
        {
           this.GeOrganizationTreePathsService.Save(model);
        }
        /// <summary>
        /// 修改
        /// </summary>
        [HttpPost]
        public void Put(Guid ancestorOrganizationId,Guid descendantOrganizationId,[FromBody]GeOrganizationTreePathsModel model)
        {
            this.GeOrganizationTreePathsService.Update(ancestorOrganizationId,descendantOrganizationId, model);
        }
        /// <summary>
        /// 删除
        /// </summary>
        [HttpPost]
        public void Delete(Guid ancestorOrganizationId,Guid descendantOrganizationId)
        {
            this.GeOrganizationTreePathsService.Delete(ancestorOrganizationId,descendantOrganizationId);
        }
       
    }
}


