﻿using System;
using System.ComponentModel;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 用户信息
    /// </summary>
    [Description("用户信息")]
    public class GeApplicationUser : EntityBase
    {
        /// <summary>
        /// 用户编号
        /// </summary>
        [Description("用户编号")]
        public virtual Guid   ApplicationUserId {get;set;}
        /// <summary>
        /// 组织机构编号
        /// </summary>
        [Description("组织机构编号")]
        public virtual Guid   OrganizationId {get;set;}
        /// <summary>
        /// 用户名
        /// </summary>
        [Description("用户名")]
        public virtual String   UserName {get;set;}
        /// <summary>
        /// 密码盐值
        /// </summary>
        [Description("密码盐值")]
        public virtual String   PasswordSalt {get;set;}
        /// <summary>
        /// 密码哈希值
        /// </summary>
        [Description("密码哈希值")]
        public virtual String   PasswordHash {get;set;}
        /// <summary>
        /// 真实姓名
        /// </summary>
        [Description("真实姓名")]
        public virtual String   FullName {get;set;}
        /// <summary>
        /// 昵称
        /// </summary>
        [Description("昵称")]
        public virtual String   NickName {get;set;}
        /// <summary>
        /// 头像
        /// </summary>
        [Description("头像")]
        public virtual String   HeadImageUrl {get;set;}
        /// <summary>
        /// 手机号
        /// </summary>
        [Description("手机号")]
        public virtual String   Phone {get;set;}
        /// <summary>
        /// 电子邮箱
        /// </summary>
        [Description("电子邮箱")]
        public virtual String   Email {get;set;}
        /// <summary>
        /// 地址
        /// </summary>
        [Description("地址")]
        public virtual String   Address {get;set;}
        /// <summary>
        /// 是否锁定
        /// </summary>
        [Description("是否锁定")]
        public virtual Boolean   Locked {get;set;}
        /// <summary>
        /// 锁定时间
        /// </summary>
        [Description("锁定时间")]
        public virtual DateTime ?  LockedTime {get;set;}
        /// <summary>
        /// 是否激活
        /// </summary>
        [Description("是否激活")]
        public virtual Boolean   Actived {get;set;}
        /// <summary>
        /// 激活时间
        /// </summary>
        [Description("激活时间")]
        public virtual DateTime ?  ActivedTime {get;set;}
        /// <summary>
        /// 性别
        /// </summary>
        [Description("性别")]
        public virtual Int32 ?  Sex {get;set;}
        /// <summary>
        /// 出生日期
        /// </summary>
        [Description("出生日期")]
        public virtual DateTime ?  Birthday {get;set;}
        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        public virtual DateTime   CreationTime {get;set;}
        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        public virtual DateTime ?  ModifyTime {get;set;}
        /// <summary>
        /// 第三方登陆OPEN_ID
        /// </summary>
        [Description("第三方登陆OPEN_ID")]
        public virtual String   OpenId {get;set;}
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeApplicationUser()
        {
            UserName = String.Empty;
            PasswordSalt = String.Empty;
            PasswordHash = String.Empty;
            FullName = String.Empty;
            NickName = String.Empty;
            HeadImageUrl = String.Empty;
            Phone = String.Empty;
            Email = String.Empty;
            Address = String.Empty;
            CreationTime = DateTime.Now;
            OpenId = String.Empty;
        }
    }
}

