﻿using System;
using System.ComponentModel;
using FluentValidation;
using FluentValidation.Attributes;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 导航菜单
    /// </summary>
    [Description("导航菜单")]
    public class GeNavigationMenuModelValidator:AbstractValidator<GeNavigationMenuModel> 
    {
      
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeNavigationMenuModelValidator()
        {
            RuleFor(item => item.NavigationMenuId).NotNull().WithMessage("菜单编号不能为空。");
              
            RuleFor(item => item.ParentNavigationMenuId).NotNull().WithMessage("上级菜单编号不能为空。");
              
            RuleFor(item => item.DisplayName).NotNull().WithMessage("显示名称不能为空。");
            RuleFor(item => item.DisplayName).NotEmpty().WithMessage("显示名称不能为空。");
           // RuleFor(item => item.DisplayName).MinimumLength(10).WithMessage("显示名称最少10个字。");
            RuleFor(item => item.DisplayName).MaximumLength(128).WithMessage("显示名称最多128个字。");
              
            RuleFor(item => item.Remark).NotNull().WithMessage("备注不能为空。");
            RuleFor(item => item.Remark).NotEmpty().WithMessage("备注不能为空。");
           // RuleFor(item => item.Remark).MinimumLength(10).WithMessage("备注最少10个字。");
            RuleFor(item => item.Remark).MaximumLength(256).WithMessage("备注最多256个字。");
              
            RuleFor(item => item.LinkAddress).NotNull().WithMessage("链接地址不能为空。");
            RuleFor(item => item.LinkAddress).NotEmpty().WithMessage("链接地址不能为空。");
           // RuleFor(item => item.LinkAddress).MinimumLength(10).WithMessage("链接地址最少10个字。");
            RuleFor(item => item.LinkAddress).MaximumLength(256).WithMessage("链接地址最多256个字。");
              
            RuleFor(item => item.Style).NotNull().WithMessage("样式不能为空。");
            RuleFor(item => item.Style).NotEmpty().WithMessage("样式不能为空。");
           // RuleFor(item => item.Style).MinimumLength(10).WithMessage("样式最少10个字。");
            RuleFor(item => item.Style).MaximumLength(128).WithMessage("样式最多128个字。");
              
            RuleFor(item => item.Icon).NotNull().WithMessage("图标不能为空。");
            RuleFor(item => item.Icon).NotEmpty().WithMessage("图标不能为空。");
           // RuleFor(item => item.Icon).MinimumLength(10).WithMessage("图标最少10个字。");
            RuleFor(item => item.Icon).MaximumLength(256).WithMessage("图标最多256个字。");
              
            RuleFor(item => item.OtherAttributes).NotNull().WithMessage("其他属性不能为空。");
            RuleFor(item => item.OtherAttributes).NotEmpty().WithMessage("其他属性不能为空。");
           // RuleFor(item => item.OtherAttributes).MinimumLength(10).WithMessage("其他属性最少10个字。");
            RuleFor(item => item.OtherAttributes).MaximumLength(4000).WithMessage("其他属性最多4000个字。");
              
            RuleFor(item => item.Sequence).NotNull().WithMessage("排序号不能为空。");
              
            RuleFor(item => item.CreationTime).NotNull().WithMessage("创建时间不能为空。");
              
            RuleFor(item => item.ModifyTime).NotNull().WithMessage("修改时间不能为空。");
              
        }
    }
}

