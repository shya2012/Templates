﻿using System;
using System.ComponentModel;
using FluentValidation;
using FluentValidation.Attributes;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 权限信息
    /// </summary>
    [Description("权限信息")]
    [Validator(typeof(GePermissionModelValidator))]
    public class GePermissionModel 
    {
        /// <summary>
        /// 权限编号
        /// </summary>
        [Description("权限编号")]
        public Guid   PermissionId {get;set;}
        /// <summary>
        /// 菜单编号
        /// </summary>
        [Description("菜单编号")]
        public Guid   NavigationMenuId {get;set;}
        /// <summary>
        /// 角色编号
        /// </summary>
        [Description("角色编号")]
        public Guid   ApplicationRoleId {get;set;}
        /// <summary>
        /// 用户编号
        /// </summary>
        [Description("用户编号")]
        public Guid   ApplicationUserId {get;set;}
        /// <summary>
        /// 是否授权
        /// </summary>
        [Description("是否授权")]
        public Boolean   IsGranted {get;set;}
        /// <summary>
        /// 授权时间
        /// </summary>
        [Description("授权时间")]
        public DateTime   CreationTime {get;set;}
        /// <summary>
        /// 构造函数
        /// </summary>
        public GePermissionModel()
        {
           // CreationTime = DateTime.Now;
        }
    }
}

