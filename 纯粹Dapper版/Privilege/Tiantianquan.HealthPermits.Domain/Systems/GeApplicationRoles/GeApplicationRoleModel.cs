﻿using System;
using System.ComponentModel;
using FluentValidation;
using FluentValidation.Attributes;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 角色信息
    /// </summary>
    [Description("角色信息")]
    [Validator(typeof(GeApplicationRoleModelValidator))]
    public class GeApplicationRoleModel 
    {
        /// <summary>
        /// 角色编号
        /// </summary>
        [Description("角色编号")]
        public Guid   ApplicationRoleId {get;set;}
        /// <summary>
        /// 角色名称
        /// </summary>
        [Description("角色名称")]
        public String   RoleName {get;set;}
        /// <summary>
        /// 角色描述
        /// </summary>
        [Description("角色描述")]
        public String   RoleDescription {get;set;}
        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        public DateTime   CreationTime {get;set;}
        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        public DateTime   ModifyTime {get;set;}
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeApplicationRoleModel()
        {
          //  RoleName = String.Empty;
          //  RoleDescription = String.Empty;
           // CreationTime = DateTime.Now;
           // ModifyTime = DateTime.Now;
        }
    }
}

