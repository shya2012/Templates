﻿using System;
using System.ComponentModel;
using FluentValidation;
using FluentValidation.Attributes;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 组织机构
    /// </summary>
    [Description("组织机构")]
    public class GeOrganizationModelValidator:AbstractValidator<GeOrganizationModel> 
    {
      
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeOrganizationModelValidator()
        {
            RuleFor(item => item.OrganizationId).NotNull().WithMessage("组织机构编号不能为空。");
              
            RuleFor(item => item.ParentOrganizationId).NotNull().WithMessage("上级组织机构编号不能为空。");
              
            RuleFor(item => item.OrganizationCode).NotNull().WithMessage("组织机构代码不能为空。");
            RuleFor(item => item.OrganizationCode).NotEmpty().WithMessage("组织机构代码不能为空。");
           // RuleFor(item => item.OrganizationCode).MinimumLength(10).WithMessage("组织机构代码最少10个字。");
            RuleFor(item => item.OrganizationCode).MaximumLength(128).WithMessage("组织机构代码最多128个字。");
              
            RuleFor(item => item.OrganizationName).NotNull().WithMessage("组织机构名称不能为空。");
            RuleFor(item => item.OrganizationName).NotEmpty().WithMessage("组织机构名称不能为空。");
           // RuleFor(item => item.OrganizationName).MinimumLength(10).WithMessage("组织机构名称最少10个字。");
            RuleFor(item => item.OrganizationName).MaximumLength(128).WithMessage("组织机构名称最多128个字。");
              
            RuleFor(item => item.OrganizationPinyin).NotNull().WithMessage("组织机构拼音不能为空。");
            RuleFor(item => item.OrganizationPinyin).NotEmpty().WithMessage("组织机构拼音不能为空。");
           // RuleFor(item => item.OrganizationPinyin).MinimumLength(10).WithMessage("组织机构拼音最少10个字。");
            RuleFor(item => item.OrganizationPinyin).MaximumLength(128).WithMessage("组织机构拼音最多128个字。");
              
            RuleFor(item => item.OrganizationClasses).NotNull().WithMessage("类别不能为空。");
              
            RuleFor(item => item.OrganizationAddress).NotNull().WithMessage("选址不能为空。");
            RuleFor(item => item.OrganizationAddress).NotEmpty().WithMessage("选址不能为空。");
           // RuleFor(item => item.OrganizationAddress).MinimumLength(10).WithMessage("选址最少10个字。");
            RuleFor(item => item.OrganizationAddress).MaximumLength(256).WithMessage("选址最多256个字。");
              
            RuleFor(item => item.FormsOfOwnership).NotNull().WithMessage("所有制形式不能为空。");
              
            RuleFor(item => item.Beds).NotNull().WithMessage("床位不能为空。");
              
            RuleFor(item => item.ServiceObject).NotNull().WithMessage("服务对象不能为空。");
            RuleFor(item => item.ServiceObject).NotEmpty().WithMessage("服务对象不能为空。");
           // RuleFor(item => item.ServiceObject).MinimumLength(10).WithMessage("服务对象最少10个字。");
            RuleFor(item => item.ServiceObject).MaximumLength(256).WithMessage("服务对象最多256个字。");
              
            RuleFor(item => item.MedicalSubjects).NotNull().WithMessage("医疗科目不能为空。");
            RuleFor(item => item.MedicalSubjects).NotEmpty().WithMessage("医疗科目不能为空。");
           // RuleFor(item => item.MedicalSubjects).MinimumLength(10).WithMessage("医疗科目最少10个字。");
            RuleFor(item => item.MedicalSubjects).MaximumLength(512).WithMessage("医疗科目最多512个字。");
              
            RuleFor(item => item.TotalAmountOfInvestment).NotNull().WithMessage("投资总额不能为空。");
            RuleFor(item => item.TotalAmountOfInvestment).NotEmpty().WithMessage("投资总额不能为空。");
           // RuleFor(item => item.TotalAmountOfInvestment).MinimumLength(10).WithMessage("投资总额最少10个字。");
            RuleFor(item => item.TotalAmountOfInvestment).MaximumLength(256).WithMessage("投资总额最多256个字。");
              
            RuleFor(item => item.RegisteredCapital).NotNull().WithMessage("注册资金不能为空。");
            RuleFor(item => item.RegisteredCapital).NotEmpty().WithMessage("注册资金不能为空。");
           // RuleFor(item => item.RegisteredCapital).MinimumLength(10).WithMessage("注册资金最少10个字。");
            RuleFor(item => item.RegisteredCapital).MaximumLength(256).WithMessage("注册资金最多256个字。");
              
            RuleFor(item => item.BusinessNature).NotNull().WithMessage("BUSINESS_NATURE不能为空。");
              
            RuleFor(item => item.Other).NotNull().WithMessage("其他不能为空。");
            RuleFor(item => item.Other).NotEmpty().WithMessage("其他不能为空。");
           // RuleFor(item => item.Other).MinimumLength(10).WithMessage("其他最少10个字。");
            RuleFor(item => item.Other).MaximumLength(256).WithMessage("其他最多256个字。");
              
            RuleFor(item => item.ValidityOfCertificate).NotNull().WithMessage("证书有效期不能为空。");
              
            RuleFor(item => item.ApprovalNumber).NotNull().WithMessage("批准文号不能为空。");
            RuleFor(item => item.ApprovalNumber).NotEmpty().WithMessage("批准文号不能为空。");
           // RuleFor(item => item.ApprovalNumber).MinimumLength(10).WithMessage("批准文号最少10个字。");
            RuleFor(item => item.ApprovalNumber).MaximumLength(128).WithMessage("批准文号最多128个字。");
              
            RuleFor(item => item.OrganizationTelphone).NotNull().WithMessage("联系电话不能为空。");
            RuleFor(item => item.OrganizationTelphone).NotEmpty().WithMessage("联系电话不能为空。");
           // RuleFor(item => item.OrganizationTelphone).MinimumLength(10).WithMessage("联系电话最少10个字。");
            RuleFor(item => item.OrganizationTelphone).MaximumLength(128).WithMessage("联系电话最多128个字。");
              
        }
    }
}

