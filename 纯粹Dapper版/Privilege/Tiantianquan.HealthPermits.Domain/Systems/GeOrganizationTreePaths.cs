﻿using System;
using System.ComponentModel;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 机构树
    /// </summary>
    [Description("机构树")]
    public class GeOrganizationTreePaths : EntityBase
    {
        /// <summary>
        /// AncestorOrganizationId
        /// </summary>
        [Description("AncestorOrganizationId")]
        public virtual Guid   AncestorOrganizationId {get;set;}
        /// <summary>
        /// DescendantOrganizationId
        /// </summary>
        [Description("DescendantOrganizationId")]
        public virtual Guid   DescendantOrganizationId {get;set;}
        /// <summary>
        /// OrganizationDeep
        /// </summary>
        [Description("OrganizationDeep")]
        public virtual Int32 ?  OrganizationDeep {get;set;}
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeOrganizationTreePaths()
        {
        }
    }
}

