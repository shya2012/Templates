﻿using System;
using System.ComponentModel;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 导航菜单
    /// </summary>
    [Description("导航菜单")]
    public class GeNavigationMenu : EntityBase
    {
        /// <summary>
        /// 菜单编号
        /// </summary>
        [Description("菜单编号")]
        public virtual Guid   NavigationMenuId {get;set;}
        /// <summary>
        /// 上级菜单编号
        /// </summary>
        [Description("上级菜单编号")]
        public virtual Guid   ParentNavigationMenuId {get;set;}
        /// <summary>
        /// 显示名称
        /// </summary>
        [Description("显示名称")]
        public virtual String   DisplayName {get;set;}
        /// <summary>
        /// 备注
        /// </summary>
        [Description("备注")]
        public virtual String   Remark {get;set;}
        /// <summary>
        /// 链接地址
        /// </summary>
        [Description("链接地址")]
        public virtual String   LinkAddress {get;set;}
        /// <summary>
        /// 样式
        /// </summary>
        [Description("样式")]
        public virtual String   Style {get;set;}
        /// <summary>
        /// 图标
        /// </summary>
        [Description("图标")]
        public virtual String   Icon {get;set;}
        /// <summary>
        /// 其他属性
        /// </summary>
        [Description("其他属性")]
        public virtual String   OtherAttributes {get;set;}
        /// <summary>
        /// 排序号
        /// </summary>
        [Description("排序号")]
        public virtual Int64   Sequence {get;set;}
        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        public virtual DateTime   CreationTime {get;set;}
        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        public virtual DateTime ?  ModifyTime {get;set;}
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeNavigationMenu()
        {
            DisplayName = String.Empty;
            Remark = String.Empty;
            LinkAddress = String.Empty;
            Style = String.Empty;
            Icon = String.Empty;
            OtherAttributes = String.Empty;
            CreationTime = DateTime.Now;
        }
    }
}

