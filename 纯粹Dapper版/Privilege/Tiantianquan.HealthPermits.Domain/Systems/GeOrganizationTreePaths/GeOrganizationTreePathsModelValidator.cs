﻿using System;
using System.ComponentModel;
using FluentValidation;
using FluentValidation.Attributes;
using Tiantianquan.Common.Domain;

namespace Tiantianquan.HealthPermits.Domain.Systems
{
    /// <summary>
    /// 机构树
    /// </summary>
    [Description("机构树")]
    public class GeOrganizationTreePathsModelValidator:AbstractValidator<GeOrganizationTreePathsModel> 
    {
      
        /// <summary>
        /// 构造函数
        /// </summary>
        public GeOrganizationTreePathsModelValidator()
        {
            RuleFor(item => item.AncestorOrganizationId).NotNull().WithMessage("AncestorOrganizationId不能为空。");
              
            RuleFor(item => item.DescendantOrganizationId).NotNull().WithMessage("DescendantOrganizationId不能为空。");
              
            RuleFor(item => item.OrganizationDeep).NotNull().WithMessage("OrganizationDeep不能为空。");
              
        }
    }
}

