﻿using System;

namespace Tiantianquan.KindleBook.Core
{
    public class EntityInfo
    {
        public virtual DateTime CreatedAt { get; set; }

        public virtual DateTime UpdatedAt { get; set; }

        public virtual long Sequence { get; set; }
    }
}
