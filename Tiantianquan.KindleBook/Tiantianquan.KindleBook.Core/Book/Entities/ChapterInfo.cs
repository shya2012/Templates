﻿using System;

namespace Tiantianquan.KindleBook.Core.Book.Entities
{
    public class ChapterInfo: EntityInfo
    {
        public virtual Guid ChapterId { get; set; }

        public virtual Guid BookId { get; set; }

        public virtual string ChapterTitle { get; set; }

        public virtual string ChapterUrl { get; set; }

        public virtual string ChapterHtmlContent { get; set; }

        public virtual string ChapterTextContent { get; set; }

        public virtual bool IsNewChapter { get; set; }

        public virtual bool IsDownload { get; set; }
    }
}
