﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tiantianquan.KindleBook.Core.Book.Entities
{
    public class BookInfo: EntityInfo
    {
        public virtual Guid BookId { get; set; }

        public virtual string BookName { get; set; }

        public virtual string BookAuthor { get; set; }

        public virtual string BookIntro { get; set; }

        public virtual string BookUrl { get; set; }
    }
}
