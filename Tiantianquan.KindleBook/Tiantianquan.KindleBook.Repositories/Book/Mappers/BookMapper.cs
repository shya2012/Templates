﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using Tiantianquan.KindleBook.Core.Book.Entities;

namespace Tiantianquan.KindleBook.Repositories.Book.Mappers
{
    public class BookMapper : ClassMap<BookInfo>
    {
        public BookMapper()
        {
            this.Table("Book_Book");
            this.Id(x => x.BookId).GeneratedBy.Assigned();
            this.Map(x => x.BookName);
            this.Map(x => x.BookAuthor);
            this.Map(x => x.BookUrl);
            this.Map(x => x.BookIntro);

            this.Map(x => x.CreatedAt);
            this.Map(x => x.UpdatedAt);
            this.Map(x => x.Sequence);
        }
    }

    public class ChapterMapper : ClassMap<ChapterInfo>
    {
        public ChapterMapper()
        {
            this.Table("Book_Chapter");
            this.Id(x => x.ChapterId).GeneratedBy.Assigned();

            this.Map(x => x.BookId);
            this.Map(x => x.ChapterTitle);
            this.Map(x => x.ChapterUrl);
            this.Map(x => x.ChapterHtmlContent);
            this.Map(x => x.ChapterTextContent);
            this.Map(x => x.IsDownload);
            this.Map(x => x.IsNewChapter);

            this.Map(x => x.CreatedAt);
            this.Map(x => x.UpdatedAt);
            this.Map(x => x.Sequence);
        }
    }
}
