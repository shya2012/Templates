﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Commanding;

namespace Mall.Commands.PetShop
{
    /// <summary>
    /// Category
    /// </summary>
   public class CategoryCreateCommand:Command
    {
        /// <summary>
        /// Name
        /// </summary>
        public  string Name{get; set;}
        /// <summary>
        /// Descn
        /// </summary>
        public  string Descn{get; set;}
        
        private CategoryCreateCommand(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="name">Name</param>
		/// <param name="descn">Descn</param>
		/// <returns></returns>
        public CategoryCreateCommand(string id,string name,string descn):base(id)
        {
            this.Name = name;
            this.Descn = descn;
        }
    }
    
    /// <summary>
    /// Category
    /// </summary>
   public class CategoryUpdateCommand:Command
    {
        /// <summary>
        /// Name
        /// </summary>
        public  string Name{get;private set;}
        /// <summary>
        /// Descn
        /// </summary>
        public  string Descn{get;private set;}
        
        private CategoryUpdateCommand(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="name">Name</param>
		/// <param name="descn">Descn</param>
		/// <returns></returns>
        public CategoryUpdateCommand(string id,string name,string descn):base(id)
        {
            this.Name = name;
            this.Descn = descn;
        }
    }
    
    /// <summary>
    /// Category
    /// </summary>
   public class CategoryDeleteCommand:Command
    {
        private CategoryDeleteCommand(){}
        
        public CategoryDeleteCommand(string id):base(id){}
    }
}

