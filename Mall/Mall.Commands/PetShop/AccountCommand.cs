﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Commanding;

namespace Mall.Commands.PetShop
{
    /// <summary>
    /// Account
    /// </summary>
   public class AccountCreateCommand:Command
    {
        /// <summary>
        /// UniqueID
        /// </summary>
        public  int UniqueID{get; set;}
        /// <summary>
        /// Email
        /// </summary>
        public  string Email{get; set;}
        /// <summary>
        /// FirstName
        /// </summary>
        public  string FirstName{get; set;}
        /// <summary>
        /// LastName
        /// </summary>
        public  string LastName{get; set;}
        /// <summary>
        /// Address1
        /// </summary>
        public  string Address1{get; set;}
        /// <summary>
        /// Address2
        /// </summary>
        public  string Address2{get; set;}
        /// <summary>
        /// City
        /// </summary>
        public  string City{get; set;}
        /// <summary>
        /// State
        /// </summary>
        public  string State{get; set;}
        /// <summary>
        /// Zip
        /// </summary>
        public  string Zip{get; set;}
        /// <summary>
        /// Country
        /// </summary>
        public  string Country{get; set;}
        /// <summary>
        /// Phone
        /// </summary>
        public  string Phone{get; set;}
        
        private AccountCreateCommand(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="uniqueID">UniqueID</param>
		/// <param name="email">Email</param>
		/// <param name="firstName">FirstName</param>
		/// <param name="lastName">LastName</param>
		/// <param name="address1">Address1</param>
		/// <param name="address2">Address2</param>
		/// <param name="city">City</param>
		/// <param name="state">State</param>
		/// <param name="zip">Zip</param>
		/// <param name="country">Country</param>
		/// <param name="phone">Phone</param>
		/// <returns></returns>
        public AccountCreateCommand(string id,int uniqueID,string email,string firstName,string lastName,string address1,string address2,string city,string state,string zip,string country,string phone):base(id)
        {
            this.UniqueID = uniqueID;
            this.Email = email;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Address1 = address1;
            this.Address2 = address2;
            this.City = city;
            this.State = state;
            this.Zip = zip;
            this.Country = country;
            this.Phone = phone;
        }
    }
    
    /// <summary>
    /// Account
    /// </summary>
   public class AccountUpdateCommand:Command
    {
        /// <summary>
        /// UniqueID
        /// </summary>
        public  int UniqueID{get;private set;}
        /// <summary>
        /// Email
        /// </summary>
        public  string Email{get;private set;}
        /// <summary>
        /// FirstName
        /// </summary>
        public  string FirstName{get;private set;}
        /// <summary>
        /// LastName
        /// </summary>
        public  string LastName{get;private set;}
        /// <summary>
        /// Address1
        /// </summary>
        public  string Address1{get;private set;}
        /// <summary>
        /// Address2
        /// </summary>
        public  string Address2{get;private set;}
        /// <summary>
        /// City
        /// </summary>
        public  string City{get;private set;}
        /// <summary>
        /// State
        /// </summary>
        public  string State{get;private set;}
        /// <summary>
        /// Zip
        /// </summary>
        public  string Zip{get;private set;}
        /// <summary>
        /// Country
        /// </summary>
        public  string Country{get;private set;}
        /// <summary>
        /// Phone
        /// </summary>
        public  string Phone{get;private set;}
        
        private AccountUpdateCommand(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="uniqueID">UniqueID</param>
		/// <param name="email">Email</param>
		/// <param name="firstName">FirstName</param>
		/// <param name="lastName">LastName</param>
		/// <param name="address1">Address1</param>
		/// <param name="address2">Address2</param>
		/// <param name="city">City</param>
		/// <param name="state">State</param>
		/// <param name="zip">Zip</param>
		/// <param name="country">Country</param>
		/// <param name="phone">Phone</param>
		/// <returns></returns>
        public AccountUpdateCommand(string id,int uniqueID,string email,string firstName,string lastName,string address1,string address2,string city,string state,string zip,string country,string phone):base(id)
        {
            this.UniqueID = uniqueID;
            this.Email = email;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Address1 = address1;
            this.Address2 = address2;
            this.City = city;
            this.State = state;
            this.Zip = zip;
            this.Country = country;
            this.Phone = phone;
        }
    }
    
    /// <summary>
    /// Account
    /// </summary>
   public class AccountDeleteCommand:Command
    {
        private AccountDeleteCommand(){}
        
        public AccountDeleteCommand(string id):base(id){}
    }
}

