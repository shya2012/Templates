﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Commanding;

namespace Mall.Commands.PetShop
{
    /// <summary>
    /// Product
    /// </summary>
   public class ProductCreateCommand:Command
    {
        /// <summary>
        /// CategoryId
        /// </summary>
        public  string CategoryId{get; set;}
        /// <summary>
        /// Name
        /// </summary>
        public  string Name{get; set;}
        /// <summary>
        /// Descn
        /// </summary>
        public  string Descn{get; set;}
        /// <summary>
        /// Image
        /// </summary>
        public  string Image{get; set;}
        
        private ProductCreateCommand(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="categoryId">CategoryId</param>
		/// <param name="name">Name</param>
		/// <param name="descn">Descn</param>
		/// <param name="image">Image</param>
		/// <returns></returns>
        public ProductCreateCommand(string id,string categoryId,string name,string descn,string image):base(id)
        {
            this.CategoryId = categoryId;
            this.Name = name;
            this.Descn = descn;
            this.Image = image;
        }
    }
    
    /// <summary>
    /// Product
    /// </summary>
   public class ProductUpdateCommand:Command
    {
        /// <summary>
        /// CategoryId
        /// </summary>
        public  string CategoryId{get;private set;}
        /// <summary>
        /// Name
        /// </summary>
        public  string Name{get;private set;}
        /// <summary>
        /// Descn
        /// </summary>
        public  string Descn{get;private set;}
        /// <summary>
        /// Image
        /// </summary>
        public  string Image{get;private set;}
        
        private ProductUpdateCommand(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="categoryId">CategoryId</param>
		/// <param name="name">Name</param>
		/// <param name="descn">Descn</param>
		/// <param name="image">Image</param>
		/// <returns></returns>
        public ProductUpdateCommand(string id,string categoryId,string name,string descn,string image):base(id)
        {
            this.CategoryId = categoryId;
            this.Name = name;
            this.Descn = descn;
            this.Image = image;
        }
    }
    
    /// <summary>
    /// Product
    /// </summary>
   public class ProductDeleteCommand:Command
    {
        private ProductDeleteCommand(){}
        
        public ProductDeleteCommand(string id):base(id){}
    }
}

