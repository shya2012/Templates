﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Commanding;

namespace Mall.Commands.PetShop
{
    /// <summary>
    /// Orders
    /// </summary>
   public class OrdersCreateCommand:Command
    {
        /// <summary>
        /// UserId
        /// </summary>
        public  string UserId{get; set;}
        /// <summary>
        /// OrderDate
        /// </summary>
        public  DateTime OrderDate{get; set;}
        /// <summary>
        /// ShipAddr1
        /// </summary>
        public  string ShipAddr1{get; set;}
        /// <summary>
        /// ShipAddr2
        /// </summary>
        public  string ShipAddr2{get; set;}
        /// <summary>
        /// ShipCity
        /// </summary>
        public  string ShipCity{get; set;}
        /// <summary>
        /// ShipState
        /// </summary>
        public  string ShipState{get; set;}
        /// <summary>
        /// ShipZip
        /// </summary>
        public  string ShipZip{get; set;}
        /// <summary>
        /// ShipCountry
        /// </summary>
        public  string ShipCountry{get; set;}
        /// <summary>
        /// BillAddr1
        /// </summary>
        public  string BillAddr1{get; set;}
        /// <summary>
        /// BillAddr2
        /// </summary>
        public  string BillAddr2{get; set;}
        /// <summary>
        /// BillCity
        /// </summary>
        public  string BillCity{get; set;}
        /// <summary>
        /// BillState
        /// </summary>
        public  string BillState{get; set;}
        /// <summary>
        /// BillZip
        /// </summary>
        public  string BillZip{get; set;}
        /// <summary>
        /// BillCountry
        /// </summary>
        public  string BillCountry{get; set;}
        /// <summary>
        /// Courier
        /// </summary>
        public  string Courier{get; set;}
        /// <summary>
        /// TotalPrice
        /// </summary>
        public  string TotalPrice{get; set;}
        /// <summary>
        /// BillToFirstName
        /// </summary>
        public  string BillToFirstName{get; set;}
        /// <summary>
        /// BillToLastName
        /// </summary>
        public  string BillToLastName{get; set;}
        /// <summary>
        /// ShipToFirstName
        /// </summary>
        public  string ShipToFirstName{get; set;}
        /// <summary>
        /// ShipToLastName
        /// </summary>
        public  string ShipToLastName{get; set;}
        /// <summary>
        /// AuthorizationNumber
        /// </summary>
        public  int AuthorizationNumber{get; set;}
        /// <summary>
        /// Locale
        /// </summary>
        public  string Locale{get; set;}
        
        private OrdersCreateCommand(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="userId">UserId</param>
		/// <param name="orderDate">OrderDate</param>
		/// <param name="shipAddr1">ShipAddr1</param>
		/// <param name="shipAddr2">ShipAddr2</param>
		/// <param name="shipCity">ShipCity</param>
		/// <param name="shipState">ShipState</param>
		/// <param name="shipZip">ShipZip</param>
		/// <param name="shipCountry">ShipCountry</param>
		/// <param name="billAddr1">BillAddr1</param>
		/// <param name="billAddr2">BillAddr2</param>
		/// <param name="billCity">BillCity</param>
		/// <param name="billState">BillState</param>
		/// <param name="billZip">BillZip</param>
		/// <param name="billCountry">BillCountry</param>
		/// <param name="courier">Courier</param>
		/// <param name="totalPrice">TotalPrice</param>
		/// <param name="billToFirstName">BillToFirstName</param>
		/// <param name="billToLastName">BillToLastName</param>
		/// <param name="shipToFirstName">ShipToFirstName</param>
		/// <param name="shipToLastName">ShipToLastName</param>
		/// <param name="authorizationNumber">AuthorizationNumber</param>
		/// <param name="locale">Locale</param>
		/// <returns></returns>
        public OrdersCreateCommand(string id,string userId,DateTime orderDate,string shipAddr1,string shipAddr2,string shipCity,string shipState,string shipZip,string shipCountry,string billAddr1,string billAddr2,string billCity,string billState,string billZip,string billCountry,string courier,string totalPrice,string billToFirstName,string billToLastName,string shipToFirstName,string shipToLastName,int authorizationNumber,string locale):base(id)
        {
            this.UserId = userId;
            this.OrderDate = orderDate;
            this.ShipAddr1 = shipAddr1;
            this.ShipAddr2 = shipAddr2;
            this.ShipCity = shipCity;
            this.ShipState = shipState;
            this.ShipZip = shipZip;
            this.ShipCountry = shipCountry;
            this.BillAddr1 = billAddr1;
            this.BillAddr2 = billAddr2;
            this.BillCity = billCity;
            this.BillState = billState;
            this.BillZip = billZip;
            this.BillCountry = billCountry;
            this.Courier = courier;
            this.TotalPrice = totalPrice;
            this.BillToFirstName = billToFirstName;
            this.BillToLastName = billToLastName;
            this.ShipToFirstName = shipToFirstName;
            this.ShipToLastName = shipToLastName;
            this.AuthorizationNumber = authorizationNumber;
            this.Locale = locale;
        }
    }
    
    /// <summary>
    /// Orders
    /// </summary>
   public class OrdersUpdateCommand:Command
    {
        /// <summary>
        /// UserId
        /// </summary>
        public  string UserId{get;private set;}
        /// <summary>
        /// OrderDate
        /// </summary>
        public  DateTime OrderDate{get;private set;}
        /// <summary>
        /// ShipAddr1
        /// </summary>
        public  string ShipAddr1{get;private set;}
        /// <summary>
        /// ShipAddr2
        /// </summary>
        public  string ShipAddr2{get;private set;}
        /// <summary>
        /// ShipCity
        /// </summary>
        public  string ShipCity{get;private set;}
        /// <summary>
        /// ShipState
        /// </summary>
        public  string ShipState{get;private set;}
        /// <summary>
        /// ShipZip
        /// </summary>
        public  string ShipZip{get;private set;}
        /// <summary>
        /// ShipCountry
        /// </summary>
        public  string ShipCountry{get;private set;}
        /// <summary>
        /// BillAddr1
        /// </summary>
        public  string BillAddr1{get;private set;}
        /// <summary>
        /// BillAddr2
        /// </summary>
        public  string BillAddr2{get;private set;}
        /// <summary>
        /// BillCity
        /// </summary>
        public  string BillCity{get;private set;}
        /// <summary>
        /// BillState
        /// </summary>
        public  string BillState{get;private set;}
        /// <summary>
        /// BillZip
        /// </summary>
        public  string BillZip{get;private set;}
        /// <summary>
        /// BillCountry
        /// </summary>
        public  string BillCountry{get;private set;}
        /// <summary>
        /// Courier
        /// </summary>
        public  string Courier{get;private set;}
        /// <summary>
        /// TotalPrice
        /// </summary>
        public  string TotalPrice{get;private set;}
        /// <summary>
        /// BillToFirstName
        /// </summary>
        public  string BillToFirstName{get;private set;}
        /// <summary>
        /// BillToLastName
        /// </summary>
        public  string BillToLastName{get;private set;}
        /// <summary>
        /// ShipToFirstName
        /// </summary>
        public  string ShipToFirstName{get;private set;}
        /// <summary>
        /// ShipToLastName
        /// </summary>
        public  string ShipToLastName{get;private set;}
        /// <summary>
        /// AuthorizationNumber
        /// </summary>
        public  int AuthorizationNumber{get;private set;}
        /// <summary>
        /// Locale
        /// </summary>
        public  string Locale{get;private set;}
        
        private OrdersUpdateCommand(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="userId">UserId</param>
		/// <param name="orderDate">OrderDate</param>
		/// <param name="shipAddr1">ShipAddr1</param>
		/// <param name="shipAddr2">ShipAddr2</param>
		/// <param name="shipCity">ShipCity</param>
		/// <param name="shipState">ShipState</param>
		/// <param name="shipZip">ShipZip</param>
		/// <param name="shipCountry">ShipCountry</param>
		/// <param name="billAddr1">BillAddr1</param>
		/// <param name="billAddr2">BillAddr2</param>
		/// <param name="billCity">BillCity</param>
		/// <param name="billState">BillState</param>
		/// <param name="billZip">BillZip</param>
		/// <param name="billCountry">BillCountry</param>
		/// <param name="courier">Courier</param>
		/// <param name="totalPrice">TotalPrice</param>
		/// <param name="billToFirstName">BillToFirstName</param>
		/// <param name="billToLastName">BillToLastName</param>
		/// <param name="shipToFirstName">ShipToFirstName</param>
		/// <param name="shipToLastName">ShipToLastName</param>
		/// <param name="authorizationNumber">AuthorizationNumber</param>
		/// <param name="locale">Locale</param>
		/// <returns></returns>
        public OrdersUpdateCommand(string id,string userId,DateTime orderDate,string shipAddr1,string shipAddr2,string shipCity,string shipState,string shipZip,string shipCountry,string billAddr1,string billAddr2,string billCity,string billState,string billZip,string billCountry,string courier,string totalPrice,string billToFirstName,string billToLastName,string shipToFirstName,string shipToLastName,int authorizationNumber,string locale):base(id)
        {
            this.UserId = userId;
            this.OrderDate = orderDate;
            this.ShipAddr1 = shipAddr1;
            this.ShipAddr2 = shipAddr2;
            this.ShipCity = shipCity;
            this.ShipState = shipState;
            this.ShipZip = shipZip;
            this.ShipCountry = shipCountry;
            this.BillAddr1 = billAddr1;
            this.BillAddr2 = billAddr2;
            this.BillCity = billCity;
            this.BillState = billState;
            this.BillZip = billZip;
            this.BillCountry = billCountry;
            this.Courier = courier;
            this.TotalPrice = totalPrice;
            this.BillToFirstName = billToFirstName;
            this.BillToLastName = billToLastName;
            this.ShipToFirstName = shipToFirstName;
            this.ShipToLastName = shipToLastName;
            this.AuthorizationNumber = authorizationNumber;
            this.Locale = locale;
        }
    }
    
    /// <summary>
    /// Orders
    /// </summary>
   public class OrdersDeleteCommand:Command
    {
        private OrdersDeleteCommand(){}
        
        public OrdersDeleteCommand(string id):base(id){}
    }
}

