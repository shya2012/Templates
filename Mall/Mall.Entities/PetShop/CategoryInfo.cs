﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mall.Infrastructure.Data;

namespace Mall.Entities.PetShop
{
    /// <summary>
    /// Category
    /// </summary>
    [FreeSql.DataAnnotations.Table(Name = "Category")]
   public class CategoryInfo:Model
    {
        /// <summary>
        /// Name
        /// </summary>
        public  string Name{get;set;}
        /// <summary>
        /// Descn
        /// </summary>
        public  string Descn{get;set;}
    }
}

