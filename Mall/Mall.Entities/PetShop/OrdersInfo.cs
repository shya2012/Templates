﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mall.Infrastructure.Data;

namespace Mall.Entities.PetShop
{
    /// <summary>
    /// Orders
    /// </summary>
    [FreeSql.DataAnnotations.Table(Name = "Orders")]
   public class OrdersInfo:Model
    {
        /// <summary>
        /// UserId
        /// </summary>
        public  string UserId{get;set;}
        /// <summary>
        /// OrderDate
        /// </summary>
        public  DateTime OrderDate{get;set;}
        /// <summary>
        /// ShipAddr1
        /// </summary>
        public  string ShipAddr1{get;set;}
        /// <summary>
        /// ShipAddr2
        /// </summary>
        public  string ShipAddr2{get;set;}
        /// <summary>
        /// ShipCity
        /// </summary>
        public  string ShipCity{get;set;}
        /// <summary>
        /// ShipState
        /// </summary>
        public  string ShipState{get;set;}
        /// <summary>
        /// ShipZip
        /// </summary>
        public  string ShipZip{get;set;}
        /// <summary>
        /// ShipCountry
        /// </summary>
        public  string ShipCountry{get;set;}
        /// <summary>
        /// BillAddr1
        /// </summary>
        public  string BillAddr1{get;set;}
        /// <summary>
        /// BillAddr2
        /// </summary>
        public  string BillAddr2{get;set;}
        /// <summary>
        /// BillCity
        /// </summary>
        public  string BillCity{get;set;}
        /// <summary>
        /// BillState
        /// </summary>
        public  string BillState{get;set;}
        /// <summary>
        /// BillZip
        /// </summary>
        public  string BillZip{get;set;}
        /// <summary>
        /// BillCountry
        /// </summary>
        public  string BillCountry{get;set;}
        /// <summary>
        /// Courier
        /// </summary>
        public  string Courier{get;set;}
        /// <summary>
        /// TotalPrice
        /// </summary>
        public  string TotalPrice{get;set;}
        /// <summary>
        /// BillToFirstName
        /// </summary>
        public  string BillToFirstName{get;set;}
        /// <summary>
        /// BillToLastName
        /// </summary>
        public  string BillToLastName{get;set;}
        /// <summary>
        /// ShipToFirstName
        /// </summary>
        public  string ShipToFirstName{get;set;}
        /// <summary>
        /// ShipToLastName
        /// </summary>
        public  string ShipToLastName{get;set;}
        /// <summary>
        /// AuthorizationNumber
        /// </summary>
        public  int AuthorizationNumber{get;set;}
        /// <summary>
        /// Locale
        /// </summary>
        public  string Locale{get;set;}
    }
}

