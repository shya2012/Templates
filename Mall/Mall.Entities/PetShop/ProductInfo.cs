﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mall.Infrastructure.Data;

namespace Mall.Entities.PetShop
{
    /// <summary>
    /// Product
    /// </summary>
    [FreeSql.DataAnnotations.Table(Name = "Product")]
   public class ProductInfo:Model
    {
        /// <summary>
        /// CategoryId
        /// </summary>
        public  string CategoryId{get;set;}
        /// <summary>
        /// Name
        /// </summary>
        public  string Name{get;set;}
        /// <summary>
        /// Descn
        /// </summary>
        public  string Descn{get;set;}
        /// <summary>
        /// Image
        /// </summary>
        public  string Image{get;set;}
    }
}

