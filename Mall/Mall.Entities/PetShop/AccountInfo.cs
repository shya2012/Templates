﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mall.Infrastructure.Data;

namespace Mall.Entities.PetShop
{
    /// <summary>
    /// Account
    /// </summary>
    [FreeSql.DataAnnotations.Table(Name = "Account")]
   public class AccountInfo:Model
    {
        /// <summary>
        /// UniqueID
        /// </summary>
        public  int UniqueID{get;set;}
        /// <summary>
        /// Email
        /// </summary>
        public  string Email{get;set;}
        /// <summary>
        /// FirstName
        /// </summary>
        public  string FirstName{get;set;}
        /// <summary>
        /// LastName
        /// </summary>
        public  string LastName{get;set;}
        /// <summary>
        /// Address1
        /// </summary>
        public  string Address1{get;set;}
        /// <summary>
        /// Address2
        /// </summary>
        public  string Address2{get;set;}
        /// <summary>
        /// City
        /// </summary>
        public  string City{get;set;}
        /// <summary>
        /// State
        /// </summary>
        public  string State{get;set;}
        /// <summary>
        /// Zip
        /// </summary>
        public  string Zip{get;set;}
        /// <summary>
        /// Country
        /// </summary>
        public  string Country{get;set;}
        /// <summary>
        /// Phone
        /// </summary>
        public  string Phone{get;set;}
    }
}

