﻿using ECommon.Components;
using Mall.Entities;
using Mall.Entities.PetShop;
using Mall.Infrastructure.Data;
using System;
using System.Collections.Generic;

namespace Mall.QueryService
{

    public class ProductRequest
    {

    }

    public class ProductQueryService:RepositoryBase<ProductInfo,string>
    {

        public List<ProductInfo> GetAllProducts()
        {
            return this.Select.ToList();
        }

        public List<ProductInfo> GetProductByPage(ProductRequest request)
        {
            var select = this.Select;
            long total = 0;
            var items= select.Count(out total).Page(1, 20).ToList();
            return items;
        }

    }



    
}
