﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Mall.Application.PetShop;
using Mall.Domain.PetShop;
using Mall.Repository.PetShop;

namespace Microsoft.Extensions.DependencyInjection
{
    public static partial class PetShopExtension
    {
        public static IServiceCollection RegisterPetShopModule(this IServiceCollection services)
        {
           //Account
           services.AddScoped<AccountService,AccountService>();
           services.AddScoped<AccountRepository,AccountRepositoryImpl>();
           //Category
           services.AddScoped<CategoryService,CategoryService>();
           services.AddScoped<CategoryRepository,CategoryRepositoryImpl>();
           //Orders
           services.AddScoped<OrdersService,OrdersService>();
           services.AddScoped<OrdersRepository,OrdersRepositoryImpl>();
           //Product
           services.AddScoped<ProductService,ProductService>();
           services.AddScoped<ProductRepository,ProductRepositoryImpl>();
            return services;
        }
    }
}


