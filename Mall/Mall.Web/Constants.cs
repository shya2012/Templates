﻿namespace Mall.Web
{
    public class Constants
    {
        public const string CommandTopic = "BankTransferCommandTopic";
        public const string EventTopic = "BankTransferEventTopic";
        public const string ApplicationMessageTopic = "BankTransferApplicationMessageTopic";
        public const string ExceptionTopic = "BankTransferExceptionTopic";

        //public const string NoteCommandTopic = "NoteCommandTopic";
        //public const string NoteEventTopic = "NoteEventTopic";
    }

    public class ConfigSettings
    {
        public static string ForumConnectionString { get; set; }
        public static string ENodeConnectionString { get; set; }
        public static int NameServerPort { get; set; }
        public static int BrokerProducerPort { get; set; }
        public static int BrokerConsumerPort { get; set; }
        public static int BrokerAdminPort { get; set; }

        static ConfigSettings()
        {
            NameServerPort = 10191;
            BrokerProducerPort = 10192;
            BrokerConsumerPort = 10193;
            BrokerAdminPort = 10194;

       
        }
    }
}
