using Autofac;
using Autofac.Extensions.DependencyInjection;
using ECommon.Configurations;
using ECommon.Logging;
using ECommon.Serilog;
using ENode.Configurations;
using ENode.MySQL;
using Mall.Infrastructure.Data;
using Mall.QueryService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Mall.Web
{
    public class user
    {
        public string name = "屠夫";
    }
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        public void ConfigureServices(IServiceCollection services)
        {
            ConfigSettings.ForumConnectionString = Configuration.GetConnectionString("forum");
            ConfigSettings.ENodeConnectionString = Configuration.GetConnectionString("enode");

            IFreeSql fsql = new FreeSql.FreeSqlBuilder()
  .UseConnectionString(FreeSql.DataType.MySql, ConfigSettings.ForumConnectionString)
  .UseNoneCommandParameter(true)
  .UseNameConvert(FreeSql.Internal.NameConvertType.PascalCaseToUnderscoreWithLower)
  .UseMonitorCommand(cmd => { ECommon.Components.ObjectContainer.Resolve<ILoggerFactory>().Create("Startup").Info(cmd.CommandText); })
  .UseAutoSyncStructure(true) //automatically synchronize the entity structure to the database
  .Build(); //be sure to define as singleton mode

            services.AddFreeRepository(dataFilter =>
            {
                dataFilter.Apply<Model>("SoftDeleted", x => x.IsDeleted == false);
            });
            services.AddSingleton(fsql);

            services.AddScoped<ProductQueryService>();

        

            services.AddRazorPages();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            Console.WriteLine(JsonConvert.SerializeObject(new user()));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }


            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });



            var assemblies = new[] {
                Assembly.Load("Mall.CommandHandlers"),
                Assembly.Load("Mall.Commands"),
                Assembly.Load("Mall.Denormalizers.Dapper"),
                Assembly.Load("Mall.Domain"),
                Assembly.Load("Mall.Web")
            };

            ENodeConfiguration
               .Instance
               .InitializeMySqlEventStore(ConfigSettings.ENodeConnectionString)
                .InitializeMySqlLockService(ConfigSettings.ENodeConnectionString)
                .InitializeMySqlPublishedVersionStore(ConfigSettings.ENodeConnectionString)
                .InitializeBusinessAssemblies(assemblies)
                .StartEQueue()
                .Start();
        }

        public void ConfigureContainer(ContainerBuilder container)
        {
            //ConfigureENode
         
            Console.WriteLine(ConfigSettings.ForumConnectionString);
            Console.WriteLine(ConfigSettings.ENodeConnectionString);
            var assemblies = new[] { 
                Assembly.Load("Mall.CommandHandlers"), 
                Assembly.Load("Mall.Commands"),
                Assembly.Load("Mall.Denormalizers.Dapper"),
                Assembly.Load("Mall.Domain"),
                Assembly.Load("Mall.Web")
            };
            for (int i = 0; i < assemblies.Length; i++)
            {
                Console.WriteLine(assemblies[i].FullName);
            }
            var loggerFactory = new SerilogLoggerFactory()
                .AddFileLogger("ECommon", "logs\\ecommon")
                .AddFileLogger("EQueue", "logs\\equeue")
                .AddFileLogger("ENode", "logs\\enode");
            ECommon.Configurations.Configuration
               .Create()
               .UseAutofac(container)
               .RegisterCommonComponents()
               .UseSerilog(loggerFactory)
               .UseJsonNet()
               .CreateENode()
               .RegisterENodeComponents()
               .RegisterBusinessComponents(assemblies)
               .RegisterEQueueComponents()
               .UseEQueue()
               .UseMySqlEventStore()
               .UseMySqlLockService()
               .UseMySqlPublishedVersionStore();
                //.BuildContainer() 控制台使用。网站不使用此方法
             
        }
    }
}
