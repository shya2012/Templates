﻿using ECommon.Components;
using ECommon.Utilities;
using ENode.Commanding;
using Mall.Commands;
using Mall.Commands.PetShop;
using Mall.Entities.PetShop;
using Mall.QueryService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mall.Web.Pages
{
    public class ProductViewModel
    {
        public string ProductName { get; set; }
    }
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private  ICommandService _commandService;

        [BindProperty]
        public ProductViewModel Command { get; set; }

        public List<ProductInfo> Products { get; set; } = new List<ProductInfo>();
        public IndexModel(ILogger<IndexModel> logger, ICommandService commandService)
        {
            _logger = logger;
            _commandService = commandService;
        }

        public void OnGet()
        {
     
            //_commandService = ObjectContainer.Resolve<ICommandService>();


            //_commandService.SendAsync(new CreateProductCommand(ECommon.Utilities.ObjectId.GenerateNewStringId(),"测试"));
            // _commandService.SendAsync(new UpdateProductCommand("60103b19fe98dc4dd0405a29", "衡阳土鸡"));
            // var commandResult= await _commandService.ExecuteAsync(new CategoryCreateCommand("6010449afe98dc51a0e28d41",  "鞋帽", "描述"),CommandReturnType.EventHandled);
            //Console.WriteLine("插入结果:" + commandResult.Result);
            //await _commandService.ExecuteAsync(new CategoryUpdateCommand("6010449afe98dc51a0e28d41", "鞋帽33", "描述222"), CommandReturnType.EventHandled);
            var logger=  ObjectContainer.Resolve<ECommon.Logging.ILoggerFactory>().Create(typeof(IndexModel));
            logger.InfoFormat("ddddddddddd");
            _logger.LogInformation("原始："+DateTime.Now.ToString("yyyyMMdd HHmmss"));

            ProductQueryService productQueryService = ObjectContainer.Resolve<ProductQueryService>();
            Products = productQueryService.GetAllProducts();
        }

        public void OnPost()
        {
            _commandService.SendAsync(new ProductCreateCommand(ECommon.Utilities.ObjectId.GenerateNewStringId(), "1",Command.ProductName,"描述","图片"));
           RedirectToPage("Index");
        }

        public void OnDelete(string id)
        {
            _commandService.SendAsync(new ProductDeleteCommand(id));
        }
    }
}
