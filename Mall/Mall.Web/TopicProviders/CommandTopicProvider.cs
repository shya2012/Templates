﻿using ENode.Commanding;
using ENode.EQueue;

namespace Mall.Web.Providers
{
    public class CommandTopicProvider : AbstractTopicProvider<ICommand>
    {
        public override string GetTopic(ICommand command)
        {
            return Constants.CommandTopic;
        }

        //public CommandTopicProvider()
        //{
        //    RegisterTopic(Constants.NoteCommandTopic, typeof(CreateNoteCommand));
        //}
    }


    

}
