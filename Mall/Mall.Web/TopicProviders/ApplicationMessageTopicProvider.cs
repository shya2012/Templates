﻿using ENode.EQueue;
using ENode.Infrastructure;
using ENode.Messaging;

namespace Mall.Web.Providers
{
    public class ApplicationMessageTopicProvider : AbstractTopicProvider<IApplicationMessage>
    {
        public override string GetTopic(IApplicationMessage applicationMessage)
        {
            return Constants.ApplicationMessageTopic;
        }
    }
}
