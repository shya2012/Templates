﻿using ENode.EQueue;
using ENode.Eventing;

namespace Mall.Web.Providers
{
    public class EventTopicProvider : AbstractTopicProvider<IDomainEvent>
    {
        public override string GetTopic(IDomainEvent source)
        {
            return Constants.EventTopic;
        }

        //public EventTopicProvider()
        //{
        //    RegisterTopic(Constants.NoteEventTopic, typeof(NoteCreatedEvent));
        //}
    }
}
