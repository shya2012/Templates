﻿using System;

namespace Mall.Infrastructure.Data
{
    /// <summary>
    /// 数据库实体
    /// </summary>
    public class Model
    {
        /// <summary>
        /// 编号
        /// </summary>
        [FreeSql.DataAnnotations.Column(IsPrimary = true, StringLength = 24)]
        public string Id { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 版本号
        /// </summary>

        [FreeSql.DataAnnotations.Column(IsVersion = true)]
        public long Version { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        public long SortNo { get; set; }
        /// <summary>
        /// 删除标志
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
