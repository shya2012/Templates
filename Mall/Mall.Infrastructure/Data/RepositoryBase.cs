﻿using FreeSql;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Mall.Infrastructure.Data
{
    public class RepositoryBase<TEntity, TKey> : BaseRepository<TEntity, TKey> where TEntity : Model
    {
        public RepositoryBase() : base(
            ECommon.Components.ObjectContainer.Resolve<IFreeSql>(),null,
            // x => x.IsDeleted == false,
            null
            )
        {

        }
    }

    //public  class RepositoryBase<TEntity,TKey> where TEntity:class
    //{
    //    protected IFreeSql fsql;

    //    public RepositoryBase()
    //    {
    //        this.fsql = ECommon.Components.ObjectContainer.Resolve<IFreeSql>();
    //    }

    //    public int Delete(TKey id)
    //    {
    //       var repository= this.fsql.GetRepository<TEntity, TKey>();
    //       return repository.Delete(id);
    //    }
    //    public async Task<int> DeleteAsync(TKey id, CancellationToken cancellationToken = default)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return await repository.DeleteAsync(id,cancellationToken);
    //    }
    //    public  TEntity Find(TKey id)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return repository.Find(id);
    //    }
    //    public async Task<TEntity> FindAsync(TKey id, CancellationToken cancellationToken = default)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return await repository.FindAsync(id, cancellationToken);
    //    }
    //    public TEntity Get(TKey id)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return repository.Get(id);
    //    }
    //    public async Task<TEntity> GetAsync(TKey id, CancellationToken cancellationToken = default)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return await repository.GetAsync(id, cancellationToken);
    //    }

    //    public  int Delete(Expression<Func<TEntity, bool>> predicate)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return repository.Delete(predicate);
    //    }
    //    public  int Delete(IEnumerable<TEntity> entitys)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return repository.Delete(entitys);
    //    }
    //    public  int Delete(TEntity entity)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return repository.Delete(entity);
    //    }

    //    public async Task<int> DeleteAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return await repository.DeleteAsync(predicate, cancellationToken);
    //    }
    //    public async Task<int> DeleteAsync(TEntity entity, CancellationToken cancellationToken = default)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return await repository.DeleteAsync(entity, cancellationToken);
    //    }
    //    public async Task<int> DeleteAsync(IEnumerable<TEntity> entitys, CancellationToken cancellationToken = default)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return await repository.DeleteAsync(entitys, cancellationToken);
    //    }
 
 

    //    public  List<TEntity> Insert(IEnumerable<TEntity> entitys)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return repository.Insert(entitys);
    //    }
    //    public TEntity Insert(TEntity entity)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return repository.Insert(entity);
    //    }
     
    //    public async Task<TEntity> InsertAsync(TEntity entity, CancellationToken cancellationToken = default)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return await repository.InsertAsync(entity, cancellationToken);
    //    }

    //    public async Task<List<TEntity>> InsertAsync(IEnumerable<TEntity> entitys, CancellationToken cancellationToken = default)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return await repository.InsertAsync(entitys, cancellationToken);
    //    }
    //    public  TEntity InsertOrUpdate(TEntity entity)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return  repository.InsertOrUpdate(entity);
    //    }

    //    public async Task<TEntity> InsertOrUpdateAsync(TEntity entity, CancellationToken cancellationToken = default)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return await repository.InsertOrUpdateAsync(entity, cancellationToken);
    //    }
    //    public void SaveMany(TEntity entity, string propertyName)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        repository.SaveMany(entity, propertyName);
    //    }
    //    public async Task SaveManyAsync(TEntity entity, string propertyName, CancellationToken cancellationToken = default)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //       await repository.SaveManyAsync(entity, propertyName, cancellationToken);
    //    }
    //    public int Update(IEnumerable<TEntity> entitys)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return repository.Update(entitys);
    //    }
    //    public int Update(TEntity entity)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return repository.Update(entity);
    //    }
    //    public async Task<int> UpdateAsync(IEnumerable<TEntity> entitys, CancellationToken cancellationToken = default)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return await repository.UpdateAsync(entitys, cancellationToken);
    //    }
    //    public async Task<int> UpdateAsync(TEntity entity, CancellationToken cancellationToken = default)
    //    {
    //        var repository = this.fsql.GetRepository<TEntity, TKey>();
    //        return await repository.UpdateAsync(entity, cancellationToken);
    //    }
    //}
}
