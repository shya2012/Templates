﻿using System;

namespace Mall.Infrastructure
{
    /// <summary>
    /// 
    /// </summary>
    public  class FriendlyResult
    {
        protected FriendlyResult(bool ok, string message, int code)
        {
            Ok = ok;
            Message = message;
            Code = code;
        }

        public bool Ok { get;  }

        public string Message { get; }

        public int Code { get; }


        public static FriendlyResult Success(string message="操作成功。")
        {
            return new FriendlyResult(true, message, 0);
        }

        public static FriendlyResult Fail(string message,int code=0)
        {
            return new FriendlyResult(false, message, code);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class FriendlyDataResult<T>: FriendlyResult
    {
        public T Data { get; }

        private FriendlyDataResult(bool ok, string message, int code,T data):base(ok,message,code)
        {
            this.Data = data;
        }
    }
}
