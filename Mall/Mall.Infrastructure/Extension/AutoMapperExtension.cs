﻿using AutoMapper;
using ENode.Eventing;
using Mall.Infrastructure.Data;
using Mall.Infrastructure.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mall.Infrastructure
{
  public static  class AutoMapperExtension
    {
        public static TDestination ToEntity<TSource, TDestination>(this TSource source, TDestination destination) 
            where  TDestination:Model
            where TSource : DomainEvent<string>
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<TSource, TDestination>()
                .ForMember(item => item.Id, item => item.MapFrom(x => x.AggregateRootStringId)));
            var mapper = configuration.CreateMapper();
            return mapper.Map<TSource, TDestination>(source, destination);
        }

        public static TDestination Map<TSource, TDestination>(this TSource source, TDestination destination)
            where TDestination : Model
            where TSource : DomainEvent<string>
        {
            return AutoMapperUtils.Map<TSource, TDestination>(source, destination);
        }
    }
}
