﻿using AutoMapper;

namespace Mall.Infrastructure.Utils
{
    public class AutoMapperUtils
    {
        public static TDestination Map<TSource, TDestination>(TSource source, TDestination destination) 
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<TSource, TDestination>()) ;
            var mapper=configuration.CreateMapper();
            return mapper.Map<TSource, TDestination>(source, destination);
        }
    }
}
