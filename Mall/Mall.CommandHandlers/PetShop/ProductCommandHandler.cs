﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Commanding;
using Mall.Commands.PetShop;
using Mall.Domain.PetShop;

namespace Mall.CommandHandlers.PetShop
{
    /// <summary>
    /// Product
    /// </summary>
    public class ProductCommandHandler : 
        ICommandHandler<ProductCreateCommand>,
        ICommandHandler<ProductUpdateCommand>,
        ICommandHandler<ProductDeleteCommand>
    {

        public async Task HandleAsync(ICommandContext context, ProductCreateCommand command)
        {
           await context.AddAsync(new Product(command.AggregateRootId,command.CategoryId,command.Name,command.Descn,command.Image));
        }
        
        public  async Task HandleAsync(ICommandContext context, ProductUpdateCommand command)
        {
            Product Product= await context.GetAsync<Product>(command.AggregateRootId);
            if (Product == null)
            {
                context.SetResult("Product不存在！");
                return;
            }
            Product.Update(command.CategoryId,command.Name,command.Descn,command.Image);
        }
        
        public  async Task HandleAsync(ICommandContext context, ProductDeleteCommand command)
        {
            Product Product= await context.GetAsync<Product>(command.AggregateRootId);
            if (Product == null)
            {
                return;
            }
            Product.Delete();
        }
    }
}

