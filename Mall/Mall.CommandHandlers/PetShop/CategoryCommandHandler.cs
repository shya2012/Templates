﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Commanding;
using Mall.Commands.PetShop;
using Mall.Domain.PetShop;

namespace Mall.CommandHandlers.PetShop
{
    /// <summary>
    /// Category
    /// </summary>
    public class CategoryCommandHandler : 
        ICommandHandler<CategoryCreateCommand>,
        ICommandHandler<CategoryUpdateCommand>,
        ICommandHandler<CategoryDeleteCommand>
    {

        public async Task HandleAsync(ICommandContext context, CategoryCreateCommand command)
        {
           await context.AddAsync(new Category(command.AggregateRootId,command.Name,command.Descn));
        }
        
        public  async Task HandleAsync(ICommandContext context, CategoryUpdateCommand command)
        {
            Category Category= await context.GetAsync<Category>(command.AggregateRootId);
            if (Category == null)
            {
                context.SetResult("Category不存在！");
                return;
            }
            Category.Update(command.Name,command.Descn);
        }
        
        public  async Task HandleAsync(ICommandContext context, CategoryDeleteCommand command)
        {
            Category Category= await context.GetAsync<Category>(command.AggregateRootId);
            if (Category == null)
            {
                return;
            }
            Category.Delete();
        }
    }
}

