﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Commanding;
using Mall.Commands.PetShop;
using Mall.Domain.PetShop;

namespace Mall.CommandHandlers.PetShop
{
    /// <summary>
    /// Orders
    /// </summary>
    public class OrdersCommandHandler : 
        ICommandHandler<OrdersCreateCommand>,
        ICommandHandler<OrdersUpdateCommand>,
        ICommandHandler<OrdersDeleteCommand>
    {

        public async Task HandleAsync(ICommandContext context, OrdersCreateCommand command)
        {
           await context.AddAsync(new Orders(command.AggregateRootId,command.UserId,command.OrderDate,command.ShipAddr1,command.ShipAddr2,command.ShipCity,command.ShipState,command.ShipZip,command.ShipCountry,command.BillAddr1,command.BillAddr2,command.BillCity,command.BillState,command.BillZip,command.BillCountry,command.Courier,command.TotalPrice,command.BillToFirstName,command.BillToLastName,command.ShipToFirstName,command.ShipToLastName,command.AuthorizationNumber,command.Locale));
        }
        
        public  async Task HandleAsync(ICommandContext context, OrdersUpdateCommand command)
        {
            Orders Orders= await context.GetAsync<Orders>(command.AggregateRootId);
            if (Orders == null)
            {
                context.SetResult("Orders不存在！");
                return;
            }
            Orders.Update(command.UserId,command.OrderDate,command.ShipAddr1,command.ShipAddr2,command.ShipCity,command.ShipState,command.ShipZip,command.ShipCountry,command.BillAddr1,command.BillAddr2,command.BillCity,command.BillState,command.BillZip,command.BillCountry,command.Courier,command.TotalPrice,command.BillToFirstName,command.BillToLastName,command.ShipToFirstName,command.ShipToLastName,command.AuthorizationNumber,command.Locale);
        }
        
        public  async Task HandleAsync(ICommandContext context, OrdersDeleteCommand command)
        {
            Orders Orders= await context.GetAsync<Orders>(command.AggregateRootId);
            if (Orders == null)
            {
                return;
            }
            Orders.Delete();
        }
    }
}

