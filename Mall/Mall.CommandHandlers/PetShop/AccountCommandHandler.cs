﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Commanding;
using Mall.Commands.PetShop;
using Mall.Domain.PetShop;

namespace Mall.CommandHandlers.PetShop
{
    /// <summary>
    /// Account
    /// </summary>
    public class AccountCommandHandler : 
        ICommandHandler<AccountCreateCommand>,
        ICommandHandler<AccountUpdateCommand>,
        ICommandHandler<AccountDeleteCommand>
    {

        public async Task HandleAsync(ICommandContext context, AccountCreateCommand command)
        {
           await context.AddAsync(new Account(command.AggregateRootId,command.UniqueID,command.Email,command.FirstName,command.LastName,command.Address1,command.Address2,command.City,command.State,command.Zip,command.Country,command.Phone));
        }
        
        public  async Task HandleAsync(ICommandContext context, AccountUpdateCommand command)
        {
            Account Account= await context.GetAsync<Account>(command.AggregateRootId);
            if (Account == null)
            {
                context.SetResult("Account不存在！");
                return;
            }
            Account.Update(command.UniqueID,command.Email,command.FirstName,command.LastName,command.Address1,command.Address2,command.City,command.State,command.Zip,command.Country,command.Phone);
        }
        
        public  async Task HandleAsync(ICommandContext context, AccountDeleteCommand command)
        {
            Account Account= await context.GetAsync<Account>(command.AggregateRootId);
            if (Account == null)
            {
                return;
            }
            Account.Delete();
        }
    }
}

