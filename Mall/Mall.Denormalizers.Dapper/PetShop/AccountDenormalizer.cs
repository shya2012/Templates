﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Messaging;
using Mall.Infrastructure.Data;
using Mall.Entities.PetShop;
using Mall.Domain.PetShop;

namespace Mall.CommandHandlers.PetShop
{
    /// <summary>
    /// Account
    /// </summary>
    public class AccountDenormalizer : RepositoryBase<AccountInfo, string>,
        IMessageHandler<AccountCreatedEvent>,
        IMessageHandler<AccountUpdatedEvent>,
        IMessageHandler<AccountDeletedEvent>
    {

        public async Task HandleAsync( AccountCreatedEvent evnt)
        {
            AccountInfo Account = new AccountInfo();
            Account.Id=evnt.AggregateRootId;
            Account.UniqueID = evnt.UniqueID;
            Account.Email = evnt.Email;
            Account.FirstName = evnt.FirstName;
            Account.LastName = evnt.LastName;
            Account.Address1 = evnt.Address1;
            Account.Address2 = evnt.Address2;
            Account.City = evnt.City;
            Account.State = evnt.State;
            Account.Zip = evnt.Zip;
            Account.Country = evnt.Country;
            Account.Phone = evnt.Phone;
            await this.InsertAsync(Account);
        }
        
        public  async Task HandleAsync(AccountUpdatedEvent evnt)
        {
            AccountInfo Account = await this.GetAsync(evnt.AggregateRootId);
            Account.UniqueID = evnt.UniqueID;
            Account.Email = evnt.Email;
            Account.FirstName = evnt.FirstName;
            Account.LastName = evnt.LastName;
            Account.Address1 = evnt.Address1;
            Account.Address2 = evnt.Address2;
            Account.City = evnt.City;
            Account.State = evnt.State;
            Account.Zip = evnt.Zip;
            Account.Country = evnt.Country;
            Account.Phone = evnt.Phone;
            await this.UpdateAsync(Account);
        }
        
        public  async Task HandleAsync(AccountDeletedEvent evnt)
        {
             await this.DeleteAsync(evnt.AggregateRootId);
        }
    }
}

