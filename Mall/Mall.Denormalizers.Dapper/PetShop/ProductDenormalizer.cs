﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Messaging;
using Mall.Infrastructure.Data;
using Mall.Entities.PetShop;
using Mall.Domain.PetShop;
using FreeSql;
using System.Linq.Expressions;

namespace Mall.CommandHandlers.PetShop
{
  
    /// <summary>
    /// Product
    /// </summary>
    public class ProductDenormalizer : RepositoryBase<ProductInfo, string>,
        IMessageHandler<ProductCreatedEvent>,
        IMessageHandler<ProductUpdatedEvent>,
        IMessageHandler<ProductDeletedEvent>
    {
   

        public async Task HandleAsync( ProductCreatedEvent evnt)
        {
            ProductInfo Product = new ProductInfo();
            Product.Id=evnt.AggregateRootId;
            Product.CategoryId = evnt.CategoryId;
            Product.Name = evnt.Name;
            Product.Descn = evnt.Descn;
            Product.Image = evnt.Image;
            await this.InsertAsync(Product);
        }
        
        public  async Task HandleAsync(ProductUpdatedEvent evnt)
        {
            ProductInfo Product = await this.GetAsync(evnt.AggregateRootId);
            Product.CategoryId = evnt.CategoryId;
            Product.Name = evnt.Name;
            Product.Descn = evnt.Descn;
            Product.Image = evnt.Image;
            await this.UpdateAsync(Product);
        }
        
        public  async Task HandleAsync(ProductDeletedEvent evnt)
        {
             await this.DeleteAsync(evnt.AggregateRootId);
        }
    }
}

