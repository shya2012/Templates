﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Messaging;
using Mall.Infrastructure.Data;
using Mall.Entities.PetShop;
using Mall.Domain.PetShop;

namespace Mall.CommandHandlers.PetShop
{
    /// <summary>
    /// Category
    /// </summary>
    public class CategoryDenormalizer : RepositoryBase<CategoryInfo, string>,
        IMessageHandler<CategoryCreatedEvent>,
        IMessageHandler<CategoryUpdatedEvent>,
        IMessageHandler<CategoryDeletedEvent>
    {

        public async Task HandleAsync( CategoryCreatedEvent evnt)
        {
            CategoryInfo Category = new CategoryInfo();
            Category.Id=evnt.AggregateRootId;
            Category.Name = evnt.Name;
            Category.Descn = evnt.Descn;
            await this.InsertAsync(Category);
        }
        
        public  async Task HandleAsync(CategoryUpdatedEvent evnt)
        {
            CategoryInfo Category = await this.GetAsync(evnt.AggregateRootId);
            Category.Name = evnt.Name;
            Category.Descn = evnt.Descn;
            await this.InsertOrUpdateAsync(Category);
        }
        
        public  async Task HandleAsync(CategoryDeletedEvent evnt)
        {
             await this.DeleteAsync(evnt.AggregateRootId);
        }
    }
}

