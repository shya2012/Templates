﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Messaging;
using Mall.Infrastructure.Data;
using Mall.Entities.PetShop;
using Mall.Domain.PetShop;

namespace Mall.CommandHandlers.PetShop
{
    /// <summary>
    /// Orders
    /// </summary>
    public class OrdersDenormalizer : RepositoryBase<OrdersInfo, string>,
        IMessageHandler<OrdersCreatedEvent>,
        IMessageHandler<OrdersUpdatedEvent>,
        IMessageHandler<OrdersDeletedEvent>
    {

        public async Task HandleAsync( OrdersCreatedEvent evnt)
        {
            OrdersInfo Orders = new OrdersInfo();
            Orders.Id=evnt.AggregateRootId;
            Orders.UserId = evnt.UserId;
            Orders.OrderDate = evnt.OrderDate;
            Orders.ShipAddr1 = evnt.ShipAddr1;
            Orders.ShipAddr2 = evnt.ShipAddr2;
            Orders.ShipCity = evnt.ShipCity;
            Orders.ShipState = evnt.ShipState;
            Orders.ShipZip = evnt.ShipZip;
            Orders.ShipCountry = evnt.ShipCountry;
            Orders.BillAddr1 = evnt.BillAddr1;
            Orders.BillAddr2 = evnt.BillAddr2;
            Orders.BillCity = evnt.BillCity;
            Orders.BillState = evnt.BillState;
            Orders.BillZip = evnt.BillZip;
            Orders.BillCountry = evnt.BillCountry;
            Orders.Courier = evnt.Courier;
            Orders.TotalPrice = evnt.TotalPrice;
            Orders.BillToFirstName = evnt.BillToFirstName;
            Orders.BillToLastName = evnt.BillToLastName;
            Orders.ShipToFirstName = evnt.ShipToFirstName;
            Orders.ShipToLastName = evnt.ShipToLastName;
            Orders.AuthorizationNumber = evnt.AuthorizationNumber;
            Orders.Locale = evnt.Locale;
            await this.InsertAsync(Orders);
        }
        
        public  async Task HandleAsync(OrdersUpdatedEvent evnt)
        {
            OrdersInfo Orders = await this.GetAsync(evnt.AggregateRootId);
            Orders.UserId = evnt.UserId;
            Orders.OrderDate = evnt.OrderDate;
            Orders.ShipAddr1 = evnt.ShipAddr1;
            Orders.ShipAddr2 = evnt.ShipAddr2;
            Orders.ShipCity = evnt.ShipCity;
            Orders.ShipState = evnt.ShipState;
            Orders.ShipZip = evnt.ShipZip;
            Orders.ShipCountry = evnt.ShipCountry;
            Orders.BillAddr1 = evnt.BillAddr1;
            Orders.BillAddr2 = evnt.BillAddr2;
            Orders.BillCity = evnt.BillCity;
            Orders.BillState = evnt.BillState;
            Orders.BillZip = evnt.BillZip;
            Orders.BillCountry = evnt.BillCountry;
            Orders.Courier = evnt.Courier;
            Orders.TotalPrice = evnt.TotalPrice;
            Orders.BillToFirstName = evnt.BillToFirstName;
            Orders.BillToLastName = evnt.BillToLastName;
            Orders.ShipToFirstName = evnt.ShipToFirstName;
            Orders.ShipToLastName = evnt.ShipToLastName;
            Orders.AuthorizationNumber = evnt.AuthorizationNumber;
            Orders.Locale = evnt.Locale;
            await this.UpdateAsync(Orders);
        }
        
        public  async Task HandleAsync(OrdersDeletedEvent evnt)
        {
             await this.DeleteAsync(evnt.AggregateRootId);
        }
    }
}

