import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import login from '@/views/login'
import callback from '@/views/callback'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: login
    },
    {
      path: '/callback',
      name: 'callback',
      component: callback
    },
    {
      path: '/admin',
      name: 'admin',
      component: HelloWorld
    }
  ]
})
