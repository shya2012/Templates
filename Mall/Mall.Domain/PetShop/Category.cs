﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Domain;
using ENode.Eventing;

namespace Mall.Domain.PetShop
{
    /// <summary>
    /// Category
    /// </summary>
   public class Category:AggregateRoot<string>
    {
        /// <summary>
        /// Name
        /// </summary>
        private string name;
        /// <summary>
        /// Descn
        /// </summary>
        private string descn;
        
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="name">Name</param>
		/// <param name="descn">Descn</param>
		/// <returns></returns>
        public Category(string id,string name,string descn):base(id)
        {
            this.ApplyEvent(new CategoryCreatedEvent(name,descn));
        }
        /// <summary>
		/// 修改
		/// </summary>
		/// <param name="name">Name</param>
		/// <param name="descn">Descn</param>
		/// <returns></returns>
        public void Update(string name,string descn)
        {
            this.ApplyEvent(new CategoryUpdatedEvent(name,descn));
        }
        /// <summary>
		/// 删除
		/// </summary>
		/// <param name="name">Name</param>
		/// <param name="descn">Descn</param>
		/// <returns></returns>
        public void Delete()
        {
            this.ApplyEvent(new CategoryDeletedEvent());
        }
        
        private void Handle(CategoryUpdatedEvent evnt)
        {
            this.name = evnt.Name;
            this.descn = evnt.Descn;
        }
        
        private void Handle(CategoryCreatedEvent evnt)
        {
            this.name = evnt.Name;
            this.descn = evnt.Descn;
        }
        
        private void Handle(CategoryDeletedEvent evnt)
        {
        
        }
    }
    
    /// <summary>
    /// Category
    /// </summary>
   public class CategoryCreatedEvent:DomainEvent<string>
    {
        /// <summary>
        /// Name
        /// </summary>
        public  string Name{get;private set;}
        /// <summary>
        /// Descn
        /// </summary>
        public  string Descn{get;private set;}
        
        private CategoryCreatedEvent(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="name">Name</param>
		/// <param name="descn">Descn</param>
		/// <returns></returns>
        public CategoryCreatedEvent(string name,string descn)
        {
            this.Name = name;
            this.Descn = descn;
        }
    }
    
    /// <summary>
    /// Category
    /// </summary>
   public class CategoryUpdatedEvent:DomainEvent<string>
    {
        /// <summary>
        /// Name
        /// </summary>
        public  string Name{get;private set;}
        /// <summary>
        /// Descn
        /// </summary>
        public  string Descn{get;private set;}
        
        private CategoryUpdatedEvent(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="name">Name</param>
		/// <param name="descn">Descn</param>
		/// <returns></returns>
        public CategoryUpdatedEvent(string name,string descn)
        {
            this.Name = name;
            this.Descn = descn;
        }
    }
    
    /// <summary>
    /// Category
    /// </summary>
   public class CategoryDeletedEvent:DomainEvent<string>
    {

    }
}

