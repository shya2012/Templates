﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Domain;
using ENode.Eventing;

namespace Mall.Domain.PetShop
{
    /// <summary>
    /// Account
    /// </summary>
   public class Account:AggregateRoot<string>
    {
        /// <summary>
        /// UniqueID
        /// </summary>
        private int uniqueID;
        /// <summary>
        /// Email
        /// </summary>
        private string email;
        /// <summary>
        /// FirstName
        /// </summary>
        private string firstName;
        /// <summary>
        /// LastName
        /// </summary>
        private string lastName;
        /// <summary>
        /// Address1
        /// </summary>
        private string address1;
        /// <summary>
        /// Address2
        /// </summary>
        private string address2;
        /// <summary>
        /// City
        /// </summary>
        private string city;
        /// <summary>
        /// State
        /// </summary>
        private string state;
        /// <summary>
        /// Zip
        /// </summary>
        private string zip;
        /// <summary>
        /// Country
        /// </summary>
        private string country;
        /// <summary>
        /// Phone
        /// </summary>
        private string phone;
        
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="uniqueID">UniqueID</param>
		/// <param name="email">Email</param>
		/// <param name="firstName">FirstName</param>
		/// <param name="lastName">LastName</param>
		/// <param name="address1">Address1</param>
		/// <param name="address2">Address2</param>
		/// <param name="city">City</param>
		/// <param name="state">State</param>
		/// <param name="zip">Zip</param>
		/// <param name="country">Country</param>
		/// <param name="phone">Phone</param>
		/// <returns></returns>
        public Account(string id,int uniqueID,string email,string firstName,string lastName,string address1,string address2,string city,string state,string zip,string country,string phone):base(id)
        {
            this.ApplyEvent(new AccountCreatedEvent(uniqueID,email,firstName,lastName,address1,address2,city,state,zip,country,phone));
        }
        /// <summary>
		/// 修改
		/// </summary>
		/// <param name="uniqueID">UniqueID</param>
		/// <param name="email">Email</param>
		/// <param name="firstName">FirstName</param>
		/// <param name="lastName">LastName</param>
		/// <param name="address1">Address1</param>
		/// <param name="address2">Address2</param>
		/// <param name="city">City</param>
		/// <param name="state">State</param>
		/// <param name="zip">Zip</param>
		/// <param name="country">Country</param>
		/// <param name="phone">Phone</param>
		/// <returns></returns>
        public void Update(int uniqueID,string email,string firstName,string lastName,string address1,string address2,string city,string state,string zip,string country,string phone)
        {
            this.ApplyEvent(new AccountUpdatedEvent(uniqueID,email,firstName,lastName,address1,address2,city,state,zip,country,phone));
        }
        /// <summary>
		/// 删除
		/// </summary>
		/// <param name="uniqueID">UniqueID</param>
		/// <param name="email">Email</param>
		/// <param name="firstName">FirstName</param>
		/// <param name="lastName">LastName</param>
		/// <param name="address1">Address1</param>
		/// <param name="address2">Address2</param>
		/// <param name="city">City</param>
		/// <param name="state">State</param>
		/// <param name="zip">Zip</param>
		/// <param name="country">Country</param>
		/// <param name="phone">Phone</param>
		/// <returns></returns>
        public void Delete()
        {
            this.ApplyEvent(new AccountDeletedEvent());
        }
        
        private void Handle(AccountUpdatedEvent evnt)
        {
            this.uniqueID = evnt.UniqueID;
            this.email = evnt.Email;
            this.firstName = evnt.FirstName;
            this.lastName = evnt.LastName;
            this.address1 = evnt.Address1;
            this.address2 = evnt.Address2;
            this.city = evnt.City;
            this.state = evnt.State;
            this.zip = evnt.Zip;
            this.country = evnt.Country;
            this.phone = evnt.Phone;
        }
        
        private void Handle(AccountCreatedEvent evnt)
        {
            this.uniqueID = evnt.UniqueID;
            this.email = evnt.Email;
            this.firstName = evnt.FirstName;
            this.lastName = evnt.LastName;
            this.address1 = evnt.Address1;
            this.address2 = evnt.Address2;
            this.city = evnt.City;
            this.state = evnt.State;
            this.zip = evnt.Zip;
            this.country = evnt.Country;
            this.phone = evnt.Phone;
        }
        
        private void Handle(AccountDeletedEvent evnt)
        {
        
        }
    }
    
    /// <summary>
    /// Account
    /// </summary>
   public class AccountCreatedEvent:DomainEvent<string>
    {
        /// <summary>
        /// UniqueID
        /// </summary>
        public  int UniqueID{get;private set;}
        /// <summary>
        /// Email
        /// </summary>
        public  string Email{get;private set;}
        /// <summary>
        /// FirstName
        /// </summary>
        public  string FirstName{get;private set;}
        /// <summary>
        /// LastName
        /// </summary>
        public  string LastName{get;private set;}
        /// <summary>
        /// Address1
        /// </summary>
        public  string Address1{get;private set;}
        /// <summary>
        /// Address2
        /// </summary>
        public  string Address2{get;private set;}
        /// <summary>
        /// City
        /// </summary>
        public  string City{get;private set;}
        /// <summary>
        /// State
        /// </summary>
        public  string State{get;private set;}
        /// <summary>
        /// Zip
        /// </summary>
        public  string Zip{get;private set;}
        /// <summary>
        /// Country
        /// </summary>
        public  string Country{get;private set;}
        /// <summary>
        /// Phone
        /// </summary>
        public  string Phone{get;private set;}
        
        private AccountCreatedEvent(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="uniqueID">UniqueID</param>
		/// <param name="email">Email</param>
		/// <param name="firstName">FirstName</param>
		/// <param name="lastName">LastName</param>
		/// <param name="address1">Address1</param>
		/// <param name="address2">Address2</param>
		/// <param name="city">City</param>
		/// <param name="state">State</param>
		/// <param name="zip">Zip</param>
		/// <param name="country">Country</param>
		/// <param name="phone">Phone</param>
		/// <returns></returns>
        public AccountCreatedEvent(int uniqueID,string email,string firstName,string lastName,string address1,string address2,string city,string state,string zip,string country,string phone)
        {
            this.UniqueID = uniqueID;
            this.Email = email;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Address1 = address1;
            this.Address2 = address2;
            this.City = city;
            this.State = state;
            this.Zip = zip;
            this.Country = country;
            this.Phone = phone;
        }
    }
    
    /// <summary>
    /// Account
    /// </summary>
   public class AccountUpdatedEvent:DomainEvent<string>
    {
        /// <summary>
        /// UniqueID
        /// </summary>
        public  int UniqueID{get;private set;}
        /// <summary>
        /// Email
        /// </summary>
        public  string Email{get;private set;}
        /// <summary>
        /// FirstName
        /// </summary>
        public  string FirstName{get;private set;}
        /// <summary>
        /// LastName
        /// </summary>
        public  string LastName{get;private set;}
        /// <summary>
        /// Address1
        /// </summary>
        public  string Address1{get;private set;}
        /// <summary>
        /// Address2
        /// </summary>
        public  string Address2{get;private set;}
        /// <summary>
        /// City
        /// </summary>
        public  string City{get;private set;}
        /// <summary>
        /// State
        /// </summary>
        public  string State{get;private set;}
        /// <summary>
        /// Zip
        /// </summary>
        public  string Zip{get;private set;}
        /// <summary>
        /// Country
        /// </summary>
        public  string Country{get;private set;}
        /// <summary>
        /// Phone
        /// </summary>
        public  string Phone{get;private set;}
        
        private AccountUpdatedEvent(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="uniqueID">UniqueID</param>
		/// <param name="email">Email</param>
		/// <param name="firstName">FirstName</param>
		/// <param name="lastName">LastName</param>
		/// <param name="address1">Address1</param>
		/// <param name="address2">Address2</param>
		/// <param name="city">City</param>
		/// <param name="state">State</param>
		/// <param name="zip">Zip</param>
		/// <param name="country">Country</param>
		/// <param name="phone">Phone</param>
		/// <returns></returns>
        public AccountUpdatedEvent(int uniqueID,string email,string firstName,string lastName,string address1,string address2,string city,string state,string zip,string country,string phone)
        {
            this.UniqueID = uniqueID;
            this.Email = email;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Address1 = address1;
            this.Address2 = address2;
            this.City = city;
            this.State = state;
            this.Zip = zip;
            this.Country = country;
            this.Phone = phone;
        }
    }
    
    /// <summary>
    /// Account
    /// </summary>
   public class AccountDeletedEvent:DomainEvent<string>
    {

    }
}

