﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Domain;
using ENode.Eventing;

namespace Mall.Domain.PetShop
{
    /// <summary>
    /// Orders
    /// </summary>
   public class Orders:AggregateRoot<string>
    {
        /// <summary>
        /// UserId
        /// </summary>
        private string userId;
        /// <summary>
        /// OrderDate
        /// </summary>
        private DateTime orderDate;
        /// <summary>
        /// ShipAddr1
        /// </summary>
        private string shipAddr1;
        /// <summary>
        /// ShipAddr2
        /// </summary>
        private string shipAddr2;
        /// <summary>
        /// ShipCity
        /// </summary>
        private string shipCity;
        /// <summary>
        /// ShipState
        /// </summary>
        private string shipState;
        /// <summary>
        /// ShipZip
        /// </summary>
        private string shipZip;
        /// <summary>
        /// ShipCountry
        /// </summary>
        private string shipCountry;
        /// <summary>
        /// BillAddr1
        /// </summary>
        private string billAddr1;
        /// <summary>
        /// BillAddr2
        /// </summary>
        private string billAddr2;
        /// <summary>
        /// BillCity
        /// </summary>
        private string billCity;
        /// <summary>
        /// BillState
        /// </summary>
        private string billState;
        /// <summary>
        /// BillZip
        /// </summary>
        private string billZip;
        /// <summary>
        /// BillCountry
        /// </summary>
        private string billCountry;
        /// <summary>
        /// Courier
        /// </summary>
        private string courier;
        /// <summary>
        /// TotalPrice
        /// </summary>
        private string totalPrice;
        /// <summary>
        /// BillToFirstName
        /// </summary>
        private string billToFirstName;
        /// <summary>
        /// BillToLastName
        /// </summary>
        private string billToLastName;
        /// <summary>
        /// ShipToFirstName
        /// </summary>
        private string shipToFirstName;
        /// <summary>
        /// ShipToLastName
        /// </summary>
        private string shipToLastName;
        /// <summary>
        /// AuthorizationNumber
        /// </summary>
        private int authorizationNumber;
        /// <summary>
        /// Locale
        /// </summary>
        private string locale;
        
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="userId">UserId</param>
		/// <param name="orderDate">OrderDate</param>
		/// <param name="shipAddr1">ShipAddr1</param>
		/// <param name="shipAddr2">ShipAddr2</param>
		/// <param name="shipCity">ShipCity</param>
		/// <param name="shipState">ShipState</param>
		/// <param name="shipZip">ShipZip</param>
		/// <param name="shipCountry">ShipCountry</param>
		/// <param name="billAddr1">BillAddr1</param>
		/// <param name="billAddr2">BillAddr2</param>
		/// <param name="billCity">BillCity</param>
		/// <param name="billState">BillState</param>
		/// <param name="billZip">BillZip</param>
		/// <param name="billCountry">BillCountry</param>
		/// <param name="courier">Courier</param>
		/// <param name="totalPrice">TotalPrice</param>
		/// <param name="billToFirstName">BillToFirstName</param>
		/// <param name="billToLastName">BillToLastName</param>
		/// <param name="shipToFirstName">ShipToFirstName</param>
		/// <param name="shipToLastName">ShipToLastName</param>
		/// <param name="authorizationNumber">AuthorizationNumber</param>
		/// <param name="locale">Locale</param>
		/// <returns></returns>
        public Orders(string id,string userId,DateTime orderDate,string shipAddr1,string shipAddr2,string shipCity,string shipState,string shipZip,string shipCountry,string billAddr1,string billAddr2,string billCity,string billState,string billZip,string billCountry,string courier,string totalPrice,string billToFirstName,string billToLastName,string shipToFirstName,string shipToLastName,int authorizationNumber,string locale):base(id)
        {
            this.ApplyEvent(new OrdersCreatedEvent(userId,orderDate,shipAddr1,shipAddr2,shipCity,shipState,shipZip,shipCountry,billAddr1,billAddr2,billCity,billState,billZip,billCountry,courier,totalPrice,billToFirstName,billToLastName,shipToFirstName,shipToLastName,authorizationNumber,locale));
        }
        /// <summary>
		/// 修改
		/// </summary>
		/// <param name="userId">UserId</param>
		/// <param name="orderDate">OrderDate</param>
		/// <param name="shipAddr1">ShipAddr1</param>
		/// <param name="shipAddr2">ShipAddr2</param>
		/// <param name="shipCity">ShipCity</param>
		/// <param name="shipState">ShipState</param>
		/// <param name="shipZip">ShipZip</param>
		/// <param name="shipCountry">ShipCountry</param>
		/// <param name="billAddr1">BillAddr1</param>
		/// <param name="billAddr2">BillAddr2</param>
		/// <param name="billCity">BillCity</param>
		/// <param name="billState">BillState</param>
		/// <param name="billZip">BillZip</param>
		/// <param name="billCountry">BillCountry</param>
		/// <param name="courier">Courier</param>
		/// <param name="totalPrice">TotalPrice</param>
		/// <param name="billToFirstName">BillToFirstName</param>
		/// <param name="billToLastName">BillToLastName</param>
		/// <param name="shipToFirstName">ShipToFirstName</param>
		/// <param name="shipToLastName">ShipToLastName</param>
		/// <param name="authorizationNumber">AuthorizationNumber</param>
		/// <param name="locale">Locale</param>
		/// <returns></returns>
        public void Update(string userId,DateTime orderDate,string shipAddr1,string shipAddr2,string shipCity,string shipState,string shipZip,string shipCountry,string billAddr1,string billAddr2,string billCity,string billState,string billZip,string billCountry,string courier,string totalPrice,string billToFirstName,string billToLastName,string shipToFirstName,string shipToLastName,int authorizationNumber,string locale)
        {
            this.ApplyEvent(new OrdersUpdatedEvent(userId,orderDate,shipAddr1,shipAddr2,shipCity,shipState,shipZip,shipCountry,billAddr1,billAddr2,billCity,billState,billZip,billCountry,courier,totalPrice,billToFirstName,billToLastName,shipToFirstName,shipToLastName,authorizationNumber,locale));
        }
        /// <summary>
		/// 删除
		/// </summary>
		/// <param name="userId">UserId</param>
		/// <param name="orderDate">OrderDate</param>
		/// <param name="shipAddr1">ShipAddr1</param>
		/// <param name="shipAddr2">ShipAddr2</param>
		/// <param name="shipCity">ShipCity</param>
		/// <param name="shipState">ShipState</param>
		/// <param name="shipZip">ShipZip</param>
		/// <param name="shipCountry">ShipCountry</param>
		/// <param name="billAddr1">BillAddr1</param>
		/// <param name="billAddr2">BillAddr2</param>
		/// <param name="billCity">BillCity</param>
		/// <param name="billState">BillState</param>
		/// <param name="billZip">BillZip</param>
		/// <param name="billCountry">BillCountry</param>
		/// <param name="courier">Courier</param>
		/// <param name="totalPrice">TotalPrice</param>
		/// <param name="billToFirstName">BillToFirstName</param>
		/// <param name="billToLastName">BillToLastName</param>
		/// <param name="shipToFirstName">ShipToFirstName</param>
		/// <param name="shipToLastName">ShipToLastName</param>
		/// <param name="authorizationNumber">AuthorizationNumber</param>
		/// <param name="locale">Locale</param>
		/// <returns></returns>
        public void Delete()
        {
            this.ApplyEvent(new OrdersDeletedEvent());
        }
        
        private void Handle(OrdersUpdatedEvent evnt)
        {
            this.userId = evnt.UserId;
            this.orderDate = evnt.OrderDate;
            this.shipAddr1 = evnt.ShipAddr1;
            this.shipAddr2 = evnt.ShipAddr2;
            this.shipCity = evnt.ShipCity;
            this.shipState = evnt.ShipState;
            this.shipZip = evnt.ShipZip;
            this.shipCountry = evnt.ShipCountry;
            this.billAddr1 = evnt.BillAddr1;
            this.billAddr2 = evnt.BillAddr2;
            this.billCity = evnt.BillCity;
            this.billState = evnt.BillState;
            this.billZip = evnt.BillZip;
            this.billCountry = evnt.BillCountry;
            this.courier = evnt.Courier;
            this.totalPrice = evnt.TotalPrice;
            this.billToFirstName = evnt.BillToFirstName;
            this.billToLastName = evnt.BillToLastName;
            this.shipToFirstName = evnt.ShipToFirstName;
            this.shipToLastName = evnt.ShipToLastName;
            this.authorizationNumber = evnt.AuthorizationNumber;
            this.locale = evnt.Locale;
        }
        
        private void Handle(OrdersCreatedEvent evnt)
        {
            this.userId = evnt.UserId;
            this.orderDate = evnt.OrderDate;
            this.shipAddr1 = evnt.ShipAddr1;
            this.shipAddr2 = evnt.ShipAddr2;
            this.shipCity = evnt.ShipCity;
            this.shipState = evnt.ShipState;
            this.shipZip = evnt.ShipZip;
            this.shipCountry = evnt.ShipCountry;
            this.billAddr1 = evnt.BillAddr1;
            this.billAddr2 = evnt.BillAddr2;
            this.billCity = evnt.BillCity;
            this.billState = evnt.BillState;
            this.billZip = evnt.BillZip;
            this.billCountry = evnt.BillCountry;
            this.courier = evnt.Courier;
            this.totalPrice = evnt.TotalPrice;
            this.billToFirstName = evnt.BillToFirstName;
            this.billToLastName = evnt.BillToLastName;
            this.shipToFirstName = evnt.ShipToFirstName;
            this.shipToLastName = evnt.ShipToLastName;
            this.authorizationNumber = evnt.AuthorizationNumber;
            this.locale = evnt.Locale;
        }
        
        private void Handle(OrdersDeletedEvent evnt)
        {
        
        }
    }
    
    /// <summary>
    /// Orders
    /// </summary>
   public class OrdersCreatedEvent:DomainEvent<string>
    {
        /// <summary>
        /// UserId
        /// </summary>
        public  string UserId{get;private set;}
        /// <summary>
        /// OrderDate
        /// </summary>
        public  DateTime OrderDate{get;private set;}
        /// <summary>
        /// ShipAddr1
        /// </summary>
        public  string ShipAddr1{get;private set;}
        /// <summary>
        /// ShipAddr2
        /// </summary>
        public  string ShipAddr2{get;private set;}
        /// <summary>
        /// ShipCity
        /// </summary>
        public  string ShipCity{get;private set;}
        /// <summary>
        /// ShipState
        /// </summary>
        public  string ShipState{get;private set;}
        /// <summary>
        /// ShipZip
        /// </summary>
        public  string ShipZip{get;private set;}
        /// <summary>
        /// ShipCountry
        /// </summary>
        public  string ShipCountry{get;private set;}
        /// <summary>
        /// BillAddr1
        /// </summary>
        public  string BillAddr1{get;private set;}
        /// <summary>
        /// BillAddr2
        /// </summary>
        public  string BillAddr2{get;private set;}
        /// <summary>
        /// BillCity
        /// </summary>
        public  string BillCity{get;private set;}
        /// <summary>
        /// BillState
        /// </summary>
        public  string BillState{get;private set;}
        /// <summary>
        /// BillZip
        /// </summary>
        public  string BillZip{get;private set;}
        /// <summary>
        /// BillCountry
        /// </summary>
        public  string BillCountry{get;private set;}
        /// <summary>
        /// Courier
        /// </summary>
        public  string Courier{get;private set;}
        /// <summary>
        /// TotalPrice
        /// </summary>
        public  string TotalPrice{get;private set;}
        /// <summary>
        /// BillToFirstName
        /// </summary>
        public  string BillToFirstName{get;private set;}
        /// <summary>
        /// BillToLastName
        /// </summary>
        public  string BillToLastName{get;private set;}
        /// <summary>
        /// ShipToFirstName
        /// </summary>
        public  string ShipToFirstName{get;private set;}
        /// <summary>
        /// ShipToLastName
        /// </summary>
        public  string ShipToLastName{get;private set;}
        /// <summary>
        /// AuthorizationNumber
        /// </summary>
        public  int AuthorizationNumber{get;private set;}
        /// <summary>
        /// Locale
        /// </summary>
        public  string Locale{get;private set;}
        
        private OrdersCreatedEvent(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="userId">UserId</param>
		/// <param name="orderDate">OrderDate</param>
		/// <param name="shipAddr1">ShipAddr1</param>
		/// <param name="shipAddr2">ShipAddr2</param>
		/// <param name="shipCity">ShipCity</param>
		/// <param name="shipState">ShipState</param>
		/// <param name="shipZip">ShipZip</param>
		/// <param name="shipCountry">ShipCountry</param>
		/// <param name="billAddr1">BillAddr1</param>
		/// <param name="billAddr2">BillAddr2</param>
		/// <param name="billCity">BillCity</param>
		/// <param name="billState">BillState</param>
		/// <param name="billZip">BillZip</param>
		/// <param name="billCountry">BillCountry</param>
		/// <param name="courier">Courier</param>
		/// <param name="totalPrice">TotalPrice</param>
		/// <param name="billToFirstName">BillToFirstName</param>
		/// <param name="billToLastName">BillToLastName</param>
		/// <param name="shipToFirstName">ShipToFirstName</param>
		/// <param name="shipToLastName">ShipToLastName</param>
		/// <param name="authorizationNumber">AuthorizationNumber</param>
		/// <param name="locale">Locale</param>
		/// <returns></returns>
        public OrdersCreatedEvent(string userId,DateTime orderDate,string shipAddr1,string shipAddr2,string shipCity,string shipState,string shipZip,string shipCountry,string billAddr1,string billAddr2,string billCity,string billState,string billZip,string billCountry,string courier,string totalPrice,string billToFirstName,string billToLastName,string shipToFirstName,string shipToLastName,int authorizationNumber,string locale)
        {
            this.UserId = userId;
            this.OrderDate = orderDate;
            this.ShipAddr1 = shipAddr1;
            this.ShipAddr2 = shipAddr2;
            this.ShipCity = shipCity;
            this.ShipState = shipState;
            this.ShipZip = shipZip;
            this.ShipCountry = shipCountry;
            this.BillAddr1 = billAddr1;
            this.BillAddr2 = billAddr2;
            this.BillCity = billCity;
            this.BillState = billState;
            this.BillZip = billZip;
            this.BillCountry = billCountry;
            this.Courier = courier;
            this.TotalPrice = totalPrice;
            this.BillToFirstName = billToFirstName;
            this.BillToLastName = billToLastName;
            this.ShipToFirstName = shipToFirstName;
            this.ShipToLastName = shipToLastName;
            this.AuthorizationNumber = authorizationNumber;
            this.Locale = locale;
        }
    }
    
    /// <summary>
    /// Orders
    /// </summary>
   public class OrdersUpdatedEvent:DomainEvent<string>
    {
        /// <summary>
        /// UserId
        /// </summary>
        public  string UserId{get;private set;}
        /// <summary>
        /// OrderDate
        /// </summary>
        public  DateTime OrderDate{get;private set;}
        /// <summary>
        /// ShipAddr1
        /// </summary>
        public  string ShipAddr1{get;private set;}
        /// <summary>
        /// ShipAddr2
        /// </summary>
        public  string ShipAddr2{get;private set;}
        /// <summary>
        /// ShipCity
        /// </summary>
        public  string ShipCity{get;private set;}
        /// <summary>
        /// ShipState
        /// </summary>
        public  string ShipState{get;private set;}
        /// <summary>
        /// ShipZip
        /// </summary>
        public  string ShipZip{get;private set;}
        /// <summary>
        /// ShipCountry
        /// </summary>
        public  string ShipCountry{get;private set;}
        /// <summary>
        /// BillAddr1
        /// </summary>
        public  string BillAddr1{get;private set;}
        /// <summary>
        /// BillAddr2
        /// </summary>
        public  string BillAddr2{get;private set;}
        /// <summary>
        /// BillCity
        /// </summary>
        public  string BillCity{get;private set;}
        /// <summary>
        /// BillState
        /// </summary>
        public  string BillState{get;private set;}
        /// <summary>
        /// BillZip
        /// </summary>
        public  string BillZip{get;private set;}
        /// <summary>
        /// BillCountry
        /// </summary>
        public  string BillCountry{get;private set;}
        /// <summary>
        /// Courier
        /// </summary>
        public  string Courier{get;private set;}
        /// <summary>
        /// TotalPrice
        /// </summary>
        public  string TotalPrice{get;private set;}
        /// <summary>
        /// BillToFirstName
        /// </summary>
        public  string BillToFirstName{get;private set;}
        /// <summary>
        /// BillToLastName
        /// </summary>
        public  string BillToLastName{get;private set;}
        /// <summary>
        /// ShipToFirstName
        /// </summary>
        public  string ShipToFirstName{get;private set;}
        /// <summary>
        /// ShipToLastName
        /// </summary>
        public  string ShipToLastName{get;private set;}
        /// <summary>
        /// AuthorizationNumber
        /// </summary>
        public  int AuthorizationNumber{get;private set;}
        /// <summary>
        /// Locale
        /// </summary>
        public  string Locale{get;private set;}
        
        private OrdersUpdatedEvent(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="userId">UserId</param>
		/// <param name="orderDate">OrderDate</param>
		/// <param name="shipAddr1">ShipAddr1</param>
		/// <param name="shipAddr2">ShipAddr2</param>
		/// <param name="shipCity">ShipCity</param>
		/// <param name="shipState">ShipState</param>
		/// <param name="shipZip">ShipZip</param>
		/// <param name="shipCountry">ShipCountry</param>
		/// <param name="billAddr1">BillAddr1</param>
		/// <param name="billAddr2">BillAddr2</param>
		/// <param name="billCity">BillCity</param>
		/// <param name="billState">BillState</param>
		/// <param name="billZip">BillZip</param>
		/// <param name="billCountry">BillCountry</param>
		/// <param name="courier">Courier</param>
		/// <param name="totalPrice">TotalPrice</param>
		/// <param name="billToFirstName">BillToFirstName</param>
		/// <param name="billToLastName">BillToLastName</param>
		/// <param name="shipToFirstName">ShipToFirstName</param>
		/// <param name="shipToLastName">ShipToLastName</param>
		/// <param name="authorizationNumber">AuthorizationNumber</param>
		/// <param name="locale">Locale</param>
		/// <returns></returns>
        public OrdersUpdatedEvent(string userId,DateTime orderDate,string shipAddr1,string shipAddr2,string shipCity,string shipState,string shipZip,string shipCountry,string billAddr1,string billAddr2,string billCity,string billState,string billZip,string billCountry,string courier,string totalPrice,string billToFirstName,string billToLastName,string shipToFirstName,string shipToLastName,int authorizationNumber,string locale)
        {
            this.UserId = userId;
            this.OrderDate = orderDate;
            this.ShipAddr1 = shipAddr1;
            this.ShipAddr2 = shipAddr2;
            this.ShipCity = shipCity;
            this.ShipState = shipState;
            this.ShipZip = shipZip;
            this.ShipCountry = shipCountry;
            this.BillAddr1 = billAddr1;
            this.BillAddr2 = billAddr2;
            this.BillCity = billCity;
            this.BillState = billState;
            this.BillZip = billZip;
            this.BillCountry = billCountry;
            this.Courier = courier;
            this.TotalPrice = totalPrice;
            this.BillToFirstName = billToFirstName;
            this.BillToLastName = billToLastName;
            this.ShipToFirstName = shipToFirstName;
            this.ShipToLastName = shipToLastName;
            this.AuthorizationNumber = authorizationNumber;
            this.Locale = locale;
        }
    }
    
    /// <summary>
    /// Orders
    /// </summary>
   public class OrdersDeletedEvent:DomainEvent<string>
    {

    }
}

