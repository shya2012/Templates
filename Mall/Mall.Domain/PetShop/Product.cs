﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENode.Domain;
using ENode.Eventing;

namespace Mall.Domain.PetShop
{
    /// <summary>
    /// Product
    /// </summary>
   public class Product:AggregateRoot<string>
    {
        /// <summary>
        /// CategoryId
        /// </summary>
        private string categoryId;
        /// <summary>
        /// Name
        /// </summary>
        private string name;
        /// <summary>
        /// Descn
        /// </summary>
        private string descn;
        /// <summary>
        /// Image
        /// </summary>
        private string image;
        
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="categoryId">CategoryId</param>
		/// <param name="name">Name</param>
		/// <param name="descn">Descn</param>
		/// <param name="image">Image</param>
		/// <returns></returns>
        public Product(string id,string categoryId,string name,string descn,string image):base(id)
        {
            this.ApplyEvent(new ProductCreatedEvent(categoryId,name,descn,image));
        }
        /// <summary>
		/// 修改
		/// </summary>
		/// <param name="categoryId">CategoryId</param>
		/// <param name="name">Name</param>
		/// <param name="descn">Descn</param>
		/// <param name="image">Image</param>
		/// <returns></returns>
        public void Update(string categoryId,string name,string descn,string image)
        {
            this.ApplyEvent(new ProductUpdatedEvent(categoryId,name,descn,image));
        }
        /// <summary>
		/// 删除
		/// </summary>
		/// <param name="categoryId">CategoryId</param>
		/// <param name="name">Name</param>
		/// <param name="descn">Descn</param>
		/// <param name="image">Image</param>
		/// <returns></returns>
        public void Delete()
        {
            this.ApplyEvent(new ProductDeletedEvent());
        }
        
        private void Handle(ProductUpdatedEvent evnt)
        {
            this.categoryId = evnt.CategoryId;
            this.name = evnt.Name;
            this.descn = evnt.Descn;
            this.image = evnt.Image;
        }
        
        private void Handle(ProductCreatedEvent evnt)
        {
            this.categoryId = evnt.CategoryId;
            this.name = evnt.Name;
            this.descn = evnt.Descn;
            this.image = evnt.Image;
        }
        
        private void Handle(ProductDeletedEvent evnt)
        {
        
        }
    }
    
    /// <summary>
    /// Product
    /// </summary>
   public class ProductCreatedEvent:DomainEvent<string>
    {
        /// <summary>
        /// CategoryId
        /// </summary>
        public  string CategoryId{get;private set;}
        /// <summary>
        /// Name
        /// </summary>
        public  string Name{get;private set;}
        /// <summary>
        /// Descn
        /// </summary>
        public  string Descn{get;private set;}
        /// <summary>
        /// Image
        /// </summary>
        public  string Image{get;private set;}
        
        private ProductCreatedEvent(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="categoryId">CategoryId</param>
		/// <param name="name">Name</param>
		/// <param name="descn">Descn</param>
		/// <param name="image">Image</param>
		/// <returns></returns>
        public ProductCreatedEvent(string categoryId,string name,string descn,string image)
        {
            this.CategoryId = categoryId;
            this.Name = name;
            this.Descn = descn;
            this.Image = image;
        }
    }
    
    /// <summary>
    /// Product
    /// </summary>
   public class ProductUpdatedEvent:DomainEvent<string>
    {
        /// <summary>
        /// CategoryId
        /// </summary>
        public  string CategoryId{get;private set;}
        /// <summary>
        /// Name
        /// </summary>
        public  string Name{get;private set;}
        /// <summary>
        /// Descn
        /// </summary>
        public  string Descn{get;private set;}
        /// <summary>
        /// Image
        /// </summary>
        public  string Image{get;private set;}
        
        private ProductUpdatedEvent(){}
        
        /// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="categoryId">CategoryId</param>
		/// <param name="name">Name</param>
		/// <param name="descn">Descn</param>
		/// <param name="image">Image</param>
		/// <returns></returns>
        public ProductUpdatedEvent(string categoryId,string name,string descn,string image)
        {
            this.CategoryId = categoryId;
            this.Name = name;
            this.Descn = descn;
            this.Image = image;
        }
    }
    
    /// <summary>
    /// Product
    /// </summary>
   public class ProductDeletedEvent:DomainEvent<string>
    {

    }
}

