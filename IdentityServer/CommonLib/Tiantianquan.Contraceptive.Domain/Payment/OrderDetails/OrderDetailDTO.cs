﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 订单明细
    /// </summary>
   public class OrderDetailDTO
    {
        /// <summary>
        /// 订单明细编号
        /// </summary>
        public  Guid OrderDetailId{get;set;}
        /// <summary>
        /// 订单编号
        /// </summary>
        public  Guid? OrderId{get;set;}
        /// <summary>
        /// 商品编码
        /// </summary>
        public  Guid? ProductId{get;set;}
        /// <summary>
        /// SKU编号
        /// </summary>
        public  Guid? SkuId{get;set;}
        /// <summary>
        /// 商品名称
        /// </summary>
        public  String ProductName{get;set;}
        /// <summary>
        /// 商品图片
        /// </summary>
        public  String ProductImage{get;set;}
        /// <summary>
        /// Sku属性
        /// </summary>
        public  String SkuProperty{get;set;}
        /// <summary>
        /// Sku属性值
        /// </summary>
        public  String SkuPropertyValue{get;set;}
        /// <summary>
        /// 成交单价
        /// </summary>
        public  Int64? TradPrice{get;set;}
        /// <summary>
        /// 成交数量
        /// </summary>
        public  Int64? TradQuantity{get;set;}
        /// <summary>
        /// 总金额
        /// </summary>
        public  Int64? TotalAmount{get;set;}
        /// <summary>
        /// 创建时间
        /// </summary>
        public  DateTime? CreateTime{get;set;}
    }
}

