﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 订单
    /// </summary>
   public class OrderModelValidator:AbstractValidator<OrderInfo>
    {
        public OrderModelValidator()
        {


            RuleFor(x => x.OrderNo).NotEmpty().WithMessage("订单号不能为空。");
            RuleFor(x => x.OrderNo).MaximumLength(20).WithMessage("订单号最多20个字。");

            RuleFor(x => x.OrderTitle).MaximumLength(128).WithMessage("标题最多128个字。");

            RuleFor(x => x.OrderImage).MaximumLength(256).WithMessage("图片最多256个字。");





            RuleFor(x => x.OrderDetail).MaximumLength(-1).WithMessage("订单明细最多-1个字。");

            RuleFor(x => x.ExpressNo).MaximumLength(256).WithMessage("物流单号最多256个字。");


            RuleFor(x => x.FullName).MaximumLength(256).WithMessage("收货人最多256个字。");

            RuleFor(x => x.AddressCode).MaximumLength(256).WithMessage("收货地址1最多256个字。");

            RuleFor(x => x.AddressName).MaximumLength(256).WithMessage("收货地址2最多256个字。");

            RuleFor(x => x.AddressStreet).MaximumLength(256).WithMessage("收货街道地址最多256个字。");

            RuleFor(x => x.Phone).MaximumLength(256).WithMessage("手机号码最多256个字。");





        }
    }
}

