﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Domain;

namespace Tiantianquan.Contraceptive.Domain.Payment
{
    /// <summary>
    /// 评论
    /// </summary>
   public class CommentInfo:EntityBase
    {
        /// <summary>
        /// 评论编号
        /// </summary>
        public virtual Guid CommentId{get;set;}
        /// <summary>
        /// 订单编号
        /// </summary>
        public virtual Guid? OrderId{get;set;}
        /// <summary>
        /// 用户编号
        /// </summary>
        public virtual Guid? ResidentUserId{get;set;}
        /// <summary>
        /// 头像
        /// </summary>
        public virtual String HeadImageUrl{get;set;}
        /// <summary>
        /// 昵称
        /// </summary>
        public virtual String FullName{get;set;}
        /// <summary>
        /// 手机号
        /// </summary>
        public virtual String Phone{get;set;}
        /// <summary>
        /// 评论内容
        /// </summary>
        public virtual String CommentContent{get;set;}
        /// <summary>
        /// 评论图片
        /// </summary>
        public virtual String CommentImages{get;set;}
        /// <summary>
        /// 评论时间
        /// </summary>
        public virtual DateTime? CreateTime{get;set;}
        /// <summary>
        /// 来源IP
        /// </summary>
        public virtual String IPAddress{get;set;}
    }
}

