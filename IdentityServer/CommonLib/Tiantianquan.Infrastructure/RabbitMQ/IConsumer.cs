﻿namespace Tiantianquan.Infrastructure.RabbitMQ
{
    public interface IConsumer
    {
        void Start(object state);

        void Stop();
    }
}
