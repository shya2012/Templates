﻿using Microsoft.Extensions.Options;

namespace Tiantianquan.Infrastructure.RabbitMQ
{
    public class RabbitMQOption : IOptions<RabbitMQOption>
    {
        public RabbitMQOption Value => this;

        public string HostName { get; set; }

        public string Queue { get; set; }

        public string RoutingKey { get; set; }

        public string Exchange { get; set; }
    }
}
