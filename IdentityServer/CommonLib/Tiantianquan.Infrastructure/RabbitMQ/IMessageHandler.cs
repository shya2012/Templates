﻿namespace Tiantianquan.Infrastructure.RabbitMQ
{
    public interface IMessageHandler
    {
        void Handle(string lable, string body);
    }

    public interface IMessageHandler<TData>
    {

    }
}
