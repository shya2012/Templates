﻿using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System.Text;

namespace Tiantianquan.Infrastructure.RabbitMQ
{
    public class RabbitMQPublisher : IPublisher
    {
        public RabbitMQPublisher(RabbitMQOption option, ILogger<RabbitMQPublisher> logger)
        {
            Option = option;
            Logger = logger;
        }

        public RabbitMQOption Option { get; private set; }

        public ILogger<RabbitMQPublisher> Logger { get; private set; }
        public void Publish<T>(string routeKey,string label, T data)
        {
            var factory = new ConnectionFactory() { HostName = Option.HostName};
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: routeKey,
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    string message = label + "|" + data.ToJson(false, true);
                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: Option.Exchange,
                                         routingKey: routeKey,
                                         basicProperties: null,
                                         body: body);
                    Logger.LogInformation("发送消息：{0},{1}", label, message);
                    //Console.WriteLine(" [x] Sent {0}", message);
                }
            }
        }
    }
}
