﻿using Exceptionless.Extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.Utils
{
    public class DataBaseUtils
    {
        /// <summary>
        /// 判断唯一键冲突
        /// </summary>
        /// <param name="action"></param>
        /// <param name="message"></param>
        public static void CheckUniqueColumn(Action action, string message)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                if (ex.GetInnermostException() is SqlException)
                {
                    SqlException sqlException = ex.GetInnermostException() as SqlException;
                    if (sqlException.Number == 2627)
                    {
                        throw new FriendlyException(message);
                    }
                }
                throw ex;
            }
        }
    }
}
