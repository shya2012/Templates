﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;

namespace Tiantianquan.Infrastructure.Utils
{
    /// <summary>
    /// 作    用：汉语拼音类
    /// 创建用户：彭兴云 
    /// 创建时间：2015-04-30
    /// </summary>

    /// <summary>
    /// 汉字转拼音类
    /// </summary>
    public class PinYinUtils
    {


        /// <summary>   
        /// 汉字转化为拼音  
        /// </summary>   
        /// <param name="str">汉字</param>   
        /// <returns>全拼</returns>   
        public static string GetPinyin(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return string.Empty;
            }
            return NPinyin.Pinyin.GetPinyin(str, Encoding.UTF8);
        }

        /// <summary>   
        /// 汉字转化为拼音首字母  
        /// </summary>   
        /// <param name="str">汉字</param>   
        /// <returns>首字母</returns>   
        public static string GetFirstPinyin(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return string.Empty;
            }
            string result = string.Empty;
            foreach (char c in str)
            {
                try
                {

                    string t = NPinyin.Pinyin.GetPinyin(c, Encoding.UTF8);
                    if (!string.IsNullOrWhiteSpace(t))
                    {
                        result += char.ToUpperInvariant(t[0]);
                    }
                    
                }
                catch
                {
                    result += char.ToUpperInvariant(c) ;
                }
            }
            return result;
        }

        

    }
}
