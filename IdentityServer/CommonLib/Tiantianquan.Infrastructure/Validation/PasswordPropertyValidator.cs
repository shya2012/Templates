﻿using FluentValidation.Validators;
using System.Text.RegularExpressions;

namespace Tiantianquan.Infrastructure.Validation
{
    public class PasswordPropertyValidator: PropertyValidator
    {
        protected Regex Pattern = new Regex(@"^\w{6,30}$");
        public PasswordPropertyValidator() : base("密码长度6到30位，不包含空格。")
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (string.IsNullOrWhiteSpace(context.PropertyValue as string))
            {
                return true;
            }
            return Pattern.IsMatch(context.PropertyValue as string);
        }
    }
}
