﻿using FluentValidation.Validators;

namespace Tiantianquan.Infrastructure.Validation
{
    public class IdCardPropertyValidator : PropertyValidator
    {
        public IdCardPropertyValidator() : base("身份证号码不合法。")
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (string.IsNullOrWhiteSpace(context.PropertyValue as string))
            {
                return true;
            }

            return Utils.ValidatorUtils.CheckIDCard(context.PropertyValue as string);
        }
    }
}
