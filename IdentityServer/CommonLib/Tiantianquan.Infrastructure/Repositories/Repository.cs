using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;
using NHibernate;
using NHibernate.Linq;
using Tiantianquan.Infrastructure.Extensions;

namespace Tiantianquan.Infrastructure.Repositories
{
    public class Repository<T,TKey> : IRepository<T,TKey> where T : class {
        //private readonly ITransactionManager _transactionManager;

  

        public ILogger Logger { get; set; }

        protected virtual ISession Session {
            get { return null; }
        }

        public virtual IQueryable<T> Table {
            get { return Session.Query<T>().Cacheable(); }
        }

        #region IRepository<T> Members

        void IRepository<T,TKey>.Insert(T entity) {
            Insert(entity);
        }

        void IRepository<T, TKey>.Update(T entity) {
            Update(entity);
        }

        void IRepository<T, TKey>.Delete(T entity) {
            Delete(entity);
        }

 

        void IRepository<T, TKey>.Flush() {
            Flush();
        }

        T IRepository<T, TKey>.Get(TKey id) {
            return Get(id);
        }

        T IRepository<T, TKey>.Get(Expression<Func<T, bool>> predicate) {
            return Get(predicate);
        }

        IQueryable<T> IRepository<T, TKey>.Table {
            get { return Table; }
        }

        int IRepository<T, TKey>.Count(Expression<Func<T, bool>> predicate) {
            return Count(predicate);
        }

        IEnumerable<T> IRepository<T, TKey>.Fetch(Expression<Func<T, bool>> predicate) {
            return Fetch(predicate).ToReadOnlyCollection();
        }

        IEnumerable<T> IRepository<T, TKey>.Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order) {
            return Fetch(predicate, order).ToReadOnlyCollection();
        }

        IEnumerable<T> IRepository<T, TKey>.Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order, int skip,
                                            int count) {
            return Fetch(predicate, order, skip, count).ToReadOnlyCollection();
        }

        #endregion

        public virtual T Get(TKey id) {
            return Session.Get<T>(id);
        }

        public virtual T Get(Expression<Func<T, bool>> predicate) {
            return Fetch(predicate).SingleOrDefault();
        }

        public virtual void Insert(T entity) {
            Session.Save(entity);
        }

        public virtual void Update(T entity) {
            Session.Evict(entity);
            Session.Merge(entity);
        }

        public virtual void Delete(T entity) {
            Session.Delete(entity);            
        }

   
        public virtual void Flush() {
            Session.Flush();
        }

        public virtual int Count(Expression<Func<T, bool>> predicate) {
            return Fetch(predicate).Count();
        }

        public virtual IQueryable<T> Fetch(Expression<Func<T, bool>> predicate) {
            return Table.Where(predicate);
        }

        public virtual IQueryable<T> Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order) {
            var orderable = new Orderable<T>(Fetch(predicate));
            order(orderable);
            return orderable.Queryable;
        }

        public virtual IQueryable<T> Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order, int skip,
                                           int count) {
            return Fetch(predicate, order).Skip(skip).Take(count);
        }
    }
}