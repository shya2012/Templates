﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Runtime.Session;

namespace Microsoft.AspNetCore.Builder
{
  public static partial  class ConfigurationExtensions
    {
        public static IServiceCollection AddAbpSession(this IServiceCollection services)
        {
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IAbpSession, PasswordClaimsAbpSession>();
            return services;
        }
    }
}
