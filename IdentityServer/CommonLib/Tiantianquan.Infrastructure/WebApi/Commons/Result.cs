﻿using System.Threading.Tasks;
using Exceptionless.Json;
using Microsoft.AspNetCore.Mvc;

namespace Tiantianquan.Infrastructure.WebApi.Commons {
    /// <summary>
    /// 返回结果
    /// </summary>
    public class Result : JsonResult {
        /// <summary>
        /// 状态码
        /// </summary>
        private readonly bool _success;
        /// <summary>
        /// 消息
        /// </summary>
        private readonly string _message;
        /// <summary>
        /// 数据
        /// </summary>
        private readonly dynamic _data;

        /// <summary>
        /// 初始化返回结果
        /// </summary>
        /// <param name="success">状态码</param>
        /// <param name="message">消息</param>
        /// <param name="data">数据</param>
        public Result(bool success, string message, dynamic data = null ) : base( null ) {
            _success = success;
            _message = message;
            _data = data;
        }

        /// <summary>
        /// 执行结果
        /// </summary>
        public override Task ExecuteResultAsync( ActionContext context ) {
            this.Value = new 
            {
                success = _success,
                message = _message,
                data = _data
            };
            return base.ExecuteResultAsync( context );
        }
    }

    public class ResultModel
    {
        public ResultModel(bool success, string message, dynamic data)
        {
            Success = success;
            Message = message;
            Data = data;
        }

        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public dynamic Data { get; set; }
    }
}
