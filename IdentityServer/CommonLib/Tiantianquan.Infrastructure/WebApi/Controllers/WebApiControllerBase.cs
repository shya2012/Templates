﻿using Microsoft.AspNetCore.Mvc;
using Tiantianquan.Infrastructure.WebApi.Commons;
using Tiantianquan.Infrastructure.WebApi.Filters;


namespace Tiantianquan.Infrastructure.WebApi.Controllers
{
    /// <summary>
    /// WebApi控制器
    /// </summary>
    [ExceptionHandler]
    [ApiController]
    //  [AutoValidateAntiforgeryToken]
    public class WebApiControllerBase : ControllerBase
    {
        /// <summary>
        /// 返回成功消息
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="message">消息</param>
        protected virtual IActionResult Success(dynamic data = null, string message = null)
        {
            if (message == null)
                message = "操作成功。";
            return new Result(true, message, data);
        }

        /// <summary>
        /// 返回失败消息
        /// </summary>
        /// <param name="message">消息</param>
        protected IActionResult Fail(string message)
        {
            return new Result(false, message);
        }
    }
}