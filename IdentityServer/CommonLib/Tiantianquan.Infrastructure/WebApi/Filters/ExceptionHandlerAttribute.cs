﻿using Exceptionless.Extensions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Text;
using Tiantianquan.Infrastructure.Dependency;
using Tiantianquan.Infrastructure.Extensions;
using Tiantianquan.Infrastructure.WebApi.Commons;

namespace Tiantianquan.Infrastructure.WebApi.Filters
{
    /// <summary>
    /// 异常处理过滤器
    /// </summary>
    public class ExceptionHandlerAttribute : ExceptionFilterAttribute
    {
        private ILogger<ExceptionHandlerAttribute> Logger { get; }
        public ExceptionHandlerAttribute()
        {
            this.Logger = IocManager.GetService<ILoggerFactory>().CreateLogger<ExceptionHandlerAttribute>();
        }
        /// <summary>
        /// 异常处理
        /// </summary>
        public override void OnException(ExceptionContext context)
        {
            context.ExceptionHandled = true;
            context.HttpContext.Response.StatusCode = 200;

            StringBuilder builder = new StringBuilder();
            builder.AppendLine("请求地址：" + context.HttpContext.GetAbsouteUri());
            builder.AppendLine("请求方法：" + context.HttpContext.Request.Method);
            builder.AppendLine("请求参数：" + context.HttpContext.GetArguments());
            builder.AppendLine("错误消息：" + context.Exception.GetInnermostException().GetMessage());
            builder.AppendLine("堆栈信息：" + context.Exception.GetInnermostException().StackTrace);
            builder.AppendLine();

            this.Logger.Log(LogLevel.Error, builder.ToString());

            context.Result = new Result(false, context.Exception.GetInnermostException().GetMessage());
        }
    }
}