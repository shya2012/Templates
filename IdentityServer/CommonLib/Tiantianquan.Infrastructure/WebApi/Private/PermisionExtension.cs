﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Tiantianquan.Infrastructure;
using Tiantianquan.Infrastructure.Utils;
using Tiantianquan.Infrastructure.WebApi.Private;

namespace Microsoft.AspNetCore.Builder
{
    public static class PermisionExtension
    {
        public static void UseActionPermision(this IApplicationBuilder app, params Assembly[] assemblies)
        {
            List<ActionPermision> permisions = new List<ActionPermision>();
            List<Type> controllers = new List<Type>();
            foreach (Assembly assembly in assemblies)
            {
                controllers.AddRange(assembly.GetTypes().Where(x =>  typeof(Controller).IsAssignableFrom(x) && !x.IsAbstract).ToList());
            }

            foreach (var controllerType in controllers)
            {
                var methods = controllerType.GetMethods(BindingFlags.Public | BindingFlags.Instance);
                foreach (var method in methods)
                {
                    ActionPermision permision = new ActionPermision();
                    RouteAttribute routeAttribute = method.GetCustomAttribute<RouteAttribute>() ;
                    if (routeAttribute == null)
                        continue;
                    DescriptionAttribute descriptionAttribute = method.GetCustomAttribute<DescriptionAttribute>() ?? new DescriptionAttribute();
                    permision.ActionPath = routeAttribute.Template;
                    permision.ControllerName = controllerType.Name;
                    permision.ActionName = method.Name;
                    permision.GroupName = controllerType.Name;
                    permision.PermisionName = descriptionAttribute.Description ?? method.Name;
                    permisions.Add(permision);
                }
            }
            Console.WriteLine(permisions.ToJson(false, true));
        }
    }
}
