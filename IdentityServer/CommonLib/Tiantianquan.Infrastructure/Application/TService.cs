﻿
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Dependency;
using Tiantianquan.Infrastructure.Runtime.Session;

namespace Tiantianquan.Infrastructure.Application
{
 //   [Intercept(typeof(TransactionScopeInterceptor))]
    public abstract class ServiceBase
    {
        public virtual IAbpSession AbpSession { get; private set; }

        public virtual ILogger Logger { get; private set; }

     

        public ServiceBase()
        {
            this.AbpSession = IocManager.GetService<IAbpSession>();
            this.Logger = IocManager.GetService<ILoggerFactory>().CreateLogger<ServiceBase>();
           
        }
    }
}
