﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using Tiantianquan.Infrastructure.Extensions;
using Tiantianquan.Infrastructure.Services;
using Tiantianquan.Infrastructure.Services.Tencent;
using Tiantianquan.Infrastructure.Utils;
using Tiantianquan.Infrastructure.WebApi.Controllers;
using UEditorNetCore;

namespace Tiantianquan.Infrastructure
{
    public class CommonController:WebApiControllerBase
    {
        public CommonController(ILogger<CommonController> logger)
        {
            Logger = logger;
        }

        public ILogger<CommonController> Logger { get; private set; }

        [HttpGet]
        //[DontWrapper]
        [Route("~/api/Common/DynamicScripts")]
        public string GetDynamicScripts([FromServices]DynamicScriptsOption option)
        {
            var assemblies = option.Assemblies;
            List<Type> listType = new List<Type>();
            foreach (var assembly in assemblies)
            {
                try
                {
                    foreach (var type in assembly.GetTypes())
                    {
                        if (type == null)
                            continue;
                        if (type.BaseType == null)
                            continue;
                        if (type.BaseType.FullName != "System.Enum")
                            continue;
                        if (string.IsNullOrWhiteSpace(type.Namespace))
                            continue;

                        //if (!type.Namespace.StartsWith(this.GetType().Namespace.Split('.')[0]) && !type.Namespace.StartsWith("Tools"))
                        //{
                        //    continue;
                        //}
                        listType.Add(type);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.Message, ex);
                }
            }

            StringBuilder builder = new StringBuilder();
            builder.Append("var enums = enums || {};");
            foreach (var type in listType)
            {
                builder.AppendLine(EnumUtils.GetJavascript(type));
            }

            //var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            //new PlainTextFormatter()
            //response.Content = new StringContent(builder.ToString(), Encoding.UTF8);
           // response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-javascript");
            
            return builder.ToString();
        }

        [HttpPost]
        [Authorize]
        [Route("~/api/Common/Upload")]
        public IActionResult Upload([FromServices]IFileStore fileStore)
        {
            if (Request.HasFormContentType)
            {
                return Success(fileStore.SaveFileIfExist("File"));
            }
            return Fail("请上传文件！");
        }

        [AllowAnonymous]
        [Route("~/api/Common/captcha")]
        [HttpGet]
        public IActionResult Captcha([FromServices]CaptchaService captchaService)
        {
            return File(captchaService.CreateImage(), "image/png");
        }

        /// <summary>
        /// 
        /// </summary>
        [HttpGet, HttpPost]
        [Route("~/api/UEditor")]
        public void Do([FromServices]UEditorService editorService)
        {
            editorService.DoAction(HttpContext);
        }

        [HttpPost]
        [Authorize]
        [Route("~/api/Common/IdCardOcr")]
        public IActionResult IdCardOcr(int cardType,[FromServices]IFileStore fileStore,[FromServices]IdCardOcrService idCardOcrService)
        {
            if (Request.HasFormContentType)
            {
                string url= fileStore.SaveFileIfExist("IdCard");
                var data = idCardOcrService.Ocr(url, cardType);
                return Success(data);
            }
            return Fail("请上传文件！");
        }
    }
}
