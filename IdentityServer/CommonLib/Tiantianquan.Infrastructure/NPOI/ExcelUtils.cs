﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tiantianquan.Infrastructure.NPOI
{
    public class ExcelUtils
    {
        public static ICellStyle CreateStyle(IWorkbook workbook,
            string fontName = "宋体",
            int fontSize = 10,
            bool isBold = false,
            HorizontalAlignment horizontalAlignment = HorizontalAlignment.Left,
            VerticalAlignment verticalAlignment = VerticalAlignment.Center,
            bool wrapText = false,
            BorderStyle left = BorderStyle.Thin,
            BorderStyle right = BorderStyle.Thin,
            BorderStyle top = BorderStyle.Thin,
            BorderStyle bottom = BorderStyle.Thin
            )
        {
            IFont font = workbook.CreateFont();
            font.FontName = fontName;
            font.FontHeightInPoints = (short)fontSize;
            font.IsBold = isBold;
            ICellStyle style = workbook.CreateCellStyle();
            style.SetFont(font);
            style.Alignment = horizontalAlignment;
            style.VerticalAlignment = verticalAlignment;
            style.WrapText = wrapText;
            style.BorderLeft = left;
            style.BorderRight = right;
            style.BorderTop = top;
            style.BorderBottom = bottom;
            return style;
        }
    }
}
