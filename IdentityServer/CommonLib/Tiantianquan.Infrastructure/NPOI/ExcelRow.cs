﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tiantianquan.Infrastructure.NPOI
{
    public class ExcelRow
    {
        /// <summary>
        /// 
        /// </summary>
        public ISheet Sheet { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IRow Row { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RowIndex { get; private set; }

        public int ColumnIndex { get; set; }

        public const int HeightConstant = 20;

        public ExcelRow(ISheet sheet, int height = 20)
        {
            if (sheet.PhysicalNumberOfRows == 0)
            {
                this.RowIndex = 0;
            }
            else
            {
                this.RowIndex = sheet.PhysicalNumberOfRows;
            }
            this.ColumnIndex = 0;
            this.Sheet = sheet;
            this.Row = this.Sheet.CreateRow(this.RowIndex);
            this.Row.Height = (short)(HeightConstant * height);
        }


    }
}
