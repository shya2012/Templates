﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tiantianquan.Infrastructure
{
   public class TokenResponse
    {


        public TokenResponse(string tokenType, string accessToken, Dictionary<string, object> customResponse) 
        {
            this.TokenType = tokenType;
            this.AccessToken = accessToken;
            this.UserData = customResponse;
        }

        public string AccessToken { get; set; }

        public string TokenType { get; set; }

        public Dictionary<string, object> UserData { get; set; }
    }
}
