﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.DrawingCore;
using System.IO;
using System.Text;

namespace Tiantianquan.Infrastructure.Services
{
    public class QRCodeService
    {
        

        public byte[] QRCode(string text, int width, int height)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(text, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            MemoryStream ms = new MemoryStream();
            qrCodeImage.Save(ms, System.DrawingCore.Imaging.ImageFormat.Png);
            ms.Flush();
            return ms.GetBuffer();
        }
    }
}
