﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Tiantianquan.Infrastructure.Services.Tencent
{
    public class IdCardOcrService
    {
        public IdCardOcrService(IConfiguration configuration, IMemoryCache cache)
        {
            Configuration = configuration;
            Cache = cache;
            this.appId = configuration.GetSection("TencentYun").GetValue<string>("AppId");
            this.secretId = configuration.GetSection("TencentYun").GetValue<string>("SecretId");
            this.secretKey = configuration.GetSection("TencentYun").GetValue<string>("SecretKey");
        }

        protected IConfiguration Configuration { get; private set; }

        protected IMemoryCache Cache { get; private set; }

        private string appId;
        private string secretId;
        private string secretKey;

        private string GetSign()
        {
            string key = appId + secretId + secretKey;
            string sign = string.Empty;
            if(this.Cache.TryGetValue<string>(key,out sign))
            {
                return sign;
            }
            sign = TencentYunHelper.GetManyTimesSign(appId, secretId, secretKey);
            this.Cache.Set<string>(key, sign, TimeSpan.FromHours(2));
            return sign;
        }

        public ResultList Ocr(string url,int cardType)
        {
            string sign = this.GetSign();

            HttpClient client = HttpClientFactory.Create();
            client.BaseAddress = new Uri("http://recognition.image.myqcloud.com");
            client.DefaultRequestHeaders.Add("host", "recognition.image.myqcloud.com");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("authorization", sign);

            string resultStr = client.PostAsync("/ocr/idcard",
                 new StringContent(new OcrIdCardUrlParameter(appId, 0, url).ToString(),
                 Encoding.UTF8, "application/json")).Result.Content.ReadAsStringAsync().Result;

            
            OcrResult okResult = JsonConvert.DeserializeObject<OcrResult>(resultStr);
            if (okResult.result_list != null)
            {
                ResultList resultList =okResult.result_list.FirstOrDefault(x => x.code == 0);
                if (resultList != null)
                {
                    return resultList;
                }
                resultList = okResult.result_list.FirstOrDefault();
                throw new FriendlyException(resultList.message);
            }
            ResultList errResult = JsonConvert.DeserializeObject<ResultList>(resultStr);
            throw new FriendlyException(errResult.message);
        }
    }

    public class Data
    {
        public string name { get; set; }
        public string sex { get; set; }
        public string nation { get; set; }
        public string birth { get; set; }
        public string address { get; set; }
        public string id { get; set; }
        public IList<int> name_confidence_all { get; set; }
        public IList<int> sex_confidence_all { get; set; }
        public IList<int> nation_confidence_all { get; set; }
        public IList<int> birth_confidence_all { get; set; }
        public IList<int> address_confidence_all { get; set; }
        public IList<int> id_confidence_all { get; set; }
    }

    public class ResultList
    {
        public int code { get; set; }
        public string message { get; set; }
        public string url { get; set; }
        public Data data { get; set; }
    }

    public class OcrResult
    {
        public IList<ResultList> result_list { get; set; }
    }
    public class OcrIdCardUrlParameter
    {
        public OcrIdCardUrlParameter(string appId, int cardType, params string[] urlList)
        {
            AppId = appId;
            CardType = cardType;
            UrlList = new List<string>(urlList);
        }

        [JsonProperty("appid")]
        public string AppId { get; set; }

        [JsonProperty("card_type")]
        public int CardType { get; set; }

        [JsonProperty("url_list")]
        public List<string> UrlList { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
