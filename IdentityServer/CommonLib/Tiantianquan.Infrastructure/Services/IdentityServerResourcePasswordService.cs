﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.Services
{
   public class IdentityServerResourcePasswordService
    {
        private IConfiguration configuration;
        private IMemoryCache memoryCache;
        private HttpClient httpClient;

        public IdentityServerResourcePasswordService(IConfiguration configuration, IMemoryCache memoryCache)
        {
            this.configuration = configuration;
            this.httpClient = HttpClientFactory.Create();
            this.httpClient.BaseAddress = new Uri(configuration.GetValue<string>("Authority"));
            this.memoryCache = memoryCache;
        }

        public async Task<string> GetTokenAsync(string username,string password)
        {
            string json = string.Empty;
            if (memoryCache.TryGetValue<string>(username, out json))
            {
                return json;
            }
            List<KeyValuePair<string, string>> nameValueCollection = new List<KeyValuePair<string, string>>();
            nameValueCollection.Add(new KeyValuePair<string, string>("client_id", configuration.GetValue<string>("ClientId")));
            nameValueCollection.Add(new KeyValuePair<string, string>("client_secret", configuration.GetValue<string>("ClientSecret")));
            nameValueCollection.Add(new KeyValuePair<string, string>("username", username));
            nameValueCollection.Add(new KeyValuePair<string, string>("password", password));
            nameValueCollection.Add(new KeyValuePair<string, string>("grant_type", "password"));
            nameValueCollection.Add(new KeyValuePair<string, string>("scope", "openid profile api offline_access"));
            var result = await httpClient.PostAsync("/connect/token", new FormUrlEncodedContent(nameValueCollection));
            json = await result.Content.ReadAsStringAsync();
            if (!result.IsSuccessStatusCode)
            {
                throw new FriendlyException(json);
            }
            memoryCache.Set<string>(username, json,  TimeSpan.FromSeconds(3500));
            return json;
        }
    }
}
