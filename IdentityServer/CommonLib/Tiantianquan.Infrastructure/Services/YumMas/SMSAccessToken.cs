﻿using Newtonsoft.Json;
using WebApiClient.DataAnnotations;

namespace Tiantianquan.Infrastructure.Services
{
    public class SMSAccessToken
    {
        [AliasAs("access_token")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [AliasAs("mas_user_id")]
        [JsonProperty("mas_user_id")]
        public string MasUserId { get; set; }

        [AliasAs("access_token_expire_seconds")]
        [JsonProperty("access_token_expire_seconds")]
        public int AccessTokenExpireSeconds { get; set; }

        [AliasAs("status")]
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
