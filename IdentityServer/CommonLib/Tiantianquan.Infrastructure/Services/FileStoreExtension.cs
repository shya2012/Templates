﻿using Microsoft.Extensions.DependencyInjection;
using Tiantianquan.Infrastructure.Services;
using Tiantianquan.Infrastructure.Services.Tencent;

namespace Microsoft.AspNetCore.Builder
{
    public static partial class FileStoreExtension
    {
        public static IServiceCollection AddDefaultFileStore(this IServiceCollection services)
        {
            services.AddSingleton<IFileStore, DefaultFileStore>();
            return services;
        }
    }

    public static partial class SnowflakeExtension
    {
        public static IServiceCollection AddSnowflake(this IServiceCollection services)
        {
            // services.AddSingleton<ISnowflakeService, SnowflakeService>();
            return services;
        }
    }

    public static partial class CaptchaExtension
    {
        public static IServiceCollection AddCaptchaService(this IServiceCollection services)
        {
            services.AddSingleton<CaptchaService, CaptchaService>();
            return services;
        }
    }

    public static partial class ChinaMobileSMSExtension
    {
        public static IServiceCollection AddChinaMobileSMSService(this IServiceCollection services)
        {
            services.AddSingleton<ChinaMobileSmsService, ChinaMobileSmsService>();
            return services;
        }
    }

    public static partial class QRCodeExtension
    {
        public static IServiceCollection AddQRCodeService(this IServiceCollection services)
        {
            services.AddSingleton<QRCodeService, QRCodeService>();
            return services;
        }
    }

    public static partial class IdentityServerResourcePasswordExtension
    {
        public static IServiceCollection AddIdentityServerResourcePasswordService(this IServiceCollection services)
        {
            services.AddSingleton<IdentityServerResourcePasswordService, IdentityServerResourcePasswordService>();

            return services;
        }
    }

    public static partial class OcrExtension
    {
        public static IServiceCollection AddIdCardOcr(this IServiceCollection services)
        {
            services.AddSingleton<IdCardOcrService, IdCardOcrService>();

            return services;
        }
    }
}