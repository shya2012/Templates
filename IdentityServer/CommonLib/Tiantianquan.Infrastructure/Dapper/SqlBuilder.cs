﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tiantianquan.Infrastructure.Dapper
{
    public class SqlBuilder
    {
        private StringBuilder buffer = new StringBuilder();

        private string _sql = string.Empty;

        private string _groupby = string.Empty;

        private string _orderby = string.Empty;

        private string _limit = string.Empty;

        public SqlBuilder(string sql)
        {
            this._sql = sql;
        }

        public void And(string condition)
        {
            buffer.Append(" AND ").Append(condition);
        }

        public void Or(string condition)
        {
            buffer.Append("  OR ").Append(condition);
        }

        public override string ToString()
        {
            if (buffer.Length == 0)
            {
                return this._sql
                    + this._groupby
                    + this._orderby
                    + this._limit;
            }
            return this._sql + " WHERE " + buffer.ToString().Substring(4)
                + this._groupby
                + this._orderby
                + this._limit;
        }

        public void GroupBy(string columns)
        {
            this._groupby = " GROUP BY " + columns;
        }

        public static string GetSqlPart<T>(List<T> ids, DynamicParameters parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException("parameters");
            }
            if (ids == null)
            {
                throw new ArgumentNullException("ids");
            }
            if (ids.Count() == 0)
            {
                throw new ArgumentException("集合数据不能为空。");
            }
            StringBuilder sb = new StringBuilder();
            foreach (var id in ids)
            {
                string paramName = "@p" + Conv.ToString(parameters.ParameterNames.Count() + 1);
                if (sb.Length > 0)
                {
                    sb.Append(",");
                }
                sb.Append(paramName);
                parameters.Add(paramName, id);
            }
            return sb.ToString();
        }

        public void OrderBy(string orderBy)
        {
            this._orderby = " ORDER BY  " + orderBy;
        }

        public void Limit(int limit)
        {
            this._limit = " LIMIT " + limit;
        }

        public void Limit(int limit, int offset)
        {
            this._limit = string.Format("LIMIT {0},{1}", limit, offset);
        }
    }
}