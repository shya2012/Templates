﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tiantianquan.Infrastructure.WebApi.Controllers;
using Tiantianquan.Infrastructure.Utils;
using Tiantianquan.Infrastructure.Utilities;
using Tiantianquan.Infrastructure.UI;
using Tiantianquan.Contraceptive.Domain.Payment;
using Tiantianquan.Contraceptive.Application.Payment;

namespace Tiantianquan.Contraceptive.WebApi.Payment
{
    /// <summary>
    /// 订单
    /// </summary>
   [Authorize]
   public class OrderController:WebApiControllerBase
    {
        protected OrderService OrderService { get; }

        public OrderController(OrderService orderService)
        {
            this.OrderService = orderService;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchParams"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/Order/GetOrderByPage")]
        public PageResult<OrderInfo> GetOrderByPage([FromBody]OrderPageParam pageParam)
        {
            return this.OrderService.GetOrderByPage(pageParam);
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("~/api/Payment/Order/GetOrderById")]
        public OrderInfo GetOrderById(Guid orderId)
        {
            return this.OrderService.GetOrderById(orderId);
        }
        /// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/Order/Put")]
        public void Put([FromBody]OrderModel model)
        {
            this.OrderService.Insert(model);
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/Order/Post")]
        public void Post(Guid orderId, [FromBody]OrderModel model)
        {
            this.OrderService.Update(orderId, model);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/Order/Delete")]
        public void Delete(Guid orderId)
        {
            this.OrderService.Delete(orderId);
        }
    }
}

