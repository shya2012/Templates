﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tiantianquan.Infrastructure.WebApi.Controllers;
using Tiantianquan.Infrastructure.Utils;
using Tiantianquan.Infrastructure.Utilities;
using Tiantianquan.Infrastructure.UI;
using Tiantianquan.Contraceptive.Domain.Payment;
using Tiantianquan.Contraceptive.Application.Payment;

namespace Tiantianquan.Contraceptive.WebApi.Payment
{
    /// <summary>
    /// 评论
    /// </summary>
   [Authorize]
   public class CommentController:WebApiControllerBase
    {
        protected CommentService CommentService { get; }

        public CommentController(CommentService commentService)
        {
            this.CommentService = commentService;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchParams"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/Comment/GetCommentByPage")]
        public PageResult<CommentInfo> GetCommentByPage([FromBody]CommentPageParam pageParam)
        {
            return this.CommentService.GetCommentByPage(pageParam);
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="commentId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("~/api/Payment/Comment/GetCommentById")]
        public CommentInfo GetCommentById(Guid commentId)
        {
            return this.CommentService.GetCommentById(commentId);
        }
        /// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/Comment/Put")]
        public void Put([FromBody]CommentModel model)
        {
            this.CommentService.Insert(model);
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="commentId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/Comment/Post")]
        public void Post(Guid commentId, [FromBody]CommentModel model)
        {
            this.CommentService.Update(commentId, model);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="commentId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/Comment/Delete")]
        public void Delete(Guid commentId)
        {
            this.CommentService.Delete(commentId);
        }
    }
}

