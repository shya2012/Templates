﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tiantianquan.Infrastructure.WebApi.Controllers;
using Tiantianquan.Infrastructure.Utils;
using Tiantianquan.Infrastructure.Utilities;
using Tiantianquan.Infrastructure.UI;
using Tiantianquan.Contraceptive.Domain.Payment;
using Tiantianquan.Contraceptive.Application.Payment;

namespace Tiantianquan.Contraceptive.WebApi.Payment
{
    /// <summary>
    /// 订单明细
    /// </summary>
   [Authorize]
   public class OrderDetailController:WebApiControllerBase
    {
        protected OrderDetailService OrderDetailService { get; }

        public OrderDetailController(OrderDetailService orderDetailService)
        {
            this.OrderDetailService = orderDetailService;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchParams"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/OrderDetail/GetOrderDetailByPage")]
        public PageResult<OrderDetailInfo> GetOrderDetailByPage([FromBody]OrderDetailPageParam pageParam)
        {
            return this.OrderDetailService.GetOrderDetailByPage(pageParam);
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="orderDetailId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("~/api/Payment/OrderDetail/GetOrderDetailById")]
        public OrderDetailInfo GetOrderDetailById(Guid orderDetailId)
        {
            return this.OrderDetailService.GetOrderDetailById(orderDetailId);
        }
        /// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/OrderDetail/Put")]
        public void Put([FromBody]OrderDetailModel model)
        {
            this.OrderDetailService.Insert(model);
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="orderDetailId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/OrderDetail/Post")]
        public void Post(Guid orderDetailId, [FromBody]OrderDetailModel model)
        {
            this.OrderDetailService.Update(orderDetailId, model);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="orderDetailId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("~/api/Payment/OrderDetail/Delete")]
        public void Delete(Guid orderDetailId)
        {
            this.OrderDetailService.Delete(orderDetailId);
        }
    }
}

