﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Tiantianquan.Contraceptive.Application.Payment;
using Tiantianquan.Contraceptive.Domain.Payment;
using Tiantianquan.Contraceptive.Repository.Payment;

namespace Microsoft.Extensions.DependencyInjection
{
    public static partial class PaymentExtension
    {
        public static IServiceCollection RegisterPaymentModule(this IServiceCollection services)
        {
           //评论
           services.AddScoped<CommentService,CommentService>();
           services.AddScoped<CommentRepository,CommentRepositoryImpl>();
           //订单
           services.AddScoped<OrderService,OrderService>();
           services.AddScoped<OrderRepository,OrderRepositoryImpl>();
           //订单明细
           services.AddScoped<OrderDetailService,OrderDetailService>();
           services.AddScoped<OrderDetailRepository,OrderDetailRepositoryImpl>();
           //收货地址
           services.AddScoped<ShippingAddressService,ShippingAddressService>();
           services.AddScoped<ShippingAddressRepository,ShippingAddressRepositoryImpl>();
            return services;
        }
    }
}


