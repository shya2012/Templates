﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure;
using Tiantianquan.Infrastructure.AutoMapper;
using Tiantianquan.Infrastructure.Application;
using Tiantianquan.Infrastructure.Utils;
using Tiantianquan.Infrastructure.Utilities;
using Tiantianquan.Infrastructure.UI;
using Tiantianquan.Contraceptive.Domain.Payment;

namespace Tiantianquan.Contraceptive.Application.Payment
{
    /// <summary>
    /// 订单
    /// </summary>
   public class OrderService:ServiceBase
    {
        public OrderService(OrderRepository orderRepository)
        {
            OrderRepository = orderRepository;
        }

        protected OrderRepository OrderRepository { get; private set; }
        
        public void Insert(OrderModel model)
        {
            OrderInfo order = new OrderInfo();
            order.OrderId = GuidUtils.SecureSequentialGuid();       
            order.ResidentUserId = model.ResidentUserId;
            order.OrderNo = model.OrderNo;
            order.OrderTitle = model.OrderTitle;
            order.OrderImage = model.OrderImage;
            order.ProductAmount = model.ProductAmount;
            order.ExpressAmount = model.ExpressAmount;
            order.TotalAmount = model.TotalAmount;
            order.PayStatus = model.PayStatus;
            order.OrderDetail = model.OrderDetail;
            order.ExpressNo = model.ExpressNo;
            order.LastExpressQueryTime = model.LastExpressQueryTime;
            order.FullName = model.FullName;
            order.AddressCode = model.AddressCode;
            order.AddressName = model.AddressName;
            order.AddressStreet = model.AddressStreet;
            order.Phone = model.Phone;
            order.Age = model.Age;
            order.Sex = model.Sex;
            order.CreateTime = model.CreateTime;
            order.PayTime = model.PayTime;
            order.OrderStatus = model.OrderStatus;
            this.OrderRepository.Insert(order);
        }
        
        public void Update(Guid orderId,OrderModel model)
        {
            OrderInfo order = this.OrderRepository.Get(orderId);
            if (order == null)
            {
                throw new FriendlyException("记录不存在。");
            }
            order.ResidentUserId = model.ResidentUserId;
            order.OrderNo = model.OrderNo;
            order.OrderTitle = model.OrderTitle;
            order.OrderImage = model.OrderImage;
            order.ProductAmount = model.ProductAmount;
            order.ExpressAmount = model.ExpressAmount;
            order.TotalAmount = model.TotalAmount;
            order.PayStatus = model.PayStatus;
            order.OrderDetail = model.OrderDetail;
            order.ExpressNo = model.ExpressNo;
            order.LastExpressQueryTime = model.LastExpressQueryTime;
            order.FullName = model.FullName;
            order.AddressCode = model.AddressCode;
            order.AddressName = model.AddressName;
            order.AddressStreet = model.AddressStreet;
            order.Phone = model.Phone;
            order.Age = model.Age;
            order.Sex = model.Sex;
            order.CreateTime = model.CreateTime;
            order.PayTime = model.PayTime;
            order.OrderStatus = model.OrderStatus;
            this.OrderRepository.Update(order);
        }
        
        public OrderInfo GetOrderById(Guid orderId)
        {
            return this.OrderRepository.Get(orderId);
        }

        public void Delete(Guid orderId)
        {
            OrderInfo order=this.OrderRepository.Get(orderId);
            if (order == null)
                return;
            this.OrderRepository.Delete(order);
        }

        public PageResult<OrderInfo> GetOrderByPage(OrderPageParam pageParam)
        {
            List<OrderInfo> items = this.OrderRepository.GetOrderByPage(pageParam);
            int total = this.OrderRepository.GetOrderRecordCount(pageParam);
            return new PageResult<OrderInfo>(total, items);
        }
    }
}

