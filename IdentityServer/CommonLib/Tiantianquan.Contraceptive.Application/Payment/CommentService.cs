﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure;
using Tiantianquan.Infrastructure.AutoMapper;
using Tiantianquan.Infrastructure.Application;
using Tiantianquan.Infrastructure.Utils;
using Tiantianquan.Infrastructure.Utilities;
using Tiantianquan.Infrastructure.UI;
using Tiantianquan.Contraceptive.Domain.Payment;

namespace Tiantianquan.Contraceptive.Application.Payment
{
    /// <summary>
    /// 评论
    /// </summary>
   public class CommentService:ServiceBase
    {
        public CommentService(CommentRepository commentRepository)
        {
            CommentRepository = commentRepository;
        }

        protected CommentRepository CommentRepository { get; private set; }
        
        public void Insert(CommentModel model)
        {
            CommentInfo comment = new CommentInfo();
            comment.CommentId = GuidUtils.SecureSequentialGuid();       
            comment.OrderId = model.OrderId;
            comment.ResidentUserId = model.ResidentUserId;
            comment.HeadImageUrl = model.HeadImageUrl;
            comment.FullName = model.FullName;
            comment.Phone = model.Phone;
            comment.CommentContent = model.CommentContent;
            comment.CommentImages = model.CommentImages;
            comment.CreateTime = model.CreateTime;
            comment.IPAddress = model.IPAddress;
            this.CommentRepository.Insert(comment);
        }
        
        public void Update(Guid commentId,CommentModel model)
        {
            CommentInfo comment = this.CommentRepository.Get(commentId);
            if (comment == null)
            {
                throw new FriendlyException("记录不存在。");
            }
            comment.OrderId = model.OrderId;
            comment.ResidentUserId = model.ResidentUserId;
            comment.HeadImageUrl = model.HeadImageUrl;
            comment.FullName = model.FullName;
            comment.Phone = model.Phone;
            comment.CommentContent = model.CommentContent;
            comment.CommentImages = model.CommentImages;
            comment.CreateTime = model.CreateTime;
            comment.IPAddress = model.IPAddress;
            this.CommentRepository.Update(comment);
        }
        
        public CommentInfo GetCommentById(Guid commentId)
        {
            return this.CommentRepository.Get(commentId);
        }

        public void Delete(Guid commentId)
        {
            CommentInfo comment=this.CommentRepository.Get(commentId);
            if (comment == null)
                return;
            this.CommentRepository.Delete(comment);
        }

        public PageResult<CommentInfo> GetCommentByPage(CommentPageParam pageParam)
        {
            List<CommentInfo> items = this.CommentRepository.GetCommentByPage(pageParam);
            int total = this.CommentRepository.GetCommentRecordCount(pageParam);
            return new PageResult<CommentInfo>(total, items);
        }
    }
}

