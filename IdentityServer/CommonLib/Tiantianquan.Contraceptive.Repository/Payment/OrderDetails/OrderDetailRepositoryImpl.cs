﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Criterion;
using Tiantianquan.Infrastructure.Repositories;
using Tiantianquan.Contraceptive.Domain.Payment;

namespace Tiantianquan.Contraceptive.Repository.Payment
{
    /// <summary>
    /// 订单明细
    /// </summary>
   public class OrderDetailRepositoryImpl:Repository<OrderDetailInfo,Guid>,OrderDetailRepository
    {
        #region 字段常量
        internal class FieldNames
        {
            /// <summary>
            /// 订单明细编号
            /// </summary>
            public const string OrderDetailId = "OrderDetailId";
            /// <summary>
            /// 订单编号
            /// </summary>
            public const string OrderId = "OrderId";
            /// <summary>
            /// 商品编码
            /// </summary>
            public const string ProductId = "ProductId";
            /// <summary>
            /// SKU编号
            /// </summary>
            public const string SkuId = "SkuId";
            /// <summary>
            /// 商品名称
            /// </summary>
            public const string ProductName = "ProductName";
            /// <summary>
            /// 商品图片
            /// </summary>
            public const string ProductImage = "ProductImage";
            /// <summary>
            /// Sku属性
            /// </summary>
            public const string SkuProperty = "SkuProperty";
            /// <summary>
            /// Sku属性值
            /// </summary>
            public const string SkuPropertyValue = "SkuPropertyValue";
            /// <summary>
            /// 成交单价
            /// </summary>
            public const string TradPrice = "TradPrice";
            /// <summary>
            /// 成交数量
            /// </summary>
            public const string TradQuantity = "TradQuantity";
            /// <summary>
            /// 总金额
            /// </summary>
            public const string TotalAmount = "TotalAmount";
            /// <summary>
            /// 创建时间
            /// </summary>
            public const string CreateTime = "CreateTime";
        }
        #endregion
        /// <summary>
        /// 获取订单明细分页数据
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        public List<OrderDetailInfo> GetOrderDetailByPage(OrderDetailPageParam pageParam)
        {
            var criteria = Session.CreateCriteria<OrderDetailInfo>();
            if (!string.IsNullOrWhiteSpace(pageParam.Keywords))
            {
                criteria.Add(Restrictions.Like(FieldNames.ProductName, "%" + pageParam.Keywords.Trim() + "%"));
            }

            criteria.AddOrder(NHibernate.Criterion.Order.Asc(FieldNames.CreateTime));
            if (pageParam.IsClientSort())
            {
                pageParam.CreateOrderCriteria(criteria);
            }
            criteria.SetFirstResult(pageParam.GetFirstResult());
            criteria.SetMaxResults(pageParam.GetMaxResults());
            List<OrderDetailInfo> items = criteria.List<OrderDetailInfo>().ToList();
            return items;
        }
        /// <summary>
        /// 获取订单明细总记录数
        /// </summary>
        /// <param name="pageParam"></param>
        /// <returns></returns>
        public int GetOrderDetailRecordCount(OrderDetailPageParam pageParam)
        {
            var criteria = Session.CreateCriteria<OrderDetailInfo>();
            if (!string.IsNullOrWhiteSpace(pageParam.Keywords))
            {
                criteria.Add(Restrictions.Like(FieldNames.ProductName, "%" + pageParam.Keywords.Trim() + "%"));
            }
            criteria.SetProjection(Projections.RowCount());
            int total = (int)criteria.UniqueResult();
            return total;
        }
    }
}

