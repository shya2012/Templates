﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Tiantianquan.Common.Logging;
using Tiantianquan.Common.Repositories;

namespace Tiantianquan.Common.Application
{
    public class TransactionScopeInterceptor : IInterceptor
    {
        public TransactionScopeInterceptor(ILoggerFactory loggerFactory)
        {
            Logger = loggerFactory.Create(typeof(TransactionScopeInterceptor));
        }

        public ILogger Logger { get; private set; }
        public void Intercept(IInvocation invocation)
        {
            var methodInfo=invocation.Method;
            if (methodInfo == null)
            {
                methodInfo = invocation.MethodInvocationTarget;
            }
            if (!methodInfo.IsDefined(typeof(TransactionAttribute),false)){
                invocation.Proceed();
                return;
            }

            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    invocation.Proceed();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                throw ex;
            }
        }
    }
}
