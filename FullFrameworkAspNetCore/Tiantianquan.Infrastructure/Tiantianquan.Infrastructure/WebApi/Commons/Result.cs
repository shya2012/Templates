﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Tiantianquan.Infrastructure.WebApi.Commons {
    /// <summary>
    /// 返回结果
    /// </summary>
    public class Result : JsonResult {
        /// <summary>
        /// 状态码
        /// </summary>
        private readonly bool _success;
        /// <summary>
        /// 消息
        /// </summary>
        private readonly string _message;
        /// <summary>
        /// 数据
        /// </summary>
        private readonly dynamic _data;

        /// <summary>
        /// 初始化返回结果
        /// </summary>
        /// <param name="success">状态码</param>
        /// <param name="message">消息</param>
        /// <param name="data">数据</param>
        public Result(bool success, string message, dynamic data = null ) : base( null ) {
            _success = success;
            _message = message;
            _data = data;
        }

        /// <summary>
        /// 执行结果
        /// </summary>
        public override Task ExecuteResultAsync( ActionContext context ) {
            this.Value = new {
                Success = _success,
                Message = _message,
                Data = _data
            };
            return base.ExecuteResultAsync( context );
        }
    }
}
