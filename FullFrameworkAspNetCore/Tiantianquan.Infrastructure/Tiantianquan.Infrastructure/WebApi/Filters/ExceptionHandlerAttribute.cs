﻿using Exceptionless.Extensions;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Text;
using Tiantianquan.Infrastructure.WebApi.Commons;

namespace Tiantianquan.Infrastructure.WebApi.Filters
{
    /// <summary>
    /// 异常处理过滤器
    /// </summary>
    public class ExceptionHandlerAttribute : ExceptionFilterAttribute
    {
        /// <summary>
        /// 异常处理
        /// </summary>
        public override void OnException(ExceptionContext context)
        {
            context.ExceptionHandled = true;
            context.HttpContext.Response.StatusCode = 200;
            context.Result = new Result(false, context.Exception.GetInnermostException().GetMessage(),context.Exception.GetInnermostException());
        }
    }
}