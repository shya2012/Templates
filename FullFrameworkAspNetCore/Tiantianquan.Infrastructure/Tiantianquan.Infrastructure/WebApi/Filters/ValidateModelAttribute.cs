﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.WebApi.Commons;

namespace Tiantianquan.Infrastructure.WebApi.Filters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var modelErrorCollection = context.ModelState.Select(item => item.Value.Errors);
                List<ModelError> errors = new List<ModelError>();
                foreach (var modelError in modelErrorCollection)
                {
                    errors.AddRange(modelError.Where(item=>!string.IsNullOrWhiteSpace(item.ErrorMessage)&&item.ErrorMessage != "The value '' is invalid."));
                }
                var err=errors.FirstOrDefault();
                if (err != null)
                {
                    string message = err.ErrorMessage;
                    context.Result = new Result(false, message, context.ModelState);
                }
                
            }
        }
    }
}
