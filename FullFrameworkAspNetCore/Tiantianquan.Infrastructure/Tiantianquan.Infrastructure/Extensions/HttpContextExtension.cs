﻿using Exceptionless.Json;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.Extensions
{
   public static class HttpContextExtension
    {
        public static string GetAbsouteUri(this HttpContext context)
        {
            return context.Request.Scheme + context.Request.Host + context.Request.Path + context.Request.QueryString;
        }

        public static string GetArguments(this HttpContext context)
        {
            Dictionary<string, object> pairs = new Dictionary<string, object>();
            if (context.Request.HasFormContentType)
            {
                foreach (var field in context.Request.Form)
                {
                    pairs.Add(field.Key, field.Value.ToString());
                }
                return JsonConvert.SerializeObject(pairs, Formatting.Indented);
            }
            if (context.Request.Body.CanRead)
            {
                StreamReader reader = new StreamReader(context.Request.Body);
                string json=reader.ReadToEnd();
                reader.Close();
                return JsonConvert.SerializeObject(JsonConvert.DeserializeObject(json), Formatting.Indented);
            }
            return string.Empty;
        }
    }
}
