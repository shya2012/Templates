﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using Tiantianquan.Infrastructure.Utils;

namespace Tiantianquan.Infrastructure
{
    public class CommonController:Controller
    {
        public CommonController(ILogger<CommonController> logger,IConfiguration configuration)
        {
            Logger = logger;
            Configuration = configuration;
        }

        public ILogger<CommonController> Logger { get; private set; }

        public IConfiguration Configuration { get; private set; }

        [HttpGet]
        //[DontWrapper]
        [Route("~/api/Common/DynamicScripts")]
        public string GetDynamicScripts()
        {
            var assemblies = new List<Assembly>();
            assemblies.Add(typeof(CommonController).Assembly);
            foreach (string assemblyString in Configuration.GetSection("EnumAssemblies").Get<List<string>>())
            {
                assemblies.Add(Assembly.Load(assemblyString));
            }
            List<Type> listType = new List<Type>();
            foreach (var assembly in assemblies)
            {
                try
                {
                    foreach (var type in assembly.GetTypes())
                    {
                        if (type == null)
                            continue;
                        if (type.BaseType == null)
                            continue;
                        if (type.BaseType.FullName != "System.Enum")
                            continue;
                        if (string.IsNullOrWhiteSpace(type.Namespace))
                            continue;

                        //if (!type.Namespace.StartsWith(this.GetType().Namespace.Split('.')[0]) && !type.Namespace.StartsWith("Tools"))
                        //{
                        //    continue;
                        //}
                        listType.Add(type);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.Message, ex);
                }
            }

            StringBuilder builder = new StringBuilder();
            builder.Append("var enums = enums || {};");
            foreach (var type in listType)
            {
                builder.AppendLine(EnumUtils.GetJavascript(type));
            }

            //var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            //new PlainTextFormatter()
            //response.Content = new StringContent(builder.ToString(), Encoding.UTF8);
           // response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-javascript");
            
            return builder.ToString();
        }
    }
}
