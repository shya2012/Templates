﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Utils;

namespace Tiantianquan.Infrastructure.Services
{
    public interface IFileStore
    {
        string SaveFileIfExist(string name);
    }

    public class DefaultFileStore : IFileStore
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;
        public DefaultFileStore(IHttpContextAccessor httpContextAccessor, IHostingEnvironment hostingEnvironment )
        {
            this._httpContextAccessor = httpContextAccessor;
            this._hostingEnvironment = hostingEnvironment;
        }
        public string SaveFileIfExist(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return string.Empty;
            }
            IFormFile file=this._httpContextAccessor.HttpContext.Request.Form.Files.GetFile(name);
            if (file == null)
            {
                return string.Empty;
            }
            string relativePath = string.Format("\\Upload\\{0}\\{1}{2}{3}",
                  DateTime.Now.ToString("yyyyMM"),
                  DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                  RandomUtils.RandomInt(1000, 9999),
                  Path.GetExtension(file.FileName));
           
            string absolutePath = this._hostingEnvironment.WebRootPath + relativePath;
            PathUtils.EnsureParentDirectory(absolutePath);

            Stream stream = file.OpenReadStream();
            byte[] buffer = new byte[file.Length];
            stream.Read(buffer, 0, buffer.Length);
            stream.Close();
            System.IO.File.WriteAllBytes(absolutePath, buffer);

            return relativePath.Replace("\\","/");
        }
    }


 
}
