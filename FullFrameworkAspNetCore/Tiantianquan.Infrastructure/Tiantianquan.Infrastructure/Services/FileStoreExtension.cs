﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiantianquan.Infrastructure.Services;
using Tiantianquan.Infrastructure.WebApi;

namespace Microsoft.AspNetCore.Builder
{
    public static partial class FileStoreExtension
    {
        public static IServiceCollection AddDefaultFileStore(this IServiceCollection services)
        {
            services.AddSingleton<IFileStore, DefaultFileStore>();
            return services;
        }
    }


    public static partial class SnowflakeExtension
    {
        public static IServiceCollection AddSnowflake(this IServiceCollection services)
        {
            services.AddSingleton<ISnowflakeService, SnowflakeService>();
            return services;
        }
    }
}
