﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.Dependency
{
    public static class IocManager
    {

        static IServiceProvider ServiceProvider;

        public static void Initialize(IServiceProvider serviceProvider)
        {
            if (IocManager.ServiceProvider == null)
            {
                IocManager.ServiceProvider = serviceProvider;
            }
        }

       public static object GetService(Type serviceType)
        {
            if (IocManager.ServiceProvider == null)
            {
                throw new ArgumentNullException(nameof(IocManager.ServiceProvider));
            }
            return IocManager.ServiceProvider.GetService(serviceType);
        }


        public static TService GetService<TService>()
        {
            return (TService)IocManager.GetService(typeof(TService));
        }
    }




}
