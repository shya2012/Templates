﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure
{
    public class Engine
    {
        private static ConcurrentDictionary<string, Database> DatabaseDictionary = new ConcurrentDictionary<string, Database>();

        private static object Locker = new object();

        public static IDbConnection GetDbConnection(string name = "default")
        {
            lock (Locker)
            {
                if (!DatabaseDictionary.ContainsKey(name))
                {
                    throw new ArgumentException(string.Format("不存在名为{0}的数据库。", name));
                }
                Database database = DatabaseDictionary[name];
       
                DbProviderFactory dbProviderFactory = DbProviderFactories.GetFactory(database.ProviderInvariantName);
                IDbConnection connection = dbProviderFactory.CreateConnection();
                connection.ConnectionString = database.ConnectionString;
                connection.Open();
                return connection;
            }
        }

        public static void RegisterDataBase(string name, string providerInvariantName, string connectionString)
        {
            if (DatabaseDictionary.ContainsKey(name))
            {
                throw new ArgumentException(string.Format("已经存在名为{0}的数据库。", name));
            }
            DatabaseDictionary[name] = new Database(providerInvariantName, connectionString);


        }
    }
}
