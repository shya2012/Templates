﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.Dapper
{
    public class SqlBuilder
    {
        private StringBuilder buffer = new StringBuilder();

        private string sql = string.Empty;

        private string groupby = string.Empty;

        public SqlBuilder(string sql)
        {
            this.sql = sql;
        }

        public void And(string condition)
        {
            buffer.Append(" AND ").Append(condition);
        }

        public void Or(string condition)
        {
            buffer.Append("  OR ").Append(condition);
        }

        public override string ToString()
        {
            if (buffer.Length == 0)
            {
                return this.sql+this.groupby;
            }
            return this.sql + " WHERE " + buffer.ToString().Substring(4) + this.groupby;
        }

        public void GroupBy(string columns)
        {
            this.groupby = " GROUP BY "+columns;
        }

        public static string GetSqlPart<T>(List<T> ids, DynamicParameters parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException("parameters");
            }
            if (ids == null)
            {
                throw new ArgumentNullException("ids");
            }
            if (ids.Count() == 0)
            {
                throw new ArgumentException("集合数据不能为空。");
            }
            StringBuilder sb = new StringBuilder();
            foreach (var id in ids)
            {
                string paramName = "@p" + Conv.ToString(parameters.ParameterNames.Count() + 1);
                if (sb.Length > 0)
                {
                    sb.Append(",");
                }
                sb.Append(paramName);
                parameters.Add(paramName, id);
            }
            return sb.ToString();
        }
    }
}
