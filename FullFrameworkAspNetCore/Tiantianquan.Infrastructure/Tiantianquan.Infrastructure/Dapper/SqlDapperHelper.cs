﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.Dapper
{
    public static class SqlDapperHelper
    {

        public static List<T> GetRecordByPage<T>(this IDbConnection connection, int startRecordIndex, int endRecordIndex, string sql, DynamicParameters parameters, string orderBy)
        {
            string sqls = "SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY " + orderBy + ") AS ROWID,* FROM (" + sql + ") AS T ) AS TT WHERE ROWID BETWEEN @StartRecordIndex AND @EndRecordIndex";
            if (parameters == null)
            {
                parameters = new SqlDynamicParameters();
            }
            parameters.Add("@StartRecordIndex", startRecordIndex, DbType.Int32);
            parameters.Add("@EndRecordIndex", endRecordIndex, DbType.Int32);

            return connection.Query<T>(sqls, parameters).ToList();

        }
        public static int GetRecordCount(this IDbConnection connection, string sql, DynamicParameters parameters)
        {
            string sqls = "SELECT COUNT(*)  FROM (" + sql + ") AS T";

            return connection.ExecuteScalar<int>(sqls, parameters);

        }
        public static List<T> GetRecordByPage<T>(int startRecordIndex, int endRecordIndex, string sql, DynamicParameters parameters, string orderBy,string database="default")
        {
            string sqls = "SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY " + orderBy + ") AS ROWID,* FROM (" + sql + ") AS T ) AS TT WHERE ROWID BETWEEN @StartRecordIndex AND @EndRecordIndex";
            if (parameters == null)
            {
                parameters = new SqlDynamicParameters();
            }
            parameters.Add("@StartRecordIndex", startRecordIndex, DbType.Int32);
            parameters.Add("@EndRecordIndex", endRecordIndex, DbType.Int32);

            using (var connection = Engine.GetDbConnection(database))
            {
                return connection.Query<T>(sqls, parameters).ToList();
            }

        }
        public static int GetRecordCount(string sql, DynamicParameters parameters, string database = "default")
        {
            string sqls = "SELECT COUNT(*)  FROM (" + sql + ") AS T";
            using (var connection = Engine.GetDbConnection(database))
            {
                return connection.ExecuteScalar<int>(sqls, parameters);
            }


        }
    }
}
