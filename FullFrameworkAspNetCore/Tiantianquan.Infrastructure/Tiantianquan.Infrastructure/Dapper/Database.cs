﻿using Newtonsoft.Json;
using System.Data;
using System.Data.Common;

namespace Tiantianquan.Infrastructure
{
    public struct Database
    {
        public Database(string providerInvariantName, string connectionString) : this()
        {
            ProviderInvariantName = providerInvariantName;
            ConnectionString = connectionString;
            //DbProviderFactory dbProviderFactory = DbProviderFactories.GetFactory(ProviderInvariantName);
            //DbConnection = dbProviderFactory.CreateConnection();
            //DbConnection.ConnectionString = ConnectionString;
        }

        public string ProviderInvariantName { get; }

        public string ConnectionString { get;}

        //[JsonIgnore]
        //public IDbConnection DbConnection { get; }

    }
}
