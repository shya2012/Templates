﻿using FluentValidation.Resources;
using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.Validation
{
    public class EmailPropertyValidator : PropertyValidator
    {
        public EmailPropertyValidator() : base("电子邮箱不合法。")
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
          return  Utils.RegexUtils.Validators.Email.IsMatch(context.PropertyValue as string);
        }
    }
}
