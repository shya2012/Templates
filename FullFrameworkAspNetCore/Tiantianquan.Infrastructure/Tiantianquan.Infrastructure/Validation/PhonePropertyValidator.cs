﻿using FluentValidation.Resources;
using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.Validation
{
    public class PhonePropertyValidator : PropertyValidator
    {
        public PhonePropertyValidator() : base("手机号码不合法。")
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
           return Utils.RegexUtils.Validators.ChinaMobile.IsMatch(context.PropertyValue as string);
        }
    }
}
