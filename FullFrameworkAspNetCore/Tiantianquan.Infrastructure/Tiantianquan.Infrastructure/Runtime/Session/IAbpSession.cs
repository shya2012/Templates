﻿using Microsoft.AspNetCore.Http;
using System;

namespace Tiantianquan.Infrastructure.Runtime.Session
{
    /// <summary>
    /// Defines some session information that can be useful for applications.
    /// </summary>
    public interface IAbpSession
    {

        HttpContext HttpContext { get; }
        /// <summary>
        /// 用户名称
        /// </summary>
        Guid UserId { get; }

        /// <summary>
        /// 账号
        /// </summary>
        string UserName { get; }
        /// <summary>
        /// 昵称
        /// </summary>
        string NickName { get; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        string FullName { get; }
        /// <summary>
        /// 所在单位
        /// </summary>
        Guid? OrganizationId { get; }
        /// <summary>
        /// 单位名称
        /// </summary>
        string OrganizationName { get; }


        string Subject { get; }


    }
}