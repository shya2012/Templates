﻿using System;
using Microsoft.AspNetCore.Http;

namespace Tiantianquan.Infrastructure.Runtime.Session
{
    /// <summary>
    /// Implements null object pattern for <see cref="IAbpSession"/>.
    /// </summary>
    public class NullAbpSession : IAbpSession
    {

        public Guid? AdminUserId => null;

        public Guid UserId => Guid.Empty;

        public Guid? OrganizationId => null;

        public string OrganizationName => string.Empty;

        public Guid? RoleId => null;

        public string RoleName => string.Empty;

        public string UserName => string.Empty;
        public string NickName => string.Empty;
        public string FullName => string.Empty;

        public int? CompanyType => null;

        public string CompanyAreaId => string.Empty;

        public HttpContext HttpContext => null;

        public string Subject => null;

        private NullAbpSession()
        {
        }
    }
}