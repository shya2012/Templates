﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure
{
   public interface IUserManager<TUser> where TUser: IdentityUser
    {
        TUser GetUserByUserNameAndPassword(string userName, string password);


    }

    public class IdentityUser
    {
        public virtual Guid UserId { get; set; }

        public virtual string UserName { get; set; }

        public virtual string PasswordSalt { get; set; }

        public virtual string PasswordHash { get; set; }

        public virtual string NickName { get; set; }

        public virtual string HeadImageUrl { get; set; }

        public virtual Guid OrgationId { get; set; }

        public virtual string OragionName { get; set; }

        public virtual DateTime CreationTime { get; set; }
    }
}
