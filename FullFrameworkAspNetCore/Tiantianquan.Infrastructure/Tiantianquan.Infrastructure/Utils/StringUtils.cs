﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure.Utils
{
    public static class StringUtils
    {
        private static Object Locker = new object();
        public static string SafeTrim(string s)
        {
            if (s == null)
                return string.Empty;
            return s.Trim();
        }

        public static string GetBusinessNo()
        {
            lock (Locker)
            {
                return DateTime.Now.ToString("yyyyMMddHHmm") + RandomUtils.RandomInt(100, 999);
            }

        }

        public static string GenerateBusinessNo()
        {
            lock (Locker)
            {
                int hash = Math.Abs(Guid.NewGuid().GetHashCode());
                int rand = RandomUtils.RandomInt(100, 999);
                int m = (hash / rand) % 9;
                return DateTime.Now.ToString("yyMM")
                        + Conv.ToString(hash).PadRight(10, '0')
                        + rand
                        + m;
            }
        }
    }
}
