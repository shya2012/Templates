﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiantianquan.Infrastructure
{
    /// <summary>
    /// 全局设置
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// 单位ID
        /// </summary>
        public Guid OrganizationId { get; set; }
        /// <summary>
        /// 单位名称
        /// </summary>
        public string OrganizationName { get; set; }

        /// <summary>
        /// 老平台地址，调用接口获取菜单
        /// </summary>
        public string PlatformWebHost {get;set;}


        public List<string> EnumAssemblies { get; set; }
    }
}
