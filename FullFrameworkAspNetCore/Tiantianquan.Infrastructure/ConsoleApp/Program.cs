﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Tiantianquan.Infrastructure;
using Tiantianquan.Infrastructure.Utils;

namespace ConsoleApp
{

    /// <summary>
    /// Person
    /// </summary>
    public class Person : IXmlSerializable
    {
        /// <summary>
        /// PersonId
        /// </summary>
        public int PersonId { get; set; }
        /// <summary>
        /// PersonName
        /// </summary>
        public string PersonName { get; set; }
        /// <summary>
        /// Birthday
        /// </summary>
        public DateTime? Birthday { get; set; }
        /// <summary>
        /// Age
        /// </summary>
        public int Age { get; set; }
        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks { get; set; }

        public XmlSchema GetSchema()
        {
            return default(XmlSchema);
        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement("Person");
            this.PersonId = reader.ReadElementContentAsInt("PersonId", "");
            this.PersonName = reader.ReadElementContentAsString("PersonName", "");
            this.Birthday = reader.ReadElementContentAsDateTime("Birthday", "");
            this.Age = reader.ReadElementContentAsInt("Age", "");
            this.Remarks = reader.ReadElementContentAsString("Remarks", "");
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(this.PersonId));
            writer.WriteValue(this.PersonId);
            writer.WriteEndElement();

            writer.WriteStartElement(nameof(this.PersonName));
            writer.WriteCData(this.PersonName);
            writer.WriteEndElement();

            writer.WriteStartElement(nameof(this.Birthday));
            writer.WriteValue(this.Birthday);
            writer.WriteEndElement();

            writer.WriteStartElement(nameof(this.Age));
            writer.WriteValue(this.Age);
            writer.WriteEndElement();

            writer.WriteStartElement(nameof(this.Remarks));
            writer.WriteCData(this.Remarks);
            writer.WriteEndElement();


        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<string> emails = Tiantianquan.Infrastructure.MockData.Generators.EmailAddressRandomGenerator.Instance.BatchGenerate(10);
            List<string> chinesenames = Tiantianquan.Infrastructure.MockData.Generators.ChineseNameRandomGenerator.Instance.BatchGenerate(10);
            List<string> mobilephones = Tiantianquan.Infrastructure.MockData.Generators.ChineseMobileRandomGenerator.Instance.BatchGenerate(10);
            List<string> idcards = Tiantianquan.Infrastructure.MockData.Generators.IdCards.ChineseIdCardRandomGenerator.Instance.BatchGenerate(10);
            List<string> address = Tiantianquan.Infrastructure.MockData.Generators.Address.ChineseAddressRandomGenerator.Instance.BatchGenerate(10);
            List<string> bankcards = Tiantianquan.Infrastructure.MockData.Generators.Banks.BankCardRandomGenerator.Instance.BatchGenerate(10);

            Console.WriteLine(Json.SerializeObject(emails));

            foreach (var chinesename in chinesenames)
            {
                string pinyin = NPinyin.Pinyin.GetPinyin(chinesename).Replace(" ", "") + RandomUtils.RandomDateTime(DateTime.Now.AddYears(-100), DateTime.Now.AddYears(7)).ToString("yyyyMM");

                Console.WriteLine(pinyin);
            }

            XmlSerializer serializer = new XmlSerializer(typeof(Person));

            Person person = new Person();
            person.PersonId = 1;
            person.PersonName = chinesenames[0];
            person.Birthday = RandomUtils.RandomDateTime(DateTime.Now.AddYears(-40), DateTime.Now);
            person.Age = DateTime.Now.Year - person.Birthday.GetValueOrDefault().Year;
            person.Remarks = "https://blog.csdn.net/shuliuzh/article/details/49426711";

            using (MemoryStream writer = new MemoryStream())
            {
                serializer.Serialize(writer, person);
                string xml = Encoding.UTF8.GetString(writer.GetBuffer());
                Console.WriteLine(xml);
                File.WriteAllText("person.xml", xml, Encoding.UTF8);

                string cryptedString = JavaDesUtils.Encrypt(xml, "12345678");
                string originalString = JavaDesUtils.Decrypt(cryptedString, "12345678");

                Console.WriteLine("原文：");
                Console.WriteLine(originalString);

                Console.WriteLine("密文：");
                Console.WriteLine(cryptedString);
            }

            Console.WriteLine(PinYinUtils.GetPinyin("计算机"));
            Console.WriteLine(PinYinUtils.GetFirstPinyin("计算机"));

            Guid s1 = Guid.Parse("{E5E4B788-DB73-45A3-BFA3-B336DA0C72EA}");
            Guid s2 = Guid.Parse("{E5E4B788-DB73-45A3-BFA3-B336DA0C72EA}");



            Console.WriteLine(Conv.ToString(Math.Abs(s1.GetHashCode())).PadRight(12, '0'));
            Console.WriteLine(Conv.ToString(Math.Abs(s2.GetHashCode())).PadRight(12, '0'));

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(GenerateCode());
            }
            Console.Read();
        }

        static string GenerateCode()
        {
            int hash = Math.Abs(Guid.NewGuid().GetHashCode());
            int rand = RandomUtils.RandomInt(100, 999);
            int m = (hash / rand) % 9;
            return DateTime.Now.ToString("yyMM")
                    + Conv.ToString(hash).PadRight(10, '0')
                    + rand
                    + m;
        }
    }
}
