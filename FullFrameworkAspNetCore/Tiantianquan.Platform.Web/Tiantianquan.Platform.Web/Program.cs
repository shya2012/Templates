﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Stores;
using IdentityServer4.Validation;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Tiantianquan.Platform.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }


    public class ClientStore : IClientStore
    {
        public Task<Client> FindClientByIdAsync(string clientId)
        {
            throw new NotImplementedException();
        }
    }

    public class UserStore : IUserConsentStore
    {
        public Task<Consent> GetUserConsentAsync(string subjectId, string clientId)
        {
            throw new NotImplementedException();
        }

        public Task RemoveUserConsentAsync(string subjectId, string clientId)
        {
            throw new NotImplementedException();
        }

        public Task StoreUserConsentAsync(Consent consent)
        {
            throw new NotImplementedException();
        }
    }

    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        public Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            List<Claim> claimsIdentity = new List<Claim>();

            claimsIdentity.Add(new Claim(ClaimTypes.NameIdentifier, "admin"));
            claimsIdentity.Add(new Claim(ClaimTypes.NameIdentifier, "管理员"));
            //ClaimsPrincipal principal = new ClaimsPrincipal(claimsIdentity);

            Dictionary<string, object> customResponse = new Dictionary<string, object>()
            {
                { "username","admin"},
                { "fullname","管理员"}
            };



            context.Result = new GrantValidationResult("1", "Bearer", DateTime.UtcNow, claimsIdentity, "local", customResponse);

            return Task.FromResult(0);
        }
    }
}
